\mainpage Hz::Traits

This is a collection of traits consumed by other `Hz` libraries.