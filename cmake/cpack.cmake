# Allow packaging (which is what CI uses)
set(CPACK_PACKAGE_VENDOR seppeon)
set(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})
set(CPACK_PACKAGE_DESCRIPTION ${PROJECT_DESCRIPTION})
set(CPACK_PACKAGE_HOMEPAGE_URL https://gitlab.com/seppeon/${PROJECT_NAME})
set(CPACK_RESOURCE_FILE_LICENSE ${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.txt)
set(CPACK_NSIS_MODIFY_PATH ON)
set(CPACK_INNOSETUP_USE_MODERN_WIZARD ON)

if (WIN32)
	set(CPACK_WIX_LICENSE_RTF ${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.txt)
	set(CPACK_WIX_UPGRADE_GUID "261beb07-8542-4960-82c4-a086949e4617")
	set(CPACK_WIX_PRODUCT_GUID "d1c178b9-4ef7-4e56-b577-366466773999")
endif()

include(CPack)