#pragma once
#include <type_traits>

namespace Hz
{
	namespace Impl
	{
		template <typename From, typename To>
		struct ApplyCvref
		{
			using type = To;
		};
		template <typename From, typename To>
		struct ApplyCvref<From const, To>
		{
			using type = To const;
		};
		template <typename From, typename To>
		struct ApplyCvref<From volatile, To>
		{
			using type = To volatile;
		};
		template <typename From, typename To>
		struct ApplyCvref<From const volatile, To>
		{
			using type = To const volatile;
		};
		template <typename From, typename To>
		struct ApplyCvref<From &, To>
		{
			using type = To &;
		};
		template <typename From, typename To>
		struct ApplyCvref<From const &, To>
		{
			using type = To const &;
		};
		template <typename From, typename To>
		struct ApplyCvref<From &&, To>
		{
			using type = To &&;
		};
		template <typename From, typename To>
		struct ApplyCvref<From const &&, To>
		{
			using type = To const &&;
		};
		template <typename From, typename To>
		struct ApplyCvref<From volatile &, To>
		{
			using type = To volatile &;
		};
		template <typename From, typename To>
		struct ApplyCvref<From volatile const &, To>
		{
			using type = To volatile const &;
		};
		template <typename From, typename To>
		struct ApplyCvref<From volatile &&, To>
		{
			using type = To volatile &&;
		};
		template <typename From, typename To>
		struct ApplyCvref<From volatile const &&, To>
		{
			using type = To volatile const &&;
		};

		template <typename From, typename To>
		struct ApplyCvrefTo
		{
			using type = typename ApplyCvref<From, To>::type;
		};
		template <typename From, typename To>
		struct ApplyCvrefTo<From, To &>
		{
			using type = typename ApplyCvref<From, std::remove_reference_t<To>>::type &;
		};
		template <typename From, typename To>
		struct ApplyCvrefTo<From, To &&>
		{
			using type = typename ApplyCvref<From, std::remove_reference_t<To>>::type &&;
		};
	}
	/**
	 * @brief Take the qualifiers from `From` and apply them to `To`.
	 * 
	 * @ingroup Aliases
	 *
	 * @tparam From The type with the desired qualifiers.
	 * @tparam To The type to apply the qualifiers to.
	 */
	template <typename From, typename To>
	using ApplyCvref = typename Impl::ApplyCvrefTo<From, To>::type;
}