#pragma once
#include <span>
#include <array>
#include <cstddef>
#include <type_traits>

namespace Hz
{
	/**
	 * @brief A customization point, that allows users of the library to declare the size of some statically sized container.
	 * 
	 * @tparam T The type of the container.
	 */
	template <typename T>  
	struct ArraySizeTraits;
	/**
	 * @brief A c-array specialization for `ArraySizeTraits`.
	 * 
	 * @tparam T The type of the elements.
	 * @tparam N The size of the c-array.
	 */
	template <typename T, std::size_t N>
	struct ArraySizeTraits<T[N]> : std::integral_constant<std::size_t, N> {};
	/**
	 * @brief A `std::array` specialization for `ArraySizeTraits`.
	 * 
	 * @tparam T The type of the elements.
	 * @tparam N The size of the array.
	 */
	template <typename T, std::size_t N>
	struct ArraySizeTraits<std::array<T, N>> : std::integral_constant<std::size_t, N> {};
	/**
	 * @brief A `std::span` specialization for `ArraySizeTraits`.
	 * 
	 * @tparam T The type of the elements.
	 * @tparam N The size of the span.
	 */
	template <typename T, std::size_t N> requires(N != std::dynamic_extent)
	struct ArraySizeTraits<std::span<T, N>> : std::integral_constant<std::size_t, N> {};
	/**
	 * @brief Get the length of a given standard fixed length type, this is a customization point.
	 * 
	 * @ingroup Variables
	 * @tparam T The type to extract the length from.
	 */
    template <typename T>
    inline constexpr auto array_size_traits_v = ArraySizeTraits<T>::value;
}