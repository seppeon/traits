#pragma once
#include <array>
#include <type_traits>

namespace Hz
{
	namespace Impl
	{
		template <typename T>
		struct ArrayLen;
		template <typename T, std::size_t N>
		struct ArrayLen<T[N]>
		{
			static constexpr auto value = N;
		};
		template <typename T, std::size_t N>
		struct ArrayLen<std::array<T, N>>
		{
			static constexpr auto value = N;
		};
	}
	/**
	 * @brief Get the length of a given standard array type (language or library).
	 * 
	 * @ingroup Variables
	 * @tparam T The type to extract the length from.
	 */
	template <typename T>
	inline constexpr auto get_array_length_v = Impl::ArrayLen<std::remove_cvref_t<std::remove_pointer_t<T>>>::value;
}