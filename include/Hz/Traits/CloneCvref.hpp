#pragma once
#include <type_traits>

namespace Hz
{
	namespace Impl
	{
		template <typename From, typename To>
		struct CloneCvref
		{
			using type = To;
		};
		template <typename From, typename To>
		struct CloneCvref<From const, To>
		{
			using type = To const;
		};
		template <typename From, typename To>
		struct CloneCvref<From volatile, To>
		{
			using type = To volatile;
		};
		template <typename From, typename To>
		struct CloneCvref<From const volatile, To>
		{
			using type = To const volatile;
		};
		template <typename From, typename To>
		struct CloneCvref<From&, To>
		{
			using type = std::remove_cvref_t<To> &;
		};
		template <typename From, typename To>
		struct CloneCvref<From const &, To>
		{
			using type = std::remove_cvref_t<To>  const &;
		};
		template <typename From, typename To>
		struct CloneCvref<From&&, To>
		{
			using type = std::remove_cvref_t<To> &&;
		};
		template <typename From, typename To>
		struct CloneCvref<From const &&, To>
		{
			using type = std::remove_cvref_t<To> const &&;
		};
		template <typename From, typename To>
		struct CloneCvref<From volatile &, To>
		{
			using type = std::remove_cvref_t<To> volatile &;
		};
		template <typename From, typename To>
		struct CloneCvref<From volatile const &, To>
		{
			using type = std::remove_cvref_t<To> volatile const &;
		};
		template <typename From, typename To>
		struct CloneCvref<From volatile &&, To>
		{
			using type = std::remove_cvref_t<To> volatile &&;
		};
		template <typename From, typename To>
		struct CloneCvref<From volatile const &&, To>
		{
			using type = std::remove_cvref_t<To> volatile const &&;
		};
	}
	/**
	 * @brief Take the qualifiers and reference from `From` and apply it to `To`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam From The type to extract properties from.
	 * @tparam To The type to apply properties to.
	 */
	template <typename From, typename To>
	using CloneCvref = typename Impl::CloneCvref<From, To>::type;

	namespace Impl
	{
		template <typename From, typename To>
		struct CloneRef
		{
			using type = To;
		};
		template <typename From, typename To>
		struct CloneRef<From &, To>
		{
			using type = std::remove_reference_t<To> &;
		};
		template <typename From, typename To>
		struct CloneRef<From &&, To>
		{
			using type = std::remove_reference_t<To> &&;
		};
	}
	/**
	 * @brief Take the reference type of `From` and apply it to `To`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam From The type to extract properties.
	 * @tparam To the type to apply properties.
	 */
	template <typename From, typename To>
	using CloneRef = typename Impl::CloneRef<From, To>::type;

	namespace Impl
	{
		template <typename From>
		struct CloneCvrefPartial
		{
			template <typename To>
			using type = CloneCvref<From, To>;
		};
	}
	/**
	 * @brief Create a type that can mimick the const-volatile-reference of `From`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam From The type to mimick the const volatile properies of.
	 */
	template <typename From>
	using CloneCvrefPartial = Impl::CloneCvrefPartial<From>;
}