#pragma once
#include <type_traits>
#include <concepts>
#include <memory>
#include <utility>
#include <cstddef>

namespace Hz
{
	/**
	 * @brief Check if a type is a valid standard allocator.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Allocator = requires( typename std::allocator_traits<std::remove_cvref_t<T>>::template rebind_alloc<int> alloc, std::size_t n, int * ptr )
	{
		typename std::remove_cvref_t<T>::value_type;
		{ alloc.allocate( n ) } -> std::same_as<int *>;
		{ alloc.deallocate( ptr, n ) } -> std::same_as<void>;
		{ alloc == alloc } -> std::convertible_to<bool>;
		{ alloc != alloc } -> std::convertible_to<bool>;
		{ decltype( alloc ){ alloc } };
		{ decltype( alloc ){ std::move( alloc ) } };
	};
}