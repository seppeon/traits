#pragma once
#include "Hz/Traits/ConceptsStdLib.hpp"

namespace Hz::Cnt
{
	using ::Hz::Std::HasBegin;
	using ::Hz::Std::HasEnd;
	using ::Hz::Std::HasCBegin;
	using ::Hz::Std::HasCEnd;
	/**
	 * @brief Check for the presence a `.Reset()` member function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasReset = requires( T obj )
	{
		{ obj.Reset() };
	};
	/**
	 * @brief Check for the presence of a `.Clear()` member function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasClear = requires( T obj )
	{
		{ obj.Clear() };
	};
	/**
	 * @brief Check for the presence of `.PushBack(obj)`, where `obj` is a `typename Obj::value_type`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasPushBack = requires( T obj, typename std::remove_cvref_t<T>::value_type value )
	{
		{ obj.PushBack( value ) } -> std::same_as<bool>;
	};
	/**
	 * @brief Check if the presence of the `.EmplaceBack(args...)` member function.
	 * 
	 * @ingroup Concepts
	 * @tparam C The type of the container.
	 * @tparam Args... The type of the applied arguments.
	 */
	template<typename C, typename... Args>
	concept HasEmplaceBack = requires( C obj, Args &&... args )
	{
		{ obj.EmplaceBack( FWD( args )... ) } noexcept->std::same_as<bool>;
	};
	/**
	 * @brief Check for the presence of the `.Emplace(args...)` member function.
	 * 
	 * @ingroup Concepts
	 * @tparam C The container type.
	 * @tparam Args... The type of the arguments.
	 */
	template<typename C, typename... Args>
	concept HasEmplace = requires( C obj, Args &&... args )
	{
		{ obj.Emplace( FWD( args )... ) } noexcept->std::same_as<bool>;
	};
	/**
	 * @brief Check for the presense of the `.Push(data)` member function, where `data` is of `typename T::value_type` value type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasPush = requires( T obj, typename std::remove_cvref_t<T>::value_type value )
	{
		{ obj.Push( value ) } noexcept->std::same_as<bool>;
	};
	/**
	 * @brief Check for the presense of the `.Insert(value)` member function, where `value` is of type `typename T::value_type`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasInsert = requires( T obj, typename std::remove_cvref_t<T>::value_type value )
	{
		{ obj.Insert( value ) } noexcept->std::same_as<bool>;
	};
	/**
	 * @brief Check for the presense of the `obj.Insert(obj.end(), value)` member function, where `value` is of type `typename T::value_type`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasIteratorInsert = requires( T obj, typename std::remove_cvref_t<T>::value_type value )
	{
		{ obj.Insert( obj.end(), value ) } noexcept->std::same_as<bool>;
	};
	/**
	 * @brief Check for the presense of the `obj.Back()` member function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasBack = requires( T obj )
	{
		{ obj.Back() } -> Ref;
	};
	/**
	 * @brief Check for the presense of the `obj.Front()` member function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasFront = requires( T obj )
	{
		{ obj.Front() } -> Ref;
	};
	/**
	 * @brief Check for the presense of the `obj.HasValue()` member function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasHasValue = requires( T obj )
	{
		{ obj.HasValue() } noexcept->Bool;
	};
	/**
	 * @brief Check for the presense of the `obj.Value()` member function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasValue = requires( T obj )
	{
		typename std::remove_cvref_t<T>::value_type;
		{ obj.Value() } noexcept;
	};
	/**
	 * @brief Check for the presense of the `obj.Get()` member function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasGet = requires( T obj )
	{
		typename std::remove_cvref_t<T>::value_type;
		{ obj.Get() } noexcept->std::same_as<typename std::remove_cvref_t<T>::value_type &>;
	};
	/**
	 * @brief Check for the presense of the `.Get<0>()` member function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasBindingGet = requires( T obj )
	{
		{ obj.template Get<0>() } noexcept->Ref;
	};
	/**
	 * @brief Check for the presense of a `.Data()` member function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasData = requires( T obj )
	{
		{ obj.Data() } noexcept->Ptr;
	};
	/**
	 * @brief Check for the presense of a `.Size()` member function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasSize = requires( T & obj )
	{
		{ obj.Size() } -> UnsignedInt;
	};
	/**
	 * @brief Check for the presense of the `.Empty()` member function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasEmpty = requires( T obj )
	{
		{ obj.Empty() } -> std::same_as<bool>;
	};
	/**
	 * @brief Check for the presense of the `.Capacity()` member function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasCapacity = requires( T obj )
	{
		{ obj.Capacity() } -> std::same_as<typename std::remove_cvref_t<T>::size_type>;
	};
	/**
	 * @brief Check for the presense of the `.PushFront(value)` member function, where `value` is a `typename T::value_type`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasPushFront = requires( T obj, typename std::remove_cvref_t<T>::value_type const & value )
	{
		{ obj.PushFront( value ) };
	};
	/**
	 * @brief Check for the presense of the `.Fill(value)` member function, where `value` is a `typename T::value_type`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasFill = requires( T obj, typename std::remove_cvref_t<T>::value_type const & value )
	{
		{ obj.Fill( value ) };
	};
	/**
	 * @brief Check for the presence of the `.EmplaceFront(args...)` member function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 * @tparam Args... The arguments to try with the emplace function.
	 */
	template<typename T, typename... Args>
	concept HasEmplaceFront = requires( T obj, Args &&... args )
	{
		{ obj.EmplaceFront( FWD( args )... ) };
	};
	/**
	 * @brief Check for the presense of `.Reserve(value)`, where `value` is a `typename T::value_type`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasReserve = requires( T obj, typename std::remove_cvref_t<T>::size_type index )
	{
		{ obj.Reserve( index ) };
	};
	/**
	 * @brief Check for the presense of `.Resize(len, args...)`, where `len` is a `std::size_t` and `args...` are the arguments to construct the elements.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 * @tparam Args... The arguments to apply to construct the new elements.
	 */
	template<typename T, typename ... Args>
	concept HasResize = requires( T obj, typename T::size_type len, Args ... args )
	{
		{ obj.Resize( len, args... ) };
	};
	/**
	 * @brief Check if PopFront is avaliable.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasPopFront = requires( T obj )
	{
		{ obj.PopFront() };
	};
	/**
	 * @brief Check if `.Pop()` is avaliable.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasPop = requires( T obj )
	{
		{ obj.Pop() };
	};
	/**
	 * @brief Check if `.MaxSize()` is avaliable.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasMaxSize = requires( T obj )
	{
		{ obj.MaxSize() } -> std::same_as<typename std::remove_cvref_t<T>::size_type>;
	};
	/**
	 * @brief Check if `.At(index)` is avaliable, where index is a `typename T::size_type`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasAt = requires( T obj, typename std::remove_cvref_t<T>::size_type index )
	{
		{ obj.At( index ) };
	};
	/**
	 * @brief Check if the index erase function `.Erase(index)` is avaliable.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasEraseOne = requires( T obj, typename std::remove_cvref_t<T>::size_type index )
	{
		{ obj.Erase( index ) };
	};
	/**
	 * @brief Check if a range of indexes can be removal member function `.Erase(index, length)` is avaliable.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasEraseRange = requires( T obj, typename std::remove_cvref_t<T>::size_type index )
	{
		{ obj.Erase( index, index ) };
	};

	// Has ForEach(index, item)
	namespace Impl
	{
		struct ForEachGenericCallback
		{
			template<std::size_t I, typename T>
			constexpr void operator()( std::integral_constant<std::size_t, I>, T & ) noexcept
			{
			}
			template<std::size_t I, typename T>
			constexpr void operator()( std::integral_constant<std::size_t, I>, T const & ) const noexcept
			{
			}
		};
	}
	/**
	 * @brief Check if the member 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasForEach = requires( T obj, typename T::value_type value, Impl::ForEachGenericCallback & lcallback, Impl::ForEachGenericCallback && rcallback )
	{
		{ obj.ForEach( lcallback ) };
		{ obj.ForEach( rcallback ) };
	};
}