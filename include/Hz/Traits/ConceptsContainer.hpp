#pragma once
#include "Hz/Traits/RegType.hpp"
#include "Hz/Traits/ConceptsIterator.hpp"

namespace Hz
{
	/**
	 * @brief Check if a container is an iterable container.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept IterableContainer = not HasRegString<T> and ForIterable<T>;
	/**
	 * @brief Check if a container is an indexable container.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept IndexableContainer = not HasRegString<T> and Indexable<T>;
	/**
	 * @brief Check if a container is and indexable/iterable container.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template <typename T>
	concept Container = IterableContainer<T> or IndexableContainer<T>;
}