#pragma once
#include "Hz/Traits/FnTraits.hpp"
#include <Hz/TL/TLConvertible.hpp>
#include <concepts>

namespace Hz
{
	/**
	 * @brief Check if a type is a function object.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasFnObject = not std::same_as<void, GetFnObject<T>>;
	/**
	 * @brief Check if a type is an abominable function type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept FnAbominable = Impl::FnTraits<T>::is_abominable;
	/**
	 * @brief Check if a type is a member function type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept FnMember = Impl::FnTraits<T>::is_member;
	/**
	 * @brief Check if a type is a free function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept IsFreeFn = not FnMember<T>;
	/**
	 * @brief Check if the function type is a const function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept FnConst = Impl::FnTraits<T>::is_const;
	/**
	 * @brief Check if the function is a mutable function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept FnMutable = Impl::FnTraits<T>::is_mutable;
	/**
	 * @brief Check if the function is a volatile function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept FnVolatile = Impl::FnTraits<T>::is_volatile;
	/**
	 * @brief Check if the function is a `noexcept` function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept FnNoexcept = Impl::FnTraits<T>::is_noexcept;
	/**
	 * @brief Check if the function is lvalue reference qualified.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept FnLvalue = Impl::FnTraits<T>::is_lvalue;
	/**
	 * @brief Check if the function is rvalue refernece qualified.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept FnRvalue = Impl::FnTraits<T>::is_rvalue;
	/**
	 * @brief Check if the function has the same arguments as another function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check against the arguments of the reference function.
	 * @tparam Fn The reference function.
	 */
	template<typename T, typename Fn>
	concept FnSameArgs = std::same_as<GetFnArgs<T>, GetFnArgs<Fn>>;
	/**
	 * @brief Check if the function has the same result as another reference function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 * @tparam Fn The reference function.
	 */
	template<typename T, typename Fn>
	concept FnSameResult = std::same_as<GetFnResult<T>, GetFnResult<Fn>>;
	/**
	 * @brief Check if two functions have the same arguments and results.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 * @tparam Fn The reference function.
	 */
	template<typename T, typename Fn>
	concept FnSame = FnSameArgs<T, Fn> and FnSameResult<T, Fn>;
	/**
	 * @brief Check if the arguments of the checked function can be converted to the refernece function `Fmt`.
	 * 
	 * @ingroup Concepts
	 * @tparam F The type to check.
	 * @tparam Fmt The refernece function.
	 */
	template<typename F, typename Fmt>
	concept ConvertibleArgsFn = TLConvertible<GetFnArgs<F>, GetFnArgs<Fmt>>;
	/**
	 * @brief Check if the results of the checked function can be converted to the refernece function `Fmt`.
	 * 
	 * @ingroup Concepts
	 * @tparam F The type to check.
	 * @tparam Fmt The refernece function.
	 */
	template<typename F, typename Fmt>
	concept ConvertibleResultFn = std::is_convertible_v<GetFnResult<F>, GetFnResult<Fmt>>;
	/**
	 * @brief Check if a functions arguments and results can be converted to the refernece function `Fmt`s arguments and results.
	 * 
	 * @ingroup Concepts
	 * @tparam F The type to check.
	 * @tparam Fmt The refernece function.
	 */
	template<typename F, typename Fmt>
	concept ConvertibleFn = ConvertibleArgsFn<F, Fmt> and ConvertibleResultFn<F, Fmt>;
}