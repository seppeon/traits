#pragma once
#include <ranges>
#include <concepts>
#include <iterator>

namespace Hz
{
	/**
	 * @brief Check if a particular iterator is supports the most basic operations required for a range based for loop.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept ForIterator = requires( T && iter )
	{
		{ ++iter };
		{ *iter };
		{ iter != iter };
	};

	namespace Impl
	{
		using std::begin; 
		using std::end;
		using std::convertible_to;

		template<typename T>
		concept ForIterable = requires( T && iter )
		{
			{ begin( iter ) } -> ForIterator;
			{ end( iter ) };
			{ begin( iter ) != end( iter ) } -> convertible_to<bool>;
		};
	}
	/**
	 * @brief Check if some type can be directly used with a range based for loop.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept ForIterable = Impl::ForIterable<T>;
	/**
	 * @brief Check if some type is a `std::input_iterator`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept InputIterable = std::input_iterator<std::ranges::iterator_t<T>>;
	/**
	 * @brief Check if some type is a `std::forward_iterator`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept ForwardIterable = std::forward_iterator<std::ranges::iterator_t<T>>;
	/**
	 * @brief Check if some type is a `std::bidirectional_iterator`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept BidirectionalIterable = std::bidirectional_iterator<std::ranges::iterator_t<T>>;
	/**
	 * @brief Check if some type is a `std::random_access_iterator`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept RandomAccessIterable = std::random_access_iterator<std::ranges::iterator_t<T>>;
	/**
	 * @brief Check if some type is a `std::contiguous_iterator`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept ContiguousIterable = std::contiguous_iterator<std::ranges::iterator_t<T>>;
	/**
	 * @brief Check if some type can be indexed with `obj[i]`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Indexable = requires( T obj, std::size_t i )
	{
		{ obj[i] };
	};
}