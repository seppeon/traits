#pragma once
#include "Hz/Traits/RegType.hpp"
#include "Hz/Traits/ConceptsStd.hpp"

namespace Hz
{
	/**
	 * @brief Check if some type is nullable.
	 *
	 * A nullable type is one that has a null state, such as std::optional.
	 * These functions typically have a `.has_value()` function.
	 *
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Nullable = Hz::HasRegNullable<T> and Hz::HasRegValue<T> and HasRegHasValue<T> and Boolable<T>;
}