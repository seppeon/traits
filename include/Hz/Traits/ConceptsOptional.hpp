#pragma once
#include "Hz/Traits/ConceptsStd.hpp"
#include "Hz/Traits/ConceptsStdLib.hpp"
#include "Hz/Traits/RegType.hpp"
#include <type_traits>

namespace Hz
{
	/**
	 * @brief Check if a type is constructible from the dereferenced value.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template <typename T>
	concept ConstructableFromDeref = requires( T foo )
	{
		{ std::remove_cvref_t<T>{ *foo } };
	};
	/**
	 * @brief Check if a type is pre-incrementable.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template <typename T>
	concept PreIncrementable = requires( T foo )
	{
		{ ++foo };
	};
	/**
	 * @brief Check if a type is an optional type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Optional = (
		Std::HasValueType<T>
		and Boolable<T>
		and ConstructableFromDeref<T>
		and DefaultConstructible<std::remove_reference_t<T>>
		and not PreIncrementable<std::remove_cvref_t<T>>
	);
	/**
	 * @brief Register any type that looks like a optional-like types as nullable.
	 * 
	 * @tparam T The optional type.
	 */
	template <Optional T>
	struct RegNullable<T> : std::true_type {};
}