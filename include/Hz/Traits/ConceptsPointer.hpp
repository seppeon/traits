#pragma once
#include "Hz/Traits/Concepts.hpp"
#include "Hz/Traits/RegType.hpp"
#include "Hz/Traits/ConceptsStdLib.hpp"

namespace Hz
{
	/**
	 * @brief Check if a type looks like a pointer.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template <typename T>
	concept PointerLike = requires( T foo )
	{
		{ *foo };
		{ foo == nullptr };
		{ std::remove_cvref_t<T>{ &( *foo ) } };
		{ std::remove_cvref_t<T>{ nullptr } };
	};
	/**
	 * @brief Check if a type looks like a smart pointer.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template <typename T>
	concept SmartPointer = PointerLike<T> and Std::HasElementType<T>;
	/**
	 * @brief Register any type that looks like a smart pointer as nullable.
	 * 
	 * @tparam T The smart pointer type.
	 */
	template <SmartPointer T>
	struct RegNullable<T> : std::true_type {};
}