#pragma once
#include "Hz/Traits/ConceptsStd.hpp"
#include "Hz/Traits/ConceptsStdLib.hpp"
#include "Hz/Traits/ConceptsProperty.hpp"
#include "Hz/Traits/ConceptsNullable.hpp"
#include "Hz/Traits/ConceptsIterator.hpp"
#include "Hz/Traits/ConceptsTuple.hpp"
#include "Hz/Traits/ConceptsContainer.hpp"
#include "Hz/Traits/ConceptsTranscodable.hpp"

namespace Hz
{
	/**
	 * @brief Check if a type is a property and is registered as a variant type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept VariantProperty = Property<T> and HasRegVariant<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a nullable type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept NullableProperty = Property<T> and Nullable<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a string type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept StringProperty = Property<T> and HasRegString<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a float type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept FloatProperty = Property<T> and Float<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a integer property type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept IntegerProperty = Property<T> and Integer<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a signed property type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept SignedProperty = Property<T> and Signed<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a unsigned property type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept UnsignedProperty = Property<T> and Unsigned<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a char property type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept CharProperty = Property<T> and Char<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a bool property type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept BoolProperty = Property<T> and Bool<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a signed integer property type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept SignedIntProperty = Property<T> and SignedInt<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a unsigned integer property type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept UnsignedIntProperty = Property<T> and UnsignedInt<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a enum property type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept EnumProperty = Property<T> and Enum<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a object property type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept ObjectProperty = Property<T> and Object<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a container property type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept ContainerProperty = Property<T> and Container<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a contiguous iterable property type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept ContiguousIterableProperty = Property<T> and ContiguousIterable<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a for-iterable property type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept ForIterableProperty = Property<T> and ForIterable<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a trivially copyable property type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept TriviallyCopyableProperty = Property<T> and TriviallyCopyable<typename std::remove_cvref_t<T>::value_type>;
	/**
	 * @brief Check if a type is a property and is a key-value property type.
	 *
	 * @note This is not a true property, it is a pair of two data members, each of which are properties. 
	 *
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept KeyValueProperty = requires( T obj )
	{
		typename std::remove_cvref_t<T>::value_property;
		typename std::remove_cvref_t<T>::key_property;
		{ obj.key } -> StringProperty;
		{ obj.value } -> Property;
	};
	/**
	 * @brief Check if a type is a property and is a `Hz::TupleLike` property type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept TupleLikeProperty = (
		Property<T> and
		TupleLike<typename T::value_type>
	);
	/**
	 * @brief Check if a type is a property and is a `Hz::Transcodable` property type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T, typename F>
	concept TranscodableProperty = (
		Property<T> and
		Transcodable<typename std::remove_cvref_t<T>::value_type, F> and
		DefaultConstructible<std::remove_cvref_t<T>>
	);
}