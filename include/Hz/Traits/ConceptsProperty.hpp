#pragma once
#include "Hz/Traits/ConceptsStdLib.hpp"
#include <type_traits>
#include <concepts>

namespace Hz
{
	/**
	 * @brief Check if a property(`D`) is constructible 
	 * 
	 * @ingroup Concepts
	 * @tparam D The type that should be constructible from the construct type.
	 */
	template<typename D>
	concept PropertyConstructible = requires( typename std::remove_cvref_t<D>::construct_type value )
	{
		{ D{ value } };
	};
	/**
	 * @brief Check if a type has the `.Settable()` method required by properties.
	 * 
	 * @ingroup Concepts
	 * @tparam D The type to check.
	 */
	template<typename D>
	concept ProperyHasStaticSettable = requires( D obj )
	{
		{ D::Settable() } noexcept -> std::same_as<bool>;
	};
	/**
	 * @brief Check if a a type is both settable and has the set method required by properties.
	 * 
	 * @ingroup Concepts
	 * @tparam D The type to check.
	 */
	template<typename D>
	concept PropertyTrueSettable = (
		std::remove_cvref_t<D>::Settable() and
		requires( std::remove_cvref_t<D> obj, typename std::remove_cvref_t<D>::set_type const & value )
		{
			{ obj.Set( value ) } noexcept -> std::same_as<bool>;
		}
	);
	/**
	 * @brief Check if a type needs a `obj.Set(value)` method, if that type is treated as a property.
	 * 
	 * @ingroup Concepts
	 * @tparam D The type to check.
	 */
	template<typename D>
	concept PropertySetNotNeeded = not D::Settable();
	/**
	 * @brief Check if the required set of set functions are present for a property.
	 *
	 * When `D::Settable()` is present and true:
	 * @li The type must have a `obj.Set(value)`.
	 *
	 * When `D::Settable()` is present and false:
	 * @li The function `obj.Set(value)` is not required.
	 *
	 * @ingroup Concepts
	 * @tparam D The type to check.
	 */
	template<typename D>
	concept PropertyDeclaresSettability = (
		ProperyHasStaticSettable<D> and
		( PropertySetNotNeeded<D> or PropertyTrueSettable<D> )
	);
	/**
	 * @brief Check if a type has the needed getter functions for a valid property.
	 * 
	 * When `D::Gettable()` is static, present and true:
	 * @li The type must have a `obj.Get(value)`.
	 *
	 * When `D::Gettable()` is static, present and false:
	 * @li The function `obj.Get(value)` is not required.
	 *
	 * When `obj.Gettable()` is non-static:
	 * @li The function `obj.Get(value)` is required.
	 *
	 * @ingroup Concepts
	 * @tparam D The type to check.
	 */
	template<typename D>
	concept PropertyDeclaresGettability =
	(
		// Must declare gettable which is minimally a member function (can be static too).
		requires( D obj ) { { obj.Gettable() } noexcept->std::same_as<bool>; } and
		(
			// If we can statically determine that its not gettable,
			// no point trying out the get function.
			not
			(
				// Test if the property is always gettable.
				requires { { D::Gettable() } noexcept -> std::same_as<bool>; } and
				D::Gettable()
			)
			// if it isn't always gettable, it must have Get and get_type
			or requires( D obj )
			{
				typename D::get_type;
				{ obj.Get() } noexcept->std::convertible_to<typename D::get_type>;
			}
		)
	);
	/**
	 * @brief Check if a type is a valid property.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T, typename D = std::remove_cvref_t<T>>
	concept Property =
	(
		// Properties must declare how they are constructed.
		PropertyConstructible<D> and
		// Properties for setters.
		PropertyDeclaresSettability<D> and
		// Properties for getters.
		PropertyDeclaresGettability<D> and
		// Properties must have a value type, this is the type used for concepts on the property type.
		Hz::Std::HasValueType<D>
	);
}