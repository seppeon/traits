#pragma once
#include <type_traits>
#include <concepts>

namespace Hz
{
	/**
	 * @brief A concept for `std::is_trivially_copyable_v<T>`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept TriviallyCopyable = std::is_trivially_copyable_v<T>;
	/**
	 * @brief A concept for `std::is_fundamental_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Fundamental = std::is_fundamental_v<T>;
	/**
	 * @brief A concept for `not std::is_aggregate_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept NotAggregate = not std::is_aggregate_v<T>;
	/**
	 * @brief A concept for `std::is_aggregate_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Aggregate = std::is_aggregate_v<T>;
	/**
	 * @brief A concept for `std::is_default_constructible_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept DefaultConstructible = std::is_default_constructible_v<T>;
	/**
	 * @brief A concept for `std::is_array_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Array = std::is_array_v<T>;
	/**
	 * @brief A concept for `std::is_copy_constructible_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept CopyConstructible = std::is_copy_constructible_v<T>;
	/**
	 * @brief A concept for `std::is_const_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Const = std::is_const_v<T>;
	/**
	 * @brief A concept for `std::is_volatile_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Volatile = std::is_volatile_v<T>;
	/**
	 * @brief A concept for `std::is_rvalue_reference_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept RValue = std::is_rvalue_reference_v<T>;
	/**
	 * @brief A concept for `std::is_lvalue_reference_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept LValue = std::is_lvalue_reference_v<T>;
	/**
	 * @brief A concept for `std::is_reference_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Ref = std::is_reference_v<T>;
	/**
	 * @brief A concept for `std::is_pointer_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Ptr = std::is_pointer_v<T>;
	/**
	 * @brief Check if a type `T` is a pointer and the pointer is convertible to a pointer to Obj.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 * @tparam Obj The type that is pointed to.
	 */
	template<typename T, typename Obj>
	concept PtrTo = std::is_pointer_v<T> and std::is_convertible_v<T, std::add_pointer_t<Obj>>;
	static_assert(PtrTo<char *, const char>);
	/**
	 * @brief A concept for `std::is_arithmetic_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Arithmetic = std::is_arithmetic_v<T>;
	/**
	 * @brief A concept for `std::is_integral_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Integral = std::is_integral_v<T>;
	/**
	 * @brief A concept for `std::is_floating_point_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Float = std::is_floating_point_v<T>;
	/**
	 * @brief A concept for `std::is_same_v<T, char>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Char = std::is_same_v<T, char>;
	/**
	 * @brief A concept for `std::is_same_v<T, bool>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Bool = std::is_same_v<T, bool>;
	/**
	 * @brief A concept that checks if a type is an integer (as opposed to an `std::integral_v`).
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Integer = std::is_integral_v<T> and not Bool<T> and not Char<T>;
	/**
	 * @brief A concept for `std::is_signed_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Signed = std::is_signed_v<T>;
	/**
	 * @brief A concept for `std::is_unsigned_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Unsigned = std::is_unsigned_v<T>;
	/**
	 * @brief A concept for checking a signed integer. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept SignedInt = Integer<T> and Signed<T>;
	/**
	 * @brief A concept for checking an unsigned integer. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept UnsignedInt = Integer<T> and Unsigned<T>;
	/**
	 * @brief A concept for checking a number (signed int, unsigned int, or float). 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename P>
	concept Number = ( Float<P> or UnsignedInt<P> or SignedInt<P> );
	/**
	 * @brief A concept for `std::is_enum_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Enum = std::is_enum_v<T>;
	/**
	 * @brief A concept for `std::is_object_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Object = std::is_object_v<T>;
	/**
	 * @brief A concept for `std::is_class_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Class = std::is_class_v<T>;
	/**
	 * @brief A concept for `std::is_void_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Void = std::is_void_v<T>;
	/**
	 * @brief A concept for `not std::is_void_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept NonVoid = not Void<T>;
	/**
	 * @brief A concept for `not std::is_const_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Mutable = not Const<T>;
	/**
	 * @brief A concept for `std::is_member_function_pointer_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename F>
	concept MemFn = std::is_member_function_pointer_v<F>;
	/**
	 * @brief A concept for `std::is_base_of_v<U, T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T, typename U>
	concept Derived = std::is_base_of_v<U, T>;
	/**
	 * @brief A concept for `std::is_member_function_pointer_v<T>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept MemberFunctionPointer = std::is_member_function_pointer_v<T>;
	/**
	 * @brief A concept for checking if a type is a function pointer.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept FunctionPointer = not std::is_pointer_v<T> and std::is_function_v<std::remove_cv_t<std::remove_pointer_t<T>>>;
	/**
	 * @brief A concept for checking if a `obj` has a dereference operator.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasDeref = requires( T obj ) { { *obj }; };
	/**
	 * @brief A concept for `std::convertible_to<T, bool>`. 
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept Boolable = std::convertible_to<T, bool>;
}