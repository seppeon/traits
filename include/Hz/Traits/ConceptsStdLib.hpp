#pragma once
#include "Hz/Traits/ConceptsStd.hpp"
#include <array>

namespace Hz::Std
{
	// Required to both support ADL and std::*
	using std::data, std::size, std::empty, std::get;
	using std::begin, std::end, std::cbegin, std::cend;
	using std::get;
	/**
	 * @brief Check for the presence of the member type `value_type`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasValueType = requires
	{
		typename std::remove_cvref_t<T>::value_type;
	};
	/**
	 * @brief Check for the presence of the member type `reference`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasRefType = requires
	{
		typename std::remove_cvref_t<T>::reference;
	};
	/**
	 * @brief Check for the presence of the member type `const_reference`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasConstRefType = requires
	{
		typename std::remove_cvref_t<T>::const_reference;
	};
	/**
	 * @brief Check for the presence of the member type `element_type`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasElementType = requires
	{
		typename std::remove_cvref_t<T>::element_type;
	};
	/**
	 * @brief Check for the presence of the ADL method `get<0>(obj)` (including support for the the std::get function).
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasBindingGet = requires( T && obj )
	{
		{ get<0>( obj ) } noexcept->Ref;
	};
	/**
	 * @brief Check for the presence of the ADL method `begin(obj)` (including support for the the std::begin function).
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasBegin = requires( T && obj )
	{
		{ begin( obj ) };
	};
	/**
	 * @brief Check for the presence of the ADL method `end(obj)` (including support for the the std::end function).
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasEnd = requires( T && obj )
	{
		{ end( obj ) };
	};
	/**
	 * @brief Check for the presence of the ADL method `cbegin(obj)` (including support for the the std::cbegin function).
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasCBegin = requires( T && obj )
	{
		{ cbegin( obj ) };
	};
	/**
	 * @brief Check for the presence of the ADL method `cend(obj)` (including support for the the std::cend function).
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasCEnd = requires( T && obj )
	{
		{ cend( obj ) };
	};
	/**
	 * @brief Check for the presence of the ADL method `empty(obj)` (including support for the the std::empty function).
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasEmpty = requires( T && obj )
	{
		{ empty( obj ) } -> std::same_as<bool>;
	};
	/**
	 * @brief Check for the presence of the member function `obj.capacity()`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasCapacity = requires( T && obj )
	{
		{ obj.capacity() } -> std::same_as<typename T::size_type>;
	};
	/**
	 * @brief Check for the presence of the member function `obj.fill(value)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasFill = requires( T && obj, typename std::remove_cvref_t<T>::value_type value )
	{
		{ obj.fill( value ) };
	};
	/**
	 * @brief Check for the presence of the member function `obj.emplace_front(value)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasEmptyFront = requires( T && obj, typename std::remove_cvref_t<T>::value_type value )
	{
		{ obj.emplace_front( value ) };
	};
	/**
	 * @brief Check for the presence of the member function `obj.reserve(value)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasReserve = requires( T && obj, typename std::remove_cvref_t<T>::size_type value )
	{
		{ obj.reserve( value ) };
	};
	/**
	 * @brief Check for the presence of the member function `obj.resize(value, args...)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T, typename ... Args>
	concept HasResize = requires( T && obj, typename std::remove_cvref_t<T>::size_type value, Args ... args )
	{
		{ obj.resize( value, args... ) };
	};
	/**
	 * @brief Check for the presence of the member function `obj.pop_back()`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasPopBack = requires( T && obj )
	{
		{ obj.pop_back() };
	};
	/**
	 * @brief Check for the presence of the member function `obj.pop_front()`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasPopFront = requires( T && obj )
	{
		{ obj.pop_front() };
	};
	/**
	 * @brief Check for the presence of the member function `obj.pop()`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasPop = requires( T && obj )
	{
		{ obj.pop() };
	};
	/**
	 * @brief Check for the presence of the member function `obj.max_size()`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasMaxSize = requires( T && obj )
	{
		{ obj.max_size() } -> std::same_as<typename std::remove_cvref_t<T>::size_type>;
	};
	/**
	 * @brief Check for the presence of the member function `obj.at(index)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasAt = requires( T && obj, typename std::remove_cvref_t<T>::size_type index )
	{
		{ obj.at( index ) };
	};
	/**
	 * @brief Check for the presence of the member function `obj.erase(iterator)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasEraseOne = requires( T && obj, typename std::remove_cvref_t<T>::iterator iter )
	{
		{ obj.erase( iter ) };
	};
	/**
	 * @brief Check for the presence of the member function `obj.erase(iterator_begin, iterator_end)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasEraseRange = requires( T && obj, typename std::remove_cvref_t<T>::iterator beg, typename std::remove_cvref_t<T>::iterator en )
	{
		{ obj.erase( beg, en ) };
	};
	/**
	 * @brief Check for the presence of the member function `obj.compare(v)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 * @tparam B The type to compare with.
	 */
	template<typename T, typename B>
	concept HasCompare = requires( T && obj, B v )
	{
		{ obj.compare( v ) };
	};
	/**
	 * @brief Check for the presence of the member function `obj.has_value()`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasHasValue = requires( T && obj )
	{
		typename std::remove_cvref_t<T>::value_type;
		{ obj.has_value() } noexcept -> Bool;
	};
	/**
	 * @brief Check for the presence of the member function `obj.value()`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasValue = requires( T && obj )
	{
		{ obj.value() };
	};
	/**
	 * @brief Check for the presence of the member function `obj.reset()`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasReset = requires( T && obj )
	{
		{ obj.reset() };
	};
	/**
	 * @brief Check for the presence of the member function `obj.clear()`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasClear = requires( T && obj )
	{
		{ obj.clear() };
	};
	/**
	 * @brief Check for the presence of the ADL function `data(obj)`, including std::data.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasData = requires( T && obj )
	{
		{ data( obj ) } -> Ptr;
	};
	/**
	 * @brief Check for the presence of the ADL function `size(obj)`, including std::size.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasSize = requires( T && obj )
	{
		{ size( obj ) } -> std::convertible_to<std::size_t>;
	};
	/**
	 * @brief Check for the presense of the member function `obj.push_back(value)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasPushBack = requires( T && obj, typename T::value_type const & value )
	{
		{ obj.push_back( value ) };
	};
	/**
	 * @brief Check for the presense of the member function `obj.push_front(value)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasPushFront = requires( T && obj, typename T::value_type const & value )
	{
		{ obj.push_front( value ) };
	};
	/**
	 * @brief Check for the presense of the member function `obj.emplace_back(args...)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename C, typename... Args>
	concept HasEmplaceBack = requires( C obj, Args &&... args )
	{
		{ obj.emplace_back( static_cast<Args&&>( args )... ) } -> Ref;
	};
	/**
	 * @brief Check for the presense of the member function `obj.emplace_front(args...)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename C, typename... Args>
	concept HasEmplaceFront = requires( C obj, Args &&... args )
	{
		{ obj.emplace_front( static_cast<Args&&>( args )... ) } -> Ref;
	};
	/**
	 * @brief Check for the presense of the map member function `obj.emplace(args...)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T, typename... Args>
	concept HasMapEmplace = requires( T && obj, Args &&... args )
	{
		{ *obj.emplace( static_cast<Args&&>( args )... ).first } -> Ref;
		{ obj.emplace( static_cast<Args&&>( args )... ).second } -> std::convertible_to<bool>;
	};
	/**
	 * @brief Check for the presense of the member function `obj.emplace(args...)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T, typename... Args>
	concept HasEmplace = requires( T && obj, Args &&... args )
	{
		{ obj.emplace( static_cast<Args&&>( args )... ) } -> Ref;
	};
	/**
	 * @brief Check for the presense of the member function `obj.push(value)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasPush = requires( T && obj, typename std::remove_cvref_t<T>::value_type value )
	{
		{ obj.push( value ) };
	};
	/**
	 * @brief Check for the presense of the member function `obj.insert(value)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasInsert = requires( T && obj, typename std::remove_cvref_t<T>::value_type value )
	{
		{ obj.insert( value ) };
	};
	/**
	 * @brief Check for the presense of the member function `obj.insert(iterator, value)`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasIteratorInsert = requires( T && obj, typename std::remove_cvref_t<T>::value_type value )
	{
		{ obj.insert( obj.end(), value ) };
	};
	/**
	 * @brief Check for the presense of the member function `obj.back()`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasBack = requires( T && obj )
	{
		{ obj.back() } -> Ref;
	};
	/**
	 * @brief Check for the presense of the member function `obj.front()`.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasFront = requires( T && obj )
	{
		{ obj.front() } -> Ref;
	};
}