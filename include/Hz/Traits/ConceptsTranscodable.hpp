#pragma once
#include <type_traits>

namespace Hz
{
	/**
	 * @brief A customization point for providing transcoders to 3rd party types.
	 * 
	 * @tparam T The type to check.
	 * @tparam F The formatter to use with the transcoder.
	 */
	template <typename T, typename F>
	struct CustomTranscoder;
}

namespace Hz
{
	/**
	 * @brief Check if the type supports an ADL `Transcoder` function, which can be used to provide a transcoder.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 * @tparam F The formatter to use with the transcoder.
	 */
	template<typename T, typename F>
	concept AdlTranscodable = requires( T obj )
	{
		{ Transcoder<F>( obj ) } noexcept;
	};
	/**
	 * @brief Check if the type supports a user specialized `Hz::CustomTranscoder` type, which can be used to provide a transcoder.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 * @tparam F The formatter to use with the transcoder.
	 */
	template <typename T, typename F>
	concept CustomTranscodable = requires( T obj )
	{
		{ CustomTranscoder<std::remove_cvref_t<T>, F>::Transcoder( obj ) } noexcept;
	};
	/**
	 * @brief Check if a type is transcodable.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check. 
	 * @tparam F The formatter to use with the transcoder.
	 */
	template<typename T, typename F>
	concept Transcodable = (AdlTranscodable<T, F> or CustomTranscodable<T, F>);
}