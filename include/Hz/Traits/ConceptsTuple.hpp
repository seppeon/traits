#pragma once
#include "Hz/Traits/ConceptsStdLib.hpp"
#include <cstddef>
#include <array>
#include <type_traits>

namespace Hz
{
	namespace Impl
	{
		template <typename>
		struct AllTypes { static constexpr bool value = false; };
		template <template <typename...> class W, typename...Args>
		struct AllTypes<W<Args...>> { static constexpr bool value = true; };
	}
	/**
	 * @brief Check if all template arguments of the type `Obj` are types (and not values, concepts or inline variables).
	 * 
	 * @ingroup Concepts
	 * @tparam Obj The type to check.
	 */
	template <typename Obj>
	concept AllTypeTemplate = Impl::AllTypes<std::remove_cvref_t<Obj>>::value;
	/**
	 * @brief Check if the tuple protocol specializations have been made for a tuple-like type.
	 * 
	 * @ingroup Concepts
	 * @tparam Obj The object to check.
	 */
	template <typename Obj, typename D = std::remove_cvref_t<Obj>>
	concept TupleProtocolSpecializations = requires
	{
		{ std::tuple_size<D>::value } -> std::convertible_to<std::size_t>;
		typename std::tuple_element<std::size_t(0), D>::type;
	};
	/**
	 * @brief Check if a type is a tuple-like object.
	 * 
	 * @ingroup Concepts
	 * @tparam Obj The object to check.
	 */
	template<typename Obj>
	concept TupleLike = Std::HasBindingGet<Obj> and AllTypeTemplate<Obj> and TupleProtocolSpecializations<Obj>;
}