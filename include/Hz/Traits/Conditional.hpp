#pragma once
#include <type_traits>

namespace Hz
{
	/**
	 * @brief Resolve some type based on the conditional evaluation of some callable.
	 *
	 * @note Unlike std::conditional, this does not require instantiation of the left and right hand options.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam R The result of some conditional expression.
	 * @tparam Eval The callable that will return a value of the type to be returned from `Cond`.
	 */
	template <bool R, auto Eval>
	using Cond = decltype(Eval(std::integral_constant<bool, R>{}));
}