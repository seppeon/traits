#pragma once
#include <type_traits>

namespace Hz
{
	namespace Impl
	{
		template <bool, typename>
		struct ConstIf;
		template <typename T>
		struct ConstIf<true, T>
		{
			using type = T const;
		};
		template <typename T>
		struct ConstIf<false, T>
		{
			using type = T;
		};
	}
	/**
	 * @brief Apply const, if `IsConst` is true.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam IsConst Whether or not to apply const to `T`.
	 * @tparam T The type to apply const to.
	 */
	template <bool IsConst, typename T>
	using ConstIf = typename Impl::ConstIf<IsConst, T>::type;
	/**
	 * @brief Apply the constness of `Match` to `T`
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam Match The type that will have its constness applied to `T`
	 * @tparam T The type to conditionally apply const.
	 */
	template <typename Match, typename T>
	using MatchConstOf = ConstIf<std::is_const_v<Match>, T>;
}