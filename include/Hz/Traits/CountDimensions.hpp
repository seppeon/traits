#pragma once
#include "Hz/Traits/Concepts.hpp"
#include "Hz/Traits/ValueType.hpp"

namespace Hz
{
	namespace Impl
	{
		template<typename T>
		struct CountDimensions
		{
			static constexpr size_t value = 0;
		};
		template<typename T> requires (Indexable<T> or ForIterable<T>)
		struct CountDimensions<T>
		{
			static constexpr size_t value = 1 + CountDimensions<ValueType<T>>::value;
		};
	}
	/**
	 * @brief Count the number of dimensions in a given type.
	 *
	 * A 2d array would return 2, a 1d array would return 1, a non-array would return 0. 
	 *
	 * @ingroup Variables
	 * @tparam T The type to extract the length from.
	 */
	template<typename T>
	inline constexpr auto count_dimensions_v = Impl::CountDimensions<T>::value;
	/**
	 * @brief Check if some container has `N` dimensions.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 * @tparam N The number of elements in the container.
	 */
	template <typename T, size_t N>
	concept WithDimensions = ((count_dimensions_v<T>) == N);
	/**
	 * @brief Check if some container has one dimension.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template <typename T>
	concept WithOneDimension = WithDimensions<T, 1>;
	/**
	 * @brief Check if some container has zero dimension.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template <typename T>
	concept WithZeroDimension = WithDimensions<T, 0>;
	/**
	 * @brief Check if some container has one or more dimensions.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template <typename T>
	concept WithOneOrMoreDimensions = ((count_dimensions_v<T>) >= 1);
	/**
	 * @brief Check if some container has multiple (more than 1) dimensions.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template <typename T>
	concept WithMultipleDimensions = ((count_dimensions_v<T>) > 1);
}