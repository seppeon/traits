#pragma once
#include "Hz/Traits/ConstIf.hpp"
#include "Hz/Traits/VolatileIf.hpp"

namespace Hz
{
	/**
	 * @brief Add const and/or volatile to a type `T` depending on some boolean condition.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam IsConst Whether or not to apply const.
	 * @tparam IsVolatile Whether or not to apply volatile.
	 * @tparam T The type to apply conditional qualifiers.
	 */
	template <bool IsConst, bool IsVolatile, typename T>
	using CvIf = VolatileIf<IsVolatile, ConstIf<IsConst, T>>;
	/**
	 * @brief Mimick the const/volatile qualifiers of `Match` and apply them to `T`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam Match The type to extract cv qualifiers from.
	 * @tparam T The type to apply qualifiers to.
	 */
	template <typename Match, typename T>
	using MatchCvOf = CvIf<std::is_const_v<Match>, std::is_volatile_v<Match>, T>;
}