#pragma once
#include "Hz/Traits/CloneCvref.hpp"
#include <algorithm>
#include <type_traits>
#include <initializer_list>

namespace Hz
{
	enum class ValueCategory
	{
		Value = 0,
		LValue = 1,
		RValue = 2,
	};

	enum class Qualifiers
	{
		None = 0,
		Const = 1,
		Volatile = 2,
		ConstVolatile = Const | Volatile,
	};

	namespace Impl
	{
		template <typename T>
		struct RefCat;
		
		template <typename T>
		struct RefCat
		{
			static constexpr auto ref_cat = ValueCategory::Value;
			static constexpr auto qualifiers = Qualifiers::None;
		};
		template <typename T>
		struct RefCat<T &>
		{
			static constexpr auto ref_cat = ValueCategory::LValue;
			static constexpr auto qualifiers = Qualifiers::None;
		};
		template <typename T>
		struct RefCat<T &&>
		{
			static constexpr auto ref_cat = ValueCategory::RValue;
			static constexpr auto qualifiers = Qualifiers::None;
		};
		
		template <typename T>
		struct RefCat<T const>
		{
			static constexpr auto ref_cat = ValueCategory::Value;
			static constexpr auto qualifiers = Qualifiers::Const;
		};
		template <typename T>
		struct RefCat<T const &>
		{
			static constexpr auto ref_cat = ValueCategory::LValue;
			static constexpr auto qualifiers = Qualifiers::Const;
		};
		template <typename T>
		struct RefCat<T const &&>
		{
			static constexpr auto ref_cat = ValueCategory::RValue;
			static constexpr auto qualifiers = Qualifiers::Const;
		};

		template <typename T>
		struct RefCat<T volatile>
		{
			static constexpr auto ref_cat = ValueCategory::Value;
			static constexpr auto qualifiers = Qualifiers::Volatile;
		};
		template <typename T>
		struct RefCat<T volatile &>
		{
			static constexpr auto ref_cat = ValueCategory::LValue;
			static constexpr auto qualifiers = Qualifiers::Volatile;
		};
		template <typename T>
		struct RefCat<T volatile &&>
		{
			static constexpr auto ref_cat = ValueCategory::RValue;
			static constexpr auto qualifiers = Qualifiers::Volatile;
		};

		template <typename T>
		struct RefCat<T const volatile>
		{
			static constexpr auto ref_cat = ValueCategory::Value;
			static constexpr auto qualifiers = Qualifiers::ConstVolatile;
		};
		template <typename T>
		struct RefCat<T const volatile &>
		{
			static constexpr auto ref_cat = ValueCategory::LValue;
			static constexpr auto qualifiers = Qualifiers::ConstVolatile;
		};
		template <typename T>
		struct RefCat<T const volatile &&>
		{
			static constexpr auto ref_cat = ValueCategory::RValue;
			static constexpr auto qualifiers = Qualifiers::ConstVolatile;
		};
	}
	/**
	 * @brief Get the value category of the some given type.
	 * 
	 * @ingroup Variables
	 * @tparam T The type to extract the category from.
	 */
	template <typename T>
	inline constexpr auto value_category_v = Impl::RefCat<T>::ref_cat;
	/**
	 * @brief Get the qualifiers of the some given type.
	 * 
	 * @ingroup Variables
	 * @tparam T The type to extract the qualifiers from.
	 */
	template <typename T>
	inline constexpr auto qualifiers_v = Impl::RefCat<T>::qualifiers;

	namespace Impl
	{
		template <typename T, ValueCategory Rc>
		struct ApplyRef;
		template <typename T>
		struct ApplyRef<T, ValueCategory::Value>
		{
			using type = T;
		};
		template <typename T>
		struct ApplyRef<T, ValueCategory::LValue>
		{
			using type = std::add_lvalue_reference_t<T>;
		};
		template <typename T>
		struct ApplyRef<T, ValueCategory::RValue>
		{
			using type = std::add_rvalue_reference_t<T>;
		};
		template <typename T, ValueCategory Rc>
		using apply_ref_t = typename ApplyRef<std::remove_reference_t<T>, Rc>::type;

		template <typename T, Qualifiers Cv, ValueCategory Cat = value_category_v<T>>
		struct ApplyCvrefCat;
		template <typename T, ValueCategory Cat>
		struct ApplyCvrefCat<T, Qualifiers::None, Cat>
		{
			using type = apply_ref_t<std::remove_cvref_t<T>, Cat>;
		};
		template <typename T, ValueCategory Cat>
		struct ApplyCvrefCat<T, Qualifiers::Const, Cat>
		{
			using type = apply_ref_t<std::remove_cvref_t<T> const, Cat>;
		};
		template <typename T, ValueCategory Cat>
		struct ApplyCvrefCat<T, Qualifiers::Volatile, Cat>
		{
			using type = apply_ref_t<std::remove_cvref_t<T> volatile, Cat>;
		};
		template <typename T, ValueCategory Cat>
		struct ApplyCvrefCat<T, Qualifiers::ConstVolatile, Cat>
		{
			using type = apply_ref_t<std::remove_cvref_t<T> const volatile, Cat>;
		};
	}
	/**
	 * @brief Apply the requested qualifiers to `T`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type to apply qualifiers.
	 * @tparam Cv The qualifiers to apply.
	 */
	template <typename T, Qualifiers Cv>
	using ApplyCvQualifiers = typename Impl::ApplyCvrefCat<T, Cv>::type;
	/**
	 * @brief Apply the requested qualifiers and value category to `T`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type to apply qualifiers and value category.
	 * @tparam Cv The qualifiers to apply.
	 * @tparam Cat The value category to apply.
	 */
	template <typename T, Qualifiers Cv, ValueCategory Cat>
	using ApplyCvQualifiersAndValueCategory = typename Impl::ApplyCvrefCat<T, Cv, Cat>::type;
	/**
	 * @brief Apply a a value category to `T`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type to apply the value category to.
	 * @tparam Cat The requested value category.
	 */
	template <typename T, ValueCategory Cat>
	using ApplyValueCategory = typename Impl::ApplyCvrefCat<T, qualifiers_v<T>, Cat>::type;
}