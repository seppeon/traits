#pragma once
#include "Hz/Traits/SimilarTo.hpp"
#include <Hz/TL/TLAtIndex.hpp>
#include <Hz/TL/TLUnique.hpp>

namespace Hz
{
	namespace Impl
	{
		inline void FailConstexpr(){}
		template <typename T>
		struct NoConvert
		{
			T value;
			bool converted = false;
			constexpr ~NoConvert() { if (not converted) { FailConstexpr(); } }

			template <SimilarTo<T> Obj>
			constexpr operator Obj() noexcept { converted = true; return FWD(value); }
		};
		template <typename T>
		NoConvert(T&&)->NoConvert<T>;

		template <Hz::TLUnique List, typename ... Args>
		struct FirstConstructibleType;
		template <template <typename ...> class As, typename ... Items, typename ... Args>
		struct FirstConstructibleType<As<Items...>, Args...>
		{
			using list_t = As<Items...>;

			template <typename T>
			static constexpr bool test = requires(Args ... args)
			{
				{ T{ NoConvert{static_cast<Args&&>(args)} ... } };
			};

			template <typename T>
			static constexpr bool test_w_convert = requires(Args ... args)
			{
				{ T{ static_cast<Args&&>(args) ... } };
			};

			static constexpr size_t GetIndex() noexcept
			{
				// Attempt non-converting constructor.
				{
					size_t index = 0;
					(void)(( (test<Items> or (++index, false)) or ... ));
					if (index != sizeof...(Items)) return index;
				}

				// Attempt converting constructor.
				{
					size_t index = 0;
					(void)(( (test_w_convert<Items> or (++index, false)) or ... ));
					if (index != sizeof...(Items)) return index;
				}

				return sizeof...(Items);
			}

			using type = TLAtIndex<list_t, GetIndex()>;
		};
	}
	/**
	 * @brief Get the first type from `List` that can be constructed without conversions from `Args`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam List The list of types to try to construct.
	 * @tparam Args... The arguments to apply to the types contained in `List`.
	 */
	template <TLUnique List, typename ... Args>
	using FirstConstructibleType = typename Impl::FirstConstructibleType<List, Args...>::type;
}