#pragma once
#include <Hz/TL/TLCommon.hpp>

namespace Hz
{
	/**
	 * @brief Check if the given type is a function object.
	 * 
	 * @ingroup Concepts
	 * @tparam Obj The type to check.
	 */
	template<typename Obj>
	concept FunctionOperator = requires
	{
		{ &std::remove_cvref_t<Obj>::operator() };
	};
	namespace Impl
	{
		template<typename>
		struct FnTraits;
	
		template<FunctionOperator Obj>
		struct FnTraits<Obj> : FnTraits<decltype( &std::remove_cvref_t<Obj>::operator() )>
		{
		};

		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) const volatile noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const volatile noexcept;
			using object_t = void;
			using free_fn_t = R( Args..., ... ) noexcept;
			using erased_free_fn_t = R( Args..., ... ) noexcept;

			using add_const = R( Args..., ... ) const volatile noexcept;
			using add_volatile = R( Args..., ... ) const volatile noexcept;
			using add_const_volatile = R( Args..., ... ) const volatile noexcept;
			using add_lvalue_reference = R( Args..., ... ) const volatile & noexcept;
			using add_rvalue_reference = R( Args..., ... ) const volatile && noexcept;
			using add_noexcept = R( Args..., ... ) const volatile noexcept;

			using remove_const = R( Args..., ... ) volatile noexcept;
			using remove_volatile = R( Args..., ... ) const noexcept;
			using remove_const_volatile = R( Args..., ... ) noexcept;
			using remove_reference = R( Args..., ... ) const volatile noexcept;
			using remove_noexcept = R( Args..., ... ) const volatile;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) const volatile>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const volatile;
			using object_t = void;
			using free_fn_t = R( Args..., ... );
			using erased_free_fn_t = R( Args..., ... );

			using add_const = R( Args..., ... ) const volatile;
			using add_volatile = R( Args..., ... ) const volatile;
			using add_const_volatile = R( Args..., ... ) const volatile;
			using add_lvalue_reference = R( Args..., ... ) const volatile &;
			using add_rvalue_reference = R( Args..., ... ) const volatile &&;
			using add_noexcept = R( Args..., ... ) const volatile noexcept;

			using remove_const = R( Args..., ... ) volatile;
			using remove_volatile = R( Args..., ... ) const;
			using remove_const_volatile = R( Args..., ... );
			using remove_reference = R( Args..., ... ) const volatile;
			using remove_noexcept = R( Args..., ... ) const volatile;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) const volatile & noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const volatile & noexcept;
			using object_t = void;
			using free_fn_t = R( Args..., ... ) noexcept;
			using erased_free_fn_t = R( Args..., ... ) noexcept;

			using add_const = R( Args..., ... ) const volatile & noexcept;
			using add_volatile = R( Args..., ... ) const volatile & noexcept;
			using add_const_volatile = R( Args..., ... ) const volatile & noexcept;
			using add_lvalue_reference = R( Args..., ... ) const volatile & noexcept;
			using add_rvalue_reference = R( Args..., ... ) const volatile && noexcept;
			using add_noexcept = R( Args..., ... ) const volatile & noexcept;

			using remove_const = R( Args..., ... ) volatile & noexcept;
			using remove_volatile = R( Args..., ... ) const & noexcept;
			using remove_const_volatile = R( Args..., ... ) & noexcept;
			using remove_reference = R( Args..., ... ) const volatile noexcept;
			using remove_noexcept = R( Args..., ... ) const volatile &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) const volatile &>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const volatile &;
			using object_t = void;
			using free_fn_t = R( Args..., ... );
			using erased_free_fn_t = R( Args..., ... );

			using add_const = R( Args..., ... ) const volatile &;
			using add_volatile = R( Args..., ... ) const volatile &;
			using add_const_volatile = R( Args..., ... ) const volatile &;
			using add_lvalue_reference = R( Args..., ... ) const volatile &;
			using add_rvalue_reference = R( Args..., ... ) const volatile &&;
			using add_noexcept = R( Args..., ... ) const volatile & noexcept;

			using remove_const = R( Args..., ... ) volatile &;
			using remove_volatile = R( Args..., ... ) const &;
			using remove_const_volatile = R( Args..., ... ) &;
			using remove_reference = R( Args..., ... ) const volatile;
			using remove_noexcept = R( Args..., ... ) const volatile &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) const volatile && noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const volatile && noexcept;
			using object_t = void;
			using free_fn_t = R( Args..., ... ) noexcept;
			using erased_free_fn_t = R( Args..., ... ) noexcept;

			using add_const = R( Args..., ... ) const volatile && noexcept;
			using add_volatile = R( Args..., ... ) const volatile && noexcept;
			using add_const_volatile = R( Args..., ... ) const volatile && noexcept;
			using add_lvalue_reference = R( Args..., ... ) const volatile & noexcept;
			using add_rvalue_reference = R( Args..., ... ) const volatile && noexcept;
			using add_noexcept = R( Args..., ... ) const volatile && noexcept;

			using remove_const = R( Args..., ... ) volatile && noexcept;
			using remove_volatile = R( Args..., ... ) const && noexcept;
			using remove_const_volatile = R( Args..., ... ) && noexcept;
			using remove_reference = R( Args..., ... ) const volatile noexcept;
			using remove_noexcept = R( Args..., ... ) const volatile &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) const volatile &&>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const volatile &&;
			using object_t = void;
			using free_fn_t = R( Args..., ... );
			using erased_free_fn_t = R( Args..., ... );

			using add_const = R( Args..., ... ) const volatile &&;
			using add_volatile = R( Args..., ... ) const volatile &&;
			using add_const_volatile = R( Args..., ... ) const volatile &&;
			using add_lvalue_reference = R( Args..., ... ) const volatile &;
			using add_rvalue_reference = R( Args..., ... ) const volatile &&;
			using add_noexcept = R( Args..., ... ) const volatile && noexcept;

			using remove_const = R( Args..., ... ) volatile &&;
			using remove_volatile = R( Args..., ... ) const &&;
			using remove_const_volatile = R( Args..., ... ) &&;
			using remove_reference = R( Args..., ... ) const volatile;
			using remove_noexcept = R( Args..., ... ) const volatile &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) const noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const noexcept;
			using object_t = void;
			using free_fn_t = R( Args..., ... ) noexcept;
			using erased_free_fn_t = R( Args..., ... ) noexcept;

			using add_const = R( Args..., ... ) const noexcept;
			using add_volatile = R( Args..., ... ) const volatile noexcept;
			using add_const_volatile = R( Args..., ... ) const volatile noexcept;
			using add_lvalue_reference = R( Args..., ... ) const & noexcept;
			using add_rvalue_reference = R( Args..., ... ) const && noexcept;
			using add_noexcept = R( Args..., ... ) const noexcept;

			using remove_const = R( Args..., ... ) noexcept;
			using remove_volatile = R( Args..., ... ) const noexcept;
			using remove_const_volatile = R( Args..., ... ) noexcept;
			using remove_reference = R( Args..., ... ) const noexcept;
			using remove_noexcept = R( Args..., ... ) const;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) const>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const;
			using object_t = void;
			using free_fn_t = R( Args..., ... );
			using erased_free_fn_t = R( Args..., ... );

			using add_const = R( Args..., ... ) const;
			using add_volatile = R( Args..., ... ) const volatile;
			using add_const_volatile = R( Args..., ... ) const volatile;
			using add_lvalue_reference = R( Args..., ... ) const &;
			using add_rvalue_reference = R( Args..., ... ) const &&;
			using add_noexcept = R( Args..., ... ) const noexcept;

			using remove_const = R( Args..., ... );
			using remove_volatile = R( Args..., ... ) const;
			using remove_const_volatile = R( Args..., ... );
			using remove_reference = R( Args..., ... ) const;
			using remove_noexcept = R( Args..., ... ) const;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) const & noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const & noexcept;
			using object_t = void;
			using free_fn_t = R( Args..., ... ) noexcept;
			using erased_free_fn_t = R( Args..., ... ) noexcept;

			using add_const = R( Args..., ... ) const & noexcept;
			using add_volatile = R( Args..., ... ) const volatile & noexcept;
			using add_const_volatile = R( Args..., ... ) const volatile & noexcept;
			using add_lvalue_reference = R( Args..., ... ) const & noexcept;
			using add_rvalue_reference = R( Args..., ... ) const && noexcept;
			using add_noexcept = R( Args..., ... ) const & noexcept;

			using remove_const = R( Args..., ... ) & noexcept;
			using remove_volatile = R( Args..., ... ) const & noexcept;
			using remove_const_volatile = R( Args..., ... ) & noexcept;
			using remove_reference = R( Args..., ... ) const noexcept;
			using remove_noexcept = R( Args..., ... ) const &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) const &>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const &;
			using object_t = void;
			using free_fn_t = R( Args..., ... );
			using erased_free_fn_t = R( Args..., ... );

			using add_const = R( Args..., ... ) const &;
			using add_volatile = R( Args..., ... ) const volatile &;
			using add_const_volatile = R( Args..., ... ) const volatile &;
			using add_lvalue_reference = R( Args..., ... ) const &;
			using add_rvalue_reference = R( Args..., ... ) const &&;
			using add_noexcept = R( Args..., ... ) const & noexcept;

			using remove_const = R( Args..., ... ) &;
			using remove_volatile = R( Args..., ... ) const &;
			using remove_const_volatile = R( Args..., ... ) &;
			using remove_reference = R( Args..., ... ) const;
			using remove_noexcept = R( Args..., ... ) const &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) const && noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const && noexcept;
			using object_t = void;
			using free_fn_t = R( Args..., ... ) noexcept;
			using erased_free_fn_t = R( Args..., ... ) noexcept;

			using add_const = R( Args..., ... ) const && noexcept;
			using add_volatile = R( Args..., ... ) const volatile && noexcept;
			using add_const_volatile = R( Args..., ... ) const volatile && noexcept;
			using add_lvalue_reference = R( Args..., ... ) const & noexcept;
			using add_rvalue_reference = R( Args..., ... ) const && noexcept;
			using add_noexcept = R( Args..., ... ) const && noexcept;

			using remove_const = R( Args..., ... ) && noexcept;
			using remove_volatile = R( Args..., ... ) const && noexcept;
			using remove_const_volatile = R( Args..., ... ) && noexcept;
			using remove_reference = R( Args..., ... ) const noexcept;
			using remove_noexcept = R( Args..., ... ) const &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) const &&>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const &&;
			using object_t = void;
			using free_fn_t = R( Args..., ... );
			using erased_free_fn_t = R( Args..., ... );

			using add_const = R( Args..., ... ) const &&;
			using add_volatile = R( Args..., ... ) const volatile &&;
			using add_const_volatile = R( Args..., ... ) const volatile &&;
			using add_lvalue_reference = R( Args..., ... ) const &;
			using add_rvalue_reference = R( Args..., ... ) const &&;
			using add_noexcept = R( Args..., ... ) const && noexcept;

			using remove_const = R( Args..., ... ) &&;
			using remove_volatile = R( Args..., ... ) const &&;
			using remove_const_volatile = R( Args..., ... ) &&;
			using remove_reference = R( Args..., ... ) const;
			using remove_noexcept = R( Args..., ... ) const &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) volatile noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) volatile noexcept;
			using object_t = void;
			using free_fn_t = R( Args..., ... ) noexcept;
			using erased_free_fn_t = R( Args..., ... ) noexcept;

			using add_const = R( Args..., ... ) const volatile noexcept;
			using add_volatile = R( Args..., ... ) volatile noexcept;
			using add_const_volatile = R( Args..., ... ) const volatile noexcept;
			using add_lvalue_reference = R( Args..., ... ) volatile & noexcept;
			using add_rvalue_reference = R( Args..., ... ) volatile && noexcept;
			using add_noexcept = R( Args..., ... ) volatile noexcept;

			using remove_const = R( Args..., ... ) volatile noexcept;
			using remove_volatile = R( Args..., ... ) noexcept;
			using remove_const_volatile = R( Args..., ... ) noexcept;
			using remove_reference = R( Args..., ... ) volatile noexcept;
			using remove_noexcept = R( Args..., ... ) volatile;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) volatile>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) volatile;
			using object_t = void;
			using free_fn_t = R( Args..., ... );
			using erased_free_fn_t = R( Args..., ... );

			using add_const = R( Args..., ... ) const volatile;
			using add_volatile = R( Args..., ... ) volatile;
			using add_const_volatile = R( Args..., ... ) const volatile;
			using add_lvalue_reference = R( Args..., ... ) volatile &;
			using add_rvalue_reference = R( Args..., ... ) volatile &&;
			using add_noexcept = R( Args..., ... ) volatile noexcept;

			using remove_const = R( Args..., ... ) volatile;
			using remove_volatile = R( Args..., ... );
			using remove_const_volatile = R( Args..., ... );
			using remove_reference = R( Args..., ... ) volatile;
			using remove_noexcept = R( Args..., ... ) volatile;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) volatile & noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) volatile & noexcept;
			using object_t = void;
			using free_fn_t = R( Args..., ... ) noexcept;
			using erased_free_fn_t = R( Args..., ... ) noexcept;

			using add_const = R( Args..., ... ) const volatile & noexcept;
			using add_volatile = R( Args..., ... ) volatile & noexcept;
			using add_const_volatile = R( Args..., ... ) const volatile & noexcept;
			using add_lvalue_reference = R( Args..., ... ) volatile & noexcept;
			using add_rvalue_reference = R( Args..., ... ) volatile && noexcept;
			using add_noexcept = R( Args..., ... ) volatile & noexcept;

			using remove_const = R( Args..., ... ) volatile & noexcept;
			using remove_volatile = R( Args..., ... ) & noexcept;
			using remove_const_volatile = R( Args..., ... ) & noexcept;
			using remove_reference = R( Args..., ... ) volatile noexcept;
			using remove_noexcept = R( Args..., ... ) volatile &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) volatile &>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) volatile &;
			using object_t = void;
			using free_fn_t = R( Args..., ... );
			using erased_free_fn_t = R( Args..., ... );

			using add_const = R( Args..., ... ) const volatile &;
			using add_volatile = R( Args..., ... ) volatile &;
			using add_const_volatile = R( Args..., ... ) const volatile &;
			using add_lvalue_reference = R( Args..., ... ) volatile &;
			using add_rvalue_reference = R( Args..., ... ) volatile &&;
			using add_noexcept = R( Args..., ... ) volatile & noexcept;

			using remove_const = R( Args..., ... ) volatile &;
			using remove_volatile = R( Args..., ... ) &;
			using remove_const_volatile = R( Args..., ... ) &;
			using remove_reference = R( Args..., ... ) volatile;
			using remove_noexcept = R( Args..., ... ) volatile &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) volatile && noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) volatile && noexcept;
			using object_t = void;
			using free_fn_t = R( Args..., ... ) noexcept;
			using erased_free_fn_t = R( Args..., ... ) noexcept;

			using add_const = R( Args..., ... ) const volatile && noexcept;
			using add_volatile = R( Args..., ... ) volatile && noexcept;
			using add_const_volatile = R( Args..., ... ) const volatile && noexcept;
			using add_lvalue_reference = R( Args..., ... ) volatile & noexcept;
			using add_rvalue_reference = R( Args..., ... ) volatile && noexcept;
			using add_noexcept = R( Args..., ... ) volatile && noexcept;

			using remove_const = R( Args..., ... ) volatile && noexcept;
			using remove_volatile = R( Args..., ... ) && noexcept;
			using remove_const_volatile = R( Args..., ... ) && noexcept;
			using remove_reference = R( Args..., ... ) volatile noexcept;
			using remove_noexcept = R( Args..., ... ) volatile &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) volatile &&>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) volatile &&;
			using object_t = void;
			using free_fn_t = R( Args..., ... );
			using erased_free_fn_t = R( Args..., ... );

			using add_const = R( Args..., ... ) const volatile &&;
			using add_volatile = R( Args..., ... ) volatile &&;
			using add_const_volatile = R( Args..., ... ) const volatile &&;
			using add_lvalue_reference = R( Args..., ... ) volatile &;
			using add_rvalue_reference = R( Args..., ... ) volatile &&;
			using add_noexcept = R( Args..., ... ) volatile && noexcept;

			using remove_const = R( Args..., ... ) volatile &&;
			using remove_volatile = R( Args..., ... ) &&;
			using remove_const_volatile = R( Args..., ... ) &&;
			using remove_reference = R( Args..., ... ) volatile;
			using remove_noexcept = R( Args..., ... ) volatile &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) noexcept;
			using object_t = void;
			using free_fn_t = R( Args..., ... ) noexcept;
			using erased_free_fn_t = R( Args..., ... ) noexcept;

			using add_const = R( Args..., ... ) const noexcept;
			using add_volatile = R( Args..., ... ) volatile noexcept;
			using add_const_volatile = R( Args..., ... ) const volatile noexcept;
			using add_lvalue_reference = R( Args..., ... ) & noexcept;
			using add_rvalue_reference = R( Args..., ... ) && noexcept;
			using add_noexcept = R( Args..., ... ) noexcept;

			using remove_const = R( Args..., ... ) noexcept;
			using remove_volatile = R( Args..., ... ) noexcept;
			using remove_const_volatile = R( Args..., ... ) noexcept;
			using remove_reference = R( Args..., ... ) noexcept;
			using remove_noexcept = R( Args..., ... );

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... )>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... );
			using object_t = void;
			using free_fn_t = R( Args..., ... );
			using erased_free_fn_t = R( Args..., ... );

			using add_const = R( Args..., ... ) const;
			using add_volatile = R( Args..., ... ) volatile;
			using add_const_volatile = R( Args..., ... ) const volatile;
			using add_lvalue_reference = R( Args..., ... ) &;
			using add_rvalue_reference = R( Args..., ... ) &&;
			using add_noexcept = R( Args..., ... ) noexcept;

			using remove_const = R( Args..., ... );
			using remove_volatile = R( Args..., ... );
			using remove_const_volatile = R( Args..., ... );
			using remove_reference = R( Args..., ... );
			using remove_noexcept = R( Args..., ... );

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) & noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) & noexcept;
			using object_t = void;
			using free_fn_t = R( Args..., ... ) noexcept;
			using erased_free_fn_t = R( Args..., ... ) noexcept;

			using add_const = R( Args..., ... ) const & noexcept;
			using add_volatile = R( Args..., ... ) volatile & noexcept;
			using add_const_volatile = R( Args..., ... ) const volatile & noexcept;
			using add_lvalue_reference = R( Args..., ... ) & noexcept;
			using add_rvalue_reference = R( Args..., ... ) && noexcept;
			using add_noexcept = R( Args..., ... ) & noexcept;

			using remove_const = R( Args..., ... ) & noexcept;
			using remove_volatile = R( Args..., ... ) & noexcept;
			using remove_const_volatile = R( Args..., ... ) & noexcept;
			using remove_reference = R( Args..., ... ) noexcept;
			using remove_noexcept = R( Args..., ... ) &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) &>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) &;
			using object_t = void;
			using free_fn_t = R( Args..., ... );
			using erased_free_fn_t = R( Args..., ... );

			using add_const = R( Args..., ... ) const &;
			using add_volatile = R( Args..., ... ) volatile &;
			using add_const_volatile = R( Args..., ... ) const volatile &;
			using add_lvalue_reference = R( Args..., ... ) &;
			using add_rvalue_reference = R( Args..., ... ) &&;
			using add_noexcept = R( Args..., ... ) & noexcept;

			using remove_const = R( Args..., ... ) &;
			using remove_volatile = R( Args..., ... ) &;
			using remove_const_volatile = R( Args..., ... ) &;
			using remove_reference = R( Args..., ... );
			using remove_noexcept = R( Args..., ... ) &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) && noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) && noexcept;
			using object_t = void;
			using free_fn_t = R( Args..., ... ) noexcept;
			using erased_free_fn_t = R( Args..., ... ) noexcept;

			using add_const = R( Args..., ... ) const && noexcept;
			using add_volatile = R( Args..., ... ) volatile && noexcept;
			using add_const_volatile = R( Args..., ... ) const volatile && noexcept;
			using add_lvalue_reference = R( Args..., ... ) & noexcept;
			using add_rvalue_reference = R( Args..., ... ) && noexcept;
			using add_noexcept = R( Args..., ... ) && noexcept;

			using remove_const = R( Args..., ... ) && noexcept;
			using remove_volatile = R( Args..., ... ) && noexcept;
			using remove_const_volatile = R( Args..., ... ) && noexcept;
			using remove_reference = R( Args..., ... ) noexcept;
			using remove_noexcept = R( Args..., ... ) &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args..., ... ) &&>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) &&;
			using object_t = void;
			using free_fn_t = R( Args..., ... );
			using erased_free_fn_t = R( Args..., ... );

			using add_const = R( Args..., ... ) const &&;
			using add_volatile = R( Args..., ... ) volatile &&;
			using add_const_volatile = R( Args..., ... ) const volatile &&;
			using add_lvalue_reference = R( Args..., ... ) &;
			using add_rvalue_reference = R( Args..., ... ) &&;
			using add_noexcept = R( Args..., ... ) && noexcept;

			using remove_const = R( Args..., ... ) &&;
			using remove_volatile = R( Args..., ... ) &&;
			using remove_const_volatile = R( Args..., ... ) &&;
			using remove_reference = R( Args..., ... );
			using remove_noexcept = R( Args..., ... ) &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) const volatile noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const volatile noexcept;
			using object_t = void;
			using free_fn_t = R( Args... ) noexcept;
			using erased_free_fn_t = R( Args... ) noexcept;

			using add_const = R( Args... ) const volatile noexcept;
			using add_volatile = R( Args... ) const volatile noexcept;
			using add_const_volatile = R( Args... ) const volatile noexcept;
			using add_lvalue_reference = R( Args... ) const volatile & noexcept;
			using add_rvalue_reference = R( Args... ) const volatile && noexcept;
			using add_noexcept = R( Args... ) const volatile noexcept;

			using remove_const = R( Args... ) volatile noexcept;
			using remove_volatile = R( Args... ) const noexcept;
			using remove_const_volatile = R( Args... ) noexcept;
			using remove_reference = R( Args... ) const volatile noexcept;
			using remove_noexcept = R( Args... ) const volatile;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) const volatile>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const volatile;
			using object_t = void;
			using free_fn_t = R( Args... );
			using erased_free_fn_t = R( Args... );

			using add_const = R( Args... ) const volatile;
			using add_volatile = R( Args... ) const volatile;
			using add_const_volatile = R( Args... ) const volatile;
			using add_lvalue_reference = R( Args... ) const volatile &;
			using add_rvalue_reference = R( Args... ) const volatile &&;
			using add_noexcept = R( Args... ) const volatile noexcept;

			using remove_const = R( Args... ) volatile;
			using remove_volatile = R( Args... ) const;
			using remove_const_volatile = R( Args... );
			using remove_reference = R( Args... ) const volatile;
			using remove_noexcept = R( Args... ) const volatile;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) const volatile & noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const volatile & noexcept;
			using object_t = void;
			using free_fn_t = R( Args... ) noexcept;
			using erased_free_fn_t = R( Args... ) noexcept;

			using add_const = R( Args... ) const volatile & noexcept;
			using add_volatile = R( Args... ) const volatile & noexcept;
			using add_const_volatile = R( Args... ) const volatile & noexcept;
			using add_lvalue_reference = R( Args... ) const volatile & noexcept;
			using add_rvalue_reference = R( Args... ) const volatile && noexcept;
			using add_noexcept = R( Args... ) const volatile & noexcept;

			using remove_const = R( Args... ) volatile & noexcept;
			using remove_volatile = R( Args... ) const & noexcept;
			using remove_const_volatile = R( Args... ) & noexcept;
			using remove_reference = R( Args... ) const volatile noexcept;
			using remove_noexcept = R( Args... ) const volatile &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) const volatile &>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const volatile &;
			using object_t = void;
			using free_fn_t = R( Args... );
			using erased_free_fn_t = R( Args... );

			using add_const = R( Args... ) const volatile &;
			using add_volatile = R( Args... ) const volatile &;
			using add_const_volatile = R( Args... ) const volatile &;
			using add_lvalue_reference = R( Args... ) const volatile &;
			using add_rvalue_reference = R( Args... ) const volatile &&;
			using add_noexcept = R( Args... ) const volatile & noexcept;

			using remove_const = R( Args... ) volatile &;
			using remove_volatile = R( Args... ) const &;
			using remove_const_volatile = R( Args... ) &;
			using remove_reference = R( Args... ) const volatile;
			using remove_noexcept = R( Args... ) const volatile &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) const volatile && noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const volatile && noexcept;
			using object_t = void;
			using free_fn_t = R( Args... ) noexcept;
			using erased_free_fn_t = R( Args... ) noexcept;

			using add_const = R( Args... ) const volatile && noexcept;
			using add_volatile = R( Args... ) const volatile && noexcept;
			using add_const_volatile = R( Args... ) const volatile && noexcept;
			using add_lvalue_reference = R( Args... ) const volatile & noexcept;
			using add_rvalue_reference = R( Args... ) const volatile && noexcept;
			using add_noexcept = R( Args... ) const volatile && noexcept;

			using remove_const = R( Args... ) volatile && noexcept;
			using remove_volatile = R( Args... ) const && noexcept;
			using remove_const_volatile = R( Args... ) && noexcept;
			using remove_reference = R( Args... ) const volatile noexcept;
			using remove_noexcept = R( Args... ) const volatile &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) const volatile &&>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const volatile &&;
			using object_t = void;
			using free_fn_t = R( Args... );
			using erased_free_fn_t = R( Args... );

			using add_const = R( Args... ) const volatile &&;
			using add_volatile = R( Args... ) const volatile &&;
			using add_const_volatile = R( Args... ) const volatile &&;
			using add_lvalue_reference = R( Args... ) const volatile &;
			using add_rvalue_reference = R( Args... ) const volatile &&;
			using add_noexcept = R( Args... ) const volatile && noexcept;

			using remove_const = R( Args... ) volatile &&;
			using remove_volatile = R( Args... ) const &&;
			using remove_const_volatile = R( Args... ) &&;
			using remove_reference = R( Args... ) const volatile;
			using remove_noexcept = R( Args... ) const volatile &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) const noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const noexcept;
			using object_t = void;
			using free_fn_t = R( Args... ) noexcept;
			using erased_free_fn_t = R( Args... ) noexcept;

			using add_const = R( Args... ) const noexcept;
			using add_volatile = R( Args... ) const volatile noexcept;
			using add_const_volatile = R( Args... ) const volatile noexcept;
			using add_lvalue_reference = R( Args... ) const & noexcept;
			using add_rvalue_reference = R( Args... ) const && noexcept;
			using add_noexcept = R( Args... ) const noexcept;

			using remove_const = R( Args... ) noexcept;
			using remove_volatile = R( Args... ) const noexcept;
			using remove_const_volatile = R( Args... ) noexcept;
			using remove_reference = R( Args... ) const noexcept;
			using remove_noexcept = R( Args... ) const;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) const>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const;
			using object_t = void;
			using free_fn_t = R( Args... );
			using erased_free_fn_t = R( Args... );

			using add_const = R( Args... ) const;
			using add_volatile = R( Args... ) const volatile;
			using add_const_volatile = R( Args... ) const volatile;
			using add_lvalue_reference = R( Args... ) const &;
			using add_rvalue_reference = R( Args... ) const &&;
			using add_noexcept = R( Args... ) const noexcept;

			using remove_const = R( Args... );
			using remove_volatile = R( Args... ) const;
			using remove_const_volatile = R( Args... );
			using remove_reference = R( Args... ) const;
			using remove_noexcept = R( Args... ) const;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) const & noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const & noexcept;
			using object_t = void;
			using free_fn_t = R( Args... ) noexcept;
			using erased_free_fn_t = R( Args... ) noexcept;

			using add_const = R( Args... ) const & noexcept;
			using add_volatile = R( Args... ) const volatile & noexcept;
			using add_const_volatile = R( Args... ) const volatile & noexcept;
			using add_lvalue_reference = R( Args... ) const & noexcept;
			using add_rvalue_reference = R( Args... ) const && noexcept;
			using add_noexcept = R( Args... ) const & noexcept;

			using remove_const = R( Args... ) & noexcept;
			using remove_volatile = R( Args... ) const & noexcept;
			using remove_const_volatile = R( Args... ) & noexcept;
			using remove_reference = R( Args... ) const noexcept;
			using remove_noexcept = R( Args... ) const &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) const &>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const &;
			using object_t = void;
			using free_fn_t = R( Args... );
			using erased_free_fn_t = R( Args... );

			using add_const = R( Args... ) const &;
			using add_volatile = R( Args... ) const volatile &;
			using add_const_volatile = R( Args... ) const volatile &;
			using add_lvalue_reference = R( Args... ) const &;
			using add_rvalue_reference = R( Args... ) const &&;
			using add_noexcept = R( Args... ) const & noexcept;

			using remove_const = R( Args... ) &;
			using remove_volatile = R( Args... ) const &;
			using remove_const_volatile = R( Args... ) &;
			using remove_reference = R( Args... ) const;
			using remove_noexcept = R( Args... ) const &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) const && noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const && noexcept;
			using object_t = void;
			using free_fn_t = R( Args... ) noexcept;
			using erased_free_fn_t = R( Args... ) noexcept;

			using add_const = R( Args... ) const && noexcept;
			using add_volatile = R( Args... ) const volatile && noexcept;
			using add_const_volatile = R( Args... ) const volatile && noexcept;
			using add_lvalue_reference = R( Args... ) const & noexcept;
			using add_rvalue_reference = R( Args... ) const && noexcept;
			using add_noexcept = R( Args... ) const && noexcept;

			using remove_const = R( Args... ) && noexcept;
			using remove_volatile = R( Args... ) const && noexcept;
			using remove_const_volatile = R( Args... ) && noexcept;
			using remove_reference = R( Args... ) const noexcept;
			using remove_noexcept = R( Args... ) const &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) const &&>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const &&;
			using object_t = void;
			using free_fn_t = R( Args... );
			using erased_free_fn_t = R( Args... );

			using add_const = R( Args... ) const &&;
			using add_volatile = R( Args... ) const volatile &&;
			using add_const_volatile = R( Args... ) const volatile &&;
			using add_lvalue_reference = R( Args... ) const &;
			using add_rvalue_reference = R( Args... ) const &&;
			using add_noexcept = R( Args... ) const && noexcept;

			using remove_const = R( Args... ) &&;
			using remove_volatile = R( Args... ) const &&;
			using remove_const_volatile = R( Args... ) &&;
			using remove_reference = R( Args... ) const;
			using remove_noexcept = R( Args... ) const &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) volatile noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) volatile noexcept;
			using object_t = void;
			using free_fn_t = R( Args... ) noexcept;
			using erased_free_fn_t = R( Args... ) noexcept;

			using add_const = R( Args... ) const volatile noexcept;
			using add_volatile = R( Args... ) volatile noexcept;
			using add_const_volatile = R( Args... ) const volatile noexcept;
			using add_lvalue_reference = R( Args... ) volatile & noexcept;
			using add_rvalue_reference = R( Args... ) volatile && noexcept;
			using add_noexcept = R( Args... ) volatile noexcept;

			using remove_const = R( Args... ) volatile noexcept;
			using remove_volatile = R( Args... ) noexcept;
			using remove_const_volatile = R( Args... ) noexcept;
			using remove_reference = R( Args... ) volatile noexcept;
			using remove_noexcept = R( Args... ) volatile;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) volatile>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) volatile;
			using object_t = void;
			using free_fn_t = R( Args... );
			using erased_free_fn_t = R( Args... );

			using add_const = R( Args... ) const volatile;
			using add_volatile = R( Args... ) volatile;
			using add_const_volatile = R( Args... ) const volatile;
			using add_lvalue_reference = R( Args... ) volatile &;
			using add_rvalue_reference = R( Args... ) volatile &&;
			using add_noexcept = R( Args... ) volatile noexcept;

			using remove_const = R( Args... ) volatile;
			using remove_volatile = R( Args... );
			using remove_const_volatile = R( Args... );
			using remove_reference = R( Args... ) volatile;
			using remove_noexcept = R( Args... ) volatile;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) volatile & noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) volatile & noexcept;
			using object_t = void;
			using free_fn_t = R( Args... ) noexcept;
			using erased_free_fn_t = R( Args... ) noexcept;

			using add_const = R( Args... ) const volatile & noexcept;
			using add_volatile = R( Args... ) volatile & noexcept;
			using add_const_volatile = R( Args... ) const volatile & noexcept;
			using add_lvalue_reference = R( Args... ) volatile & noexcept;
			using add_rvalue_reference = R( Args... ) volatile && noexcept;
			using add_noexcept = R( Args... ) volatile & noexcept;

			using remove_const = R( Args... ) volatile & noexcept;
			using remove_volatile = R( Args... ) & noexcept;
			using remove_const_volatile = R( Args... ) & noexcept;
			using remove_reference = R( Args... ) volatile noexcept;
			using remove_noexcept = R( Args... ) volatile &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) volatile &>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) volatile &;
			using object_t = void;
			using free_fn_t = R( Args... );
			using erased_free_fn_t = R( Args... );

			using add_const = R( Args... ) const volatile &;
			using add_volatile = R( Args... ) volatile &;
			using add_const_volatile = R( Args... ) const volatile &;
			using add_lvalue_reference = R( Args... ) volatile &;
			using add_rvalue_reference = R( Args... ) volatile &&;
			using add_noexcept = R( Args... ) volatile & noexcept;

			using remove_const = R( Args... ) volatile &;
			using remove_volatile = R( Args... ) &;
			using remove_const_volatile = R( Args... ) &;
			using remove_reference = R( Args... ) volatile;
			using remove_noexcept = R( Args... ) volatile &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) volatile && noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) volatile && noexcept;
			using object_t = void;
			using free_fn_t = R( Args... ) noexcept;
			using erased_free_fn_t = R( Args... ) noexcept;

			using add_const = R( Args... ) const volatile && noexcept;
			using add_volatile = R( Args... ) volatile && noexcept;
			using add_const_volatile = R( Args... ) const volatile && noexcept;
			using add_lvalue_reference = R( Args... ) volatile & noexcept;
			using add_rvalue_reference = R( Args... ) volatile && noexcept;
			using add_noexcept = R( Args... ) volatile && noexcept;

			using remove_const = R( Args... ) volatile && noexcept;
			using remove_volatile = R( Args... ) && noexcept;
			using remove_const_volatile = R( Args... ) && noexcept;
			using remove_reference = R( Args... ) volatile noexcept;
			using remove_noexcept = R( Args... ) volatile &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) volatile &&>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) volatile &&;
			using object_t = void;
			using free_fn_t = R( Args... );
			using erased_free_fn_t = R( Args... );

			using add_const = R( Args... ) const volatile &&;
			using add_volatile = R( Args... ) volatile &&;
			using add_const_volatile = R( Args... ) const volatile &&;
			using add_lvalue_reference = R( Args... ) volatile &;
			using add_rvalue_reference = R( Args... ) volatile &&;
			using add_noexcept = R( Args... ) volatile && noexcept;

			using remove_const = R( Args... ) volatile &&;
			using remove_volatile = R( Args... ) &&;
			using remove_const_volatile = R( Args... ) &&;
			using remove_reference = R( Args... ) volatile;
			using remove_noexcept = R( Args... ) volatile &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) noexcept;
			using object_t = void;
			using free_fn_t = R( Args... ) noexcept;
			using erased_free_fn_t = R( Args... ) noexcept;

			using add_const = R( Args... ) const noexcept;
			using add_volatile = R( Args... ) volatile noexcept;
			using add_const_volatile = R( Args... ) const volatile noexcept;
			using add_lvalue_reference = R( Args... ) & noexcept;
			using add_rvalue_reference = R( Args... ) && noexcept;
			using add_noexcept = R( Args... ) noexcept;

			using remove_const = R( Args... ) noexcept;
			using remove_volatile = R( Args... ) noexcept;
			using remove_const_volatile = R( Args... ) noexcept;
			using remove_reference = R( Args... ) noexcept;
			using remove_noexcept = R( Args... );

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... )>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... );
			using object_t = void;
			using free_fn_t = R( Args... );
			using erased_free_fn_t = R( Args... );

			using add_const = R( Args... ) const;
			using add_volatile = R( Args... ) volatile;
			using add_const_volatile = R( Args... ) const volatile;
			using add_lvalue_reference = R( Args... ) &;
			using add_rvalue_reference = R( Args... ) &&;
			using add_noexcept = R( Args... ) noexcept;

			using remove_const = R( Args... );
			using remove_volatile = R( Args... );
			using remove_const_volatile = R( Args... );
			using remove_reference = R( Args... );
			using remove_noexcept = R( Args... );

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) & noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) & noexcept;
			using object_t = void;
			using free_fn_t = R( Args... ) noexcept;
			using erased_free_fn_t = R( Args... ) noexcept;

			using add_const = R( Args... ) const & noexcept;
			using add_volatile = R( Args... ) volatile & noexcept;
			using add_const_volatile = R( Args... ) const volatile & noexcept;
			using add_lvalue_reference = R( Args... ) & noexcept;
			using add_rvalue_reference = R( Args... ) && noexcept;
			using add_noexcept = R( Args... ) & noexcept;

			using remove_const = R( Args... ) & noexcept;
			using remove_volatile = R( Args... ) & noexcept;
			using remove_const_volatile = R( Args... ) & noexcept;
			using remove_reference = R( Args... ) noexcept;
			using remove_noexcept = R( Args... ) &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) &>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) &;
			using object_t = void;
			using free_fn_t = R( Args... );
			using erased_free_fn_t = R( Args... );

			using add_const = R( Args... ) const &;
			using add_volatile = R( Args... ) volatile &;
			using add_const_volatile = R( Args... ) const volatile &;
			using add_lvalue_reference = R( Args... ) &;
			using add_rvalue_reference = R( Args... ) &&;
			using add_noexcept = R( Args... ) & noexcept;

			using remove_const = R( Args... ) &;
			using remove_volatile = R( Args... ) &;
			using remove_const_volatile = R( Args... ) &;
			using remove_reference = R( Args... );
			using remove_noexcept = R( Args... ) &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) && noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) && noexcept;
			using object_t = void;
			using free_fn_t = R( Args... ) noexcept;
			using erased_free_fn_t = R( Args... ) noexcept;

			using add_const = R( Args... ) const && noexcept;
			using add_volatile = R( Args... ) volatile && noexcept;
			using add_const_volatile = R( Args... ) const volatile && noexcept;
			using add_lvalue_reference = R( Args... ) & noexcept;
			using add_rvalue_reference = R( Args... ) && noexcept;
			using add_noexcept = R( Args... ) && noexcept;

			using remove_const = R( Args... ) && noexcept;
			using remove_volatile = R( Args... ) && noexcept;
			using remove_const_volatile = R( Args... ) && noexcept;
			using remove_reference = R( Args... ) noexcept;
			using remove_noexcept = R( Args... ) &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R( Args... ) &&>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) &&;
			using object_t = void;
			using free_fn_t = R( Args... );
			using erased_free_fn_t = R( Args... );

			using add_const = R( Args... ) const &&;
			using add_volatile = R( Args... ) volatile &&;
			using add_const_volatile = R( Args... ) const volatile &&;
			using add_lvalue_reference = R( Args... ) &;
			using add_rvalue_reference = R( Args... ) &&;
			using add_noexcept = R( Args... ) && noexcept;

			using remove_const = R( Args... ) &&;
			using remove_volatile = R( Args... ) &&;
			using remove_const_volatile = R( Args... ) &&;
			using remove_reference = R( Args... );
			using remove_noexcept = R( Args... ) &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = true;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R ( & )( Args..., ... ) noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) noexcept;
			using object_t = void;
			using free_fn_t = R( Args..., ... ) noexcept;
			using erased_free_fn_t = R( Args..., ... ) noexcept;

			using add_const = R ( & )( Args..., ... ) noexcept;
			using add_volatile = R ( & )( Args..., ... ) noexcept;
			using add_const_volatile = R ( & )( Args..., ... ) noexcept;
			using add_lvalue_reference = R ( & )( Args..., ... ) noexcept;
			using add_rvalue_reference = R ( & )( Args..., ... ) noexcept;
			using add_noexcept = R ( & )( Args..., ... ) noexcept;

			using remove_const = R ( & )( Args..., ... ) noexcept;
			using remove_volatile = R ( & )( Args..., ... ) noexcept;
			using remove_const_volatile = R ( & )( Args..., ... ) noexcept;
			using remove_reference = R ( & )( Args..., ... ) noexcept;
			using remove_noexcept = R ( & )( Args..., ... );

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R ( & )( Args..., ... )>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... );
			using object_t = void;
			using free_fn_t = R( Args..., ... );
			using erased_free_fn_t = R( Args..., ... );

			using add_const = R ( & )( Args..., ... );
			using add_volatile = R ( & )( Args..., ... );
			using add_const_volatile = R ( & )( Args..., ... );
			using add_lvalue_reference = R ( & )( Args..., ... );
			using add_rvalue_reference = R ( & )( Args..., ... );
			using add_noexcept = R ( & )( Args..., ... ) noexcept;

			using remove_const = R ( & )( Args..., ... );
			using remove_volatile = R ( & )( Args..., ... );
			using remove_const_volatile = R ( & )( Args..., ... );
			using remove_reference = R ( & )( Args..., ... );
			using remove_noexcept = R ( & )( Args..., ... );

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R ( & )( Args... ) noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) noexcept;
			using object_t = void;
			using free_fn_t = R( Args... ) noexcept;
			using erased_free_fn_t = R( Args... ) noexcept;

			using add_const = R ( & )( Args... ) noexcept;
			using add_volatile = R ( & )( Args... ) noexcept;
			using add_const_volatile = R ( & )( Args... ) noexcept;
			using add_lvalue_reference = R ( & )( Args... ) noexcept;
			using add_rvalue_reference = R ( & )( Args... ) noexcept;
			using add_noexcept = R ( & )( Args... ) noexcept;

			using remove_const = R ( & )( Args... ) noexcept;
			using remove_volatile = R ( & )( Args... ) noexcept;
			using remove_const_volatile = R ( & )( Args... ) noexcept;
			using remove_reference = R ( & )( Args... ) noexcept;
			using remove_noexcept = R ( & )( Args... );

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R ( & )( Args... )>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... );
			using object_t = void;
			using free_fn_t = R( Args... );
			using erased_free_fn_t = R( Args... );

			using add_const = R ( & )( Args... );
			using add_volatile = R ( & )( Args... );
			using add_const_volatile = R ( & )( Args... );
			using add_lvalue_reference = R ( & )( Args... );
			using add_rvalue_reference = R ( & )( Args... );
			using add_noexcept = R ( & )( Args... ) noexcept;

			using remove_const = R ( & )( Args... );
			using remove_volatile = R ( & )( Args... );
			using remove_const_volatile = R ( & )( Args... );
			using remove_reference = R ( & )( Args... );
			using remove_noexcept = R ( & )( Args... );

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R ( * )( Args..., ... ) noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) noexcept;
			using object_t = void;
			using free_fn_t = R( Args..., ... ) noexcept;
			using erased_free_fn_t = R( Args..., ... ) noexcept;

			using add_const = R ( * )( Args..., ... ) noexcept;
			using add_volatile = R ( * )( Args..., ... ) noexcept;
			using add_const_volatile = R ( * )( Args..., ... ) noexcept;
			using add_lvalue_reference = R ( * )( Args..., ... ) noexcept;
			using add_rvalue_reference = R ( * )( Args..., ... ) noexcept;
			using add_noexcept = R ( * )( Args..., ... ) noexcept;

			using remove_const = R ( * )( Args..., ... ) noexcept;
			using remove_volatile = R ( * )( Args..., ... ) noexcept;
			using remove_const_volatile = R ( * )( Args..., ... ) noexcept;
			using remove_reference = R ( * )( Args..., ... ) noexcept;
			using remove_noexcept = R ( * )( Args..., ... );

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R ( * )( Args..., ... )>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... );
			using object_t = void;
			using free_fn_t = R( Args..., ... );
			using erased_free_fn_t = R( Args..., ... );

			using add_const = R ( * )( Args..., ... );
			using add_volatile = R ( * )( Args..., ... );
			using add_const_volatile = R ( * )( Args..., ... );
			using add_lvalue_reference = R ( * )( Args..., ... );
			using add_rvalue_reference = R ( * )( Args..., ... );
			using add_noexcept = R ( * )( Args..., ... ) noexcept;

			using remove_const = R ( * )( Args..., ... );
			using remove_volatile = R ( * )( Args..., ... );
			using remove_const_volatile = R ( * )( Args..., ... );
			using remove_reference = R ( * )( Args..., ... );
			using remove_noexcept = R ( * )( Args..., ... );

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R ( * )( Args... ) noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) noexcept;
			using object_t = void;
			using free_fn_t = R( Args... ) noexcept;
			using erased_free_fn_t = R( Args... ) noexcept;

			using add_const = R ( * )( Args... ) noexcept;
			using add_volatile = R ( * )( Args... ) noexcept;
			using add_const_volatile = R ( * )( Args... ) noexcept;
			using add_lvalue_reference = R ( * )( Args... ) noexcept;
			using add_rvalue_reference = R ( * )( Args... ) noexcept;
			using add_noexcept = R ( * )( Args... ) noexcept;

			using remove_const = R ( * )( Args... ) noexcept;
			using remove_volatile = R ( * )( Args... ) noexcept;
			using remove_const_volatile = R ( * )( Args... ) noexcept;
			using remove_reference = R ( * )( Args... ) noexcept;
			using remove_noexcept = R ( * )( Args... );

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename... Args>
		struct FnTraits<R ( * )( Args... )>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... );
			using object_t = void;
			using free_fn_t = R( Args... );
			using erased_free_fn_t = R( Args... );

			using add_const = R ( * )( Args... );
			using add_volatile = R ( * )( Args... );
			using add_const_volatile = R ( * )( Args... );
			using add_lvalue_reference = R ( * )( Args... );
			using add_rvalue_reference = R ( * )( Args... );
			using add_noexcept = R ( * )( Args... ) noexcept;

			using remove_const = R ( * )( Args... );
			using remove_volatile = R ( * )( Args... );
			using remove_const_volatile = R ( * )( Args... );
			using remove_reference = R ( * )( Args... );
			using remove_noexcept = R ( * )( Args... );

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = false;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) const volatile noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const volatile noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj const volatile &, Args..., ... ) noexcept;
			using erased_free_fn_t = R( void const volatile *, Args..., ... ) noexcept;

			using add_const = R ( Obj::* )( Args..., ... ) const volatile noexcept;
			using add_volatile = R ( Obj::* )( Args..., ... ) const volatile noexcept;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) const volatile & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) const volatile && noexcept;
			using add_noexcept = R ( Obj::* )( Args..., ... ) const volatile noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) volatile noexcept;
			using remove_volatile = R ( Obj::* )( Args..., ... ) const noexcept;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) noexcept;
			using remove_reference = R ( Obj::* )( Args..., ... ) const volatile noexcept;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) const volatile;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) const volatile>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const volatile;
			using object_t = Obj;
			using free_fn_t = R( Obj const volatile &, Args..., ... );
			using erased_free_fn_t = R( void const volatile *, Args..., ... );

			using add_const = R ( Obj::* )( Args..., ... ) const volatile;
			using add_volatile = R ( Obj::* )( Args..., ... ) const volatile;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) const volatile &;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) const volatile &&;
			using add_noexcept = R ( Obj::* )( Args..., ... ) const volatile noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) volatile;
			using remove_volatile = R ( Obj::* )( Args..., ... ) const;
			using remove_const_volatile = R ( Obj::* )( Args..., ... );
			using remove_reference = R ( Obj::* )( Args..., ... ) const volatile;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) const volatile;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) const volatile & noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const volatile & noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj const volatile &, Args..., ... ) noexcept;
			using erased_free_fn_t = R( void const volatile *, Args..., ... ) noexcept;

			using add_const = R ( Obj::* )( Args..., ... ) const volatile & noexcept;
			using add_volatile = R ( Obj::* )( Args..., ... ) const volatile & noexcept;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile & noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) const volatile & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) const volatile && noexcept;
			using add_noexcept = R ( Obj::* )( Args..., ... ) const volatile & noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) volatile & noexcept;
			using remove_volatile = R ( Obj::* )( Args..., ... ) const & noexcept;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) & noexcept;
			using remove_reference = R ( Obj::* )( Args..., ... ) const volatile noexcept;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) const volatile &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) const volatile &>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const volatile &;
			using object_t = Obj;
			using free_fn_t = R( Obj const volatile &, Args..., ... );
			using erased_free_fn_t = R( void const volatile *, Args..., ... );

			using add_const = R ( Obj::* )( Args..., ... ) const volatile &;
			using add_volatile = R ( Obj::* )( Args..., ... ) const volatile &;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile &;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) const volatile &;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) const volatile &&;
			using add_noexcept = R ( Obj::* )( Args..., ... ) const volatile & noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) volatile &;
			using remove_volatile = R ( Obj::* )( Args..., ... ) const &;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) &;
			using remove_reference = R ( Obj::* )( Args..., ... ) const volatile;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) const volatile &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) const volatile && noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const volatile && noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj const volatile &&, Args..., ... ) noexcept;
			using erased_free_fn_t = R( void const volatile *, Args..., ... ) noexcept;

			using add_const = R ( Obj::* )( Args..., ... ) const volatile && noexcept;
			using add_volatile = R ( Obj::* )( Args..., ... ) const volatile && noexcept;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile && noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) const volatile & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) const volatile && noexcept;
			using add_noexcept = R ( Obj::* )( Args..., ... ) const volatile && noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) volatile && noexcept;
			using remove_volatile = R ( Obj::* )( Args..., ... ) const && noexcept;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) && noexcept;
			using remove_reference = R ( Obj::* )( Args..., ... ) const volatile noexcept;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) const volatile &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) const volatile &&>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const volatile &&;
			using object_t = Obj;
			using free_fn_t = R( Obj const volatile &&, Args..., ... );
			using erased_free_fn_t = R( void const volatile *, Args..., ... );

			using add_const = R ( Obj::* )( Args..., ... ) const volatile &&;
			using add_volatile = R ( Obj::* )( Args..., ... ) const volatile &&;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile &&;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) const volatile &;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) const volatile &&;
			using add_noexcept = R ( Obj::* )( Args..., ... ) const volatile && noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) volatile &&;
			using remove_volatile = R ( Obj::* )( Args..., ... ) const &&;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) &&;
			using remove_reference = R ( Obj::* )( Args..., ... ) const volatile;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) const volatile &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) const noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj const &, Args..., ... ) noexcept;
			using erased_free_fn_t = R( void const *, Args..., ... ) noexcept;

			using add_const = R ( Obj::* )( Args..., ... ) const noexcept;
			using add_volatile = R ( Obj::* )( Args..., ... ) const volatile noexcept;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) const & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) const && noexcept;
			using add_noexcept = R ( Obj::* )( Args..., ... ) const noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) noexcept;
			using remove_volatile = R ( Obj::* )( Args..., ... ) const noexcept;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) noexcept;
			using remove_reference = R ( Obj::* )( Args..., ... ) const noexcept;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) const;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) const>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const;
			using object_t = Obj;
			using free_fn_t = R( Obj const &, Args..., ... );
			using erased_free_fn_t = R( void const *, Args..., ... );

			using add_const = R ( Obj::* )( Args..., ... ) const;
			using add_volatile = R ( Obj::* )( Args..., ... ) const volatile;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) const &;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) const &&;
			using add_noexcept = R ( Obj::* )( Args..., ... ) const noexcept;

			using remove_const = R ( Obj::* )( Args..., ... );
			using remove_volatile = R ( Obj::* )( Args..., ... ) const;
			using remove_const_volatile = R ( Obj::* )( Args..., ... );
			using remove_reference = R ( Obj::* )( Args..., ... ) const;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) const;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) const & noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const & noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj const &, Args..., ... ) noexcept;
			using erased_free_fn_t = R( void const *, Args..., ... ) noexcept;

			using add_const = R ( Obj::* )( Args..., ... ) const & noexcept;
			using add_volatile = R ( Obj::* )( Args..., ... ) const volatile & noexcept;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile & noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) const & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) const && noexcept;
			using add_noexcept = R ( Obj::* )( Args..., ... ) const & noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) & noexcept;
			using remove_volatile = R ( Obj::* )( Args..., ... ) const & noexcept;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) & noexcept;
			using remove_reference = R ( Obj::* )( Args..., ... ) const noexcept;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) const &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) const &>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const &;
			using object_t = Obj;
			using free_fn_t = R( Obj const &, Args..., ... );
			using erased_free_fn_t = R( void const *, Args..., ... );

			using add_const = R ( Obj::* )( Args..., ... ) const &;
			using add_volatile = R ( Obj::* )( Args..., ... ) const volatile &;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile &;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) const &;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) const &&;
			using add_noexcept = R ( Obj::* )( Args..., ... ) const & noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) &;
			using remove_volatile = R ( Obj::* )( Args..., ... ) const &;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) &;
			using remove_reference = R ( Obj::* )( Args..., ... ) const;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) const &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) const && noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const && noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj const &&, Args..., ... ) noexcept;
			using erased_free_fn_t = R( void const *, Args..., ... ) noexcept;

			using add_const = R ( Obj::* )( Args..., ... ) const && noexcept;
			using add_volatile = R ( Obj::* )( Args..., ... ) const volatile && noexcept;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile && noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) const & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) const && noexcept;
			using add_noexcept = R ( Obj::* )( Args..., ... ) const && noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) && noexcept;
			using remove_volatile = R ( Obj::* )( Args..., ... ) const && noexcept;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) && noexcept;
			using remove_reference = R ( Obj::* )( Args..., ... ) const noexcept;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) const &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) const &&>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) const &&;
			using object_t = Obj;
			using free_fn_t = R( Obj const &&, Args..., ... );
			using erased_free_fn_t = R( void const *, Args..., ... );

			using add_const = R ( Obj::* )( Args..., ... ) const &&;
			using add_volatile = R ( Obj::* )( Args..., ... ) const volatile &&;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile &&;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) const &;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) const &&;
			using add_noexcept = R ( Obj::* )( Args..., ... ) const && noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) &&;
			using remove_volatile = R ( Obj::* )( Args..., ... ) const &&;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) &&;
			using remove_reference = R ( Obj::* )( Args..., ... ) const;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) const &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) volatile noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) volatile noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj volatile &, Args..., ... ) noexcept;
			using erased_free_fn_t = R( void volatile *, Args..., ... ) noexcept;

			using add_const = R ( Obj::* )( Args..., ... ) const volatile noexcept;
			using add_volatile = R ( Obj::* )( Args..., ... ) volatile noexcept;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) volatile & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) volatile && noexcept;
			using add_noexcept = R ( Obj::* )( Args..., ... ) volatile noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) volatile noexcept;
			using remove_volatile = R ( Obj::* )( Args..., ... ) noexcept;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) noexcept;
			using remove_reference = R ( Obj::* )( Args..., ... ) volatile noexcept;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) volatile;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) volatile>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) volatile;
			using object_t = Obj;
			using free_fn_t = R( Obj volatile &, Args..., ... );
			using erased_free_fn_t = R( void volatile *, Args..., ... );

			using add_const = R ( Obj::* )( Args..., ... ) const volatile;
			using add_volatile = R ( Obj::* )( Args..., ... ) volatile;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) volatile &;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) volatile &&;
			using add_noexcept = R ( Obj::* )( Args..., ... ) volatile noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) volatile;
			using remove_volatile = R ( Obj::* )( Args..., ... );
			using remove_const_volatile = R ( Obj::* )( Args..., ... );
			using remove_reference = R ( Obj::* )( Args..., ... ) volatile;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) volatile;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) volatile & noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) volatile & noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj volatile &, Args..., ... ) noexcept;
			using erased_free_fn_t = R( void volatile *, Args..., ... ) noexcept;

			using add_const = R ( Obj::* )( Args..., ... ) const volatile & noexcept;
			using add_volatile = R ( Obj::* )( Args..., ... ) volatile & noexcept;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile & noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) volatile & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) volatile && noexcept;
			using add_noexcept = R ( Obj::* )( Args..., ... ) volatile & noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) volatile & noexcept;
			using remove_volatile = R ( Obj::* )( Args..., ... ) & noexcept;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) & noexcept;
			using remove_reference = R ( Obj::* )( Args..., ... ) volatile noexcept;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) volatile &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) volatile &>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) volatile &;
			using object_t = Obj;
			using free_fn_t = R( Obj volatile &, Args..., ... );
			using erased_free_fn_t = R( void volatile *, Args..., ... );

			using add_const = R ( Obj::* )( Args..., ... ) const volatile &;
			using add_volatile = R ( Obj::* )( Args..., ... ) volatile &;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile &;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) volatile &;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) volatile &&;
			using add_noexcept = R ( Obj::* )( Args..., ... ) volatile & noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) volatile &;
			using remove_volatile = R ( Obj::* )( Args..., ... ) &;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) &;
			using remove_reference = R ( Obj::* )( Args..., ... ) volatile;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) volatile &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) volatile && noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) volatile && noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj volatile &&, Args..., ... ) noexcept;
			using erased_free_fn_t = R( void volatile *, Args..., ... ) noexcept;

			using add_const = R ( Obj::* )( Args..., ... ) const volatile && noexcept;
			using add_volatile = R ( Obj::* )( Args..., ... ) volatile && noexcept;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile && noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) volatile & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) volatile && noexcept;
			using add_noexcept = R ( Obj::* )( Args..., ... ) volatile && noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) volatile && noexcept;
			using remove_volatile = R ( Obj::* )( Args..., ... ) && noexcept;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) && noexcept;
			using remove_reference = R ( Obj::* )( Args..., ... ) volatile noexcept;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) volatile &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) volatile &&>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) volatile &&;
			using object_t = Obj;
			using free_fn_t = R( Obj volatile &&, Args..., ... );
			using erased_free_fn_t = R( void volatile *, Args..., ... );

			using add_const = R ( Obj::* )( Args..., ... ) const volatile &&;
			using add_volatile = R ( Obj::* )( Args..., ... ) volatile &&;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile &&;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) volatile &;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) volatile &&;
			using add_noexcept = R ( Obj::* )( Args..., ... ) volatile && noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) volatile &&;
			using remove_volatile = R ( Obj::* )( Args..., ... ) &&;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) &&;
			using remove_reference = R ( Obj::* )( Args..., ... ) volatile;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) volatile &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj &, Args..., ... ) noexcept;
			using erased_free_fn_t = R( void *, Args..., ... ) noexcept;

			using add_const = R ( Obj::* )( Args..., ... ) const noexcept;
			using add_volatile = R ( Obj::* )( Args..., ... ) volatile noexcept;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) && noexcept;
			using add_noexcept = R ( Obj::* )( Args..., ... ) noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) noexcept;
			using remove_volatile = R ( Obj::* )( Args..., ... ) noexcept;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) noexcept;
			using remove_reference = R ( Obj::* )( Args..., ... ) noexcept;
			using remove_noexcept = R ( Obj::* )( Args..., ... );

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... )>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... );
			using object_t = Obj;
			using free_fn_t = R( Obj &, Args..., ... );
			using erased_free_fn_t = R( void *, Args..., ... );

			using add_const = R ( Obj::* )( Args..., ... ) const;
			using add_volatile = R ( Obj::* )( Args..., ... ) volatile;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) &;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) &&;
			using add_noexcept = R ( Obj::* )( Args..., ... ) noexcept;

			using remove_const = R ( Obj::* )( Args..., ... );
			using remove_volatile = R ( Obj::* )( Args..., ... );
			using remove_const_volatile = R ( Obj::* )( Args..., ... );
			using remove_reference = R ( Obj::* )( Args..., ... );
			using remove_noexcept = R ( Obj::* )( Args..., ... );

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) & noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) & noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj &, Args..., ... ) noexcept;
			using erased_free_fn_t = R( void *, Args..., ... ) noexcept;

			using add_const = R ( Obj::* )( Args..., ... ) const & noexcept;
			using add_volatile = R ( Obj::* )( Args..., ... ) volatile & noexcept;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile & noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) && noexcept;
			using add_noexcept = R ( Obj::* )( Args..., ... ) & noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) & noexcept;
			using remove_volatile = R ( Obj::* )( Args..., ... ) & noexcept;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) & noexcept;
			using remove_reference = R ( Obj::* )( Args..., ... ) noexcept;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) &>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) &;
			using object_t = Obj;
			using free_fn_t = R( Obj &, Args..., ... );
			using erased_free_fn_t = R( void *, Args..., ... );

			using add_const = R ( Obj::* )( Args..., ... ) const &;
			using add_volatile = R ( Obj::* )( Args..., ... ) volatile &;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile &;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) &;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) &&;
			using add_noexcept = R ( Obj::* )( Args..., ... ) & noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) &;
			using remove_volatile = R ( Obj::* )( Args..., ... ) &;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) &;
			using remove_reference = R ( Obj::* )( Args..., ... );
			using remove_noexcept = R ( Obj::* )( Args..., ... ) &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) && noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) && noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj &&, Args..., ... ) noexcept;
			using erased_free_fn_t = R( void *, Args..., ... ) noexcept;

			using add_const = R ( Obj::* )( Args..., ... ) const && noexcept;
			using add_volatile = R ( Obj::* )( Args..., ... ) volatile && noexcept;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile && noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) && noexcept;
			using add_noexcept = R ( Obj::* )( Args..., ... ) && noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) && noexcept;
			using remove_volatile = R ( Obj::* )( Args..., ... ) && noexcept;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) && noexcept;
			using remove_reference = R ( Obj::* )( Args..., ... ) noexcept;
			using remove_noexcept = R ( Obj::* )( Args..., ... ) &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args..., ... ) &&>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args..., ... ) &&;
			using object_t = Obj;
			using free_fn_t = R( Obj &&, Args..., ... );
			using erased_free_fn_t = R( void *, Args..., ... );

			using add_const = R ( Obj::* )( Args..., ... ) const &&;
			using add_volatile = R ( Obj::* )( Args..., ... ) volatile &&;
			using add_const_volatile = R ( Obj::* )( Args..., ... ) const volatile &&;
			using add_lvalue_reference = R ( Obj::* )( Args..., ... ) &;
			using add_rvalue_reference = R ( Obj::* )( Args..., ... ) &&;
			using add_noexcept = R ( Obj::* )( Args..., ... ) && noexcept;

			using remove_const = R ( Obj::* )( Args..., ... ) &&;
			using remove_volatile = R ( Obj::* )( Args..., ... ) &&;
			using remove_const_volatile = R ( Obj::* )( Args..., ... ) &&;
			using remove_reference = R ( Obj::* )( Args..., ... );
			using remove_noexcept = R ( Obj::* )( Args..., ... ) &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = true;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) const volatile noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const volatile noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj const volatile &, Args... ) noexcept;
			using erased_free_fn_t = R( void const volatile *, Args... ) noexcept;

			using add_const = R ( Obj::* )( Args... ) const volatile noexcept;
			using add_volatile = R ( Obj::* )( Args... ) const volatile noexcept;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args... ) const volatile & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args... ) const volatile && noexcept;
			using add_noexcept = R ( Obj::* )( Args... ) const volatile noexcept;

			using remove_const = R ( Obj::* )( Args... ) volatile noexcept;
			using remove_volatile = R ( Obj::* )( Args... ) const noexcept;
			using remove_const_volatile = R ( Obj::* )( Args... ) noexcept;
			using remove_reference = R ( Obj::* )( Args... ) const volatile noexcept;
			using remove_noexcept = R ( Obj::* )( Args... ) const volatile;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) const volatile>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const volatile;
			using object_t = Obj;
			using free_fn_t = R( Obj const volatile &, Args... );
			using erased_free_fn_t = R( void const volatile *, Args... );

			using add_const = R ( Obj::* )( Args... ) const volatile;
			using add_volatile = R ( Obj::* )( Args... ) const volatile;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile;
			using add_lvalue_reference = R ( Obj::* )( Args... ) const volatile &;
			using add_rvalue_reference = R ( Obj::* )( Args... ) const volatile &&;
			using add_noexcept = R ( Obj::* )( Args... ) const volatile noexcept;

			using remove_const = R ( Obj::* )( Args... ) volatile;
			using remove_volatile = R ( Obj::* )( Args... ) const;
			using remove_const_volatile = R ( Obj::* )( Args... );
			using remove_reference = R ( Obj::* )( Args... ) const volatile;
			using remove_noexcept = R ( Obj::* )( Args... ) const volatile;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) const volatile & noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const volatile & noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj const volatile &, Args... ) noexcept;
			using erased_free_fn_t = R( void const volatile *, Args... ) noexcept;

			using add_const = R ( Obj::* )( Args... ) const volatile & noexcept;
			using add_volatile = R ( Obj::* )( Args... ) const volatile & noexcept;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile & noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args... ) const volatile & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args... ) const volatile && noexcept;
			using add_noexcept = R ( Obj::* )( Args... ) const volatile & noexcept;

			using remove_const = R ( Obj::* )( Args... ) volatile & noexcept;
			using remove_volatile = R ( Obj::* )( Args... ) const & noexcept;
			using remove_const_volatile = R ( Obj::* )( Args... ) & noexcept;
			using remove_reference = R ( Obj::* )( Args... ) const volatile noexcept;
			using remove_noexcept = R ( Obj::* )( Args... ) const volatile &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) const volatile &>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const volatile &;
			using object_t = Obj;
			using free_fn_t = R( Obj const volatile &, Args... );
			using erased_free_fn_t = R( void const volatile *, Args... );

			using add_const = R ( Obj::* )( Args... ) const volatile &;
			using add_volatile = R ( Obj::* )( Args... ) const volatile &;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile &;
			using add_lvalue_reference = R ( Obj::* )( Args... ) const volatile &;
			using add_rvalue_reference = R ( Obj::* )( Args... ) const volatile &&;
			using add_noexcept = R ( Obj::* )( Args... ) const volatile & noexcept;

			using remove_const = R ( Obj::* )( Args... ) volatile &;
			using remove_volatile = R ( Obj::* )( Args... ) const &;
			using remove_const_volatile = R ( Obj::* )( Args... ) &;
			using remove_reference = R ( Obj::* )( Args... ) const volatile;
			using remove_noexcept = R ( Obj::* )( Args... ) const volatile &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) const volatile && noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const volatile && noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj const volatile &&, Args... ) noexcept;
			using erased_free_fn_t = R( void const volatile *, Args... ) noexcept;

			using add_const = R ( Obj::* )( Args... ) const volatile && noexcept;
			using add_volatile = R ( Obj::* )( Args... ) const volatile && noexcept;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile && noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args... ) const volatile & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args... ) const volatile && noexcept;
			using add_noexcept = R ( Obj::* )( Args... ) const volatile && noexcept;

			using remove_const = R ( Obj::* )( Args... ) volatile && noexcept;
			using remove_volatile = R ( Obj::* )( Args... ) const && noexcept;
			using remove_const_volatile = R ( Obj::* )( Args... ) && noexcept;
			using remove_reference = R ( Obj::* )( Args... ) const volatile noexcept;
			using remove_noexcept = R ( Obj::* )( Args... ) const volatile &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) const volatile &&>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const volatile &&;
			using object_t = Obj;
			using free_fn_t = R( Obj const volatile &&, Args... );
			using erased_free_fn_t = R( void const volatile *, Args... );

			using add_const = R ( Obj::* )( Args... ) const volatile &&;
			using add_volatile = R ( Obj::* )( Args... ) const volatile &&;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile &&;
			using add_lvalue_reference = R ( Obj::* )( Args... ) const volatile &;
			using add_rvalue_reference = R ( Obj::* )( Args... ) const volatile &&;
			using add_noexcept = R ( Obj::* )( Args... ) const volatile && noexcept;

			using remove_const = R ( Obj::* )( Args... ) volatile &&;
			using remove_volatile = R ( Obj::* )( Args... ) const &&;
			using remove_const_volatile = R ( Obj::* )( Args... ) &&;
			using remove_reference = R ( Obj::* )( Args... ) const volatile;
			using remove_noexcept = R ( Obj::* )( Args... ) const volatile &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) const noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj const &, Args... ) noexcept;
			using erased_free_fn_t = R( void const *, Args... ) noexcept;

			using add_const = R ( Obj::* )( Args... ) const noexcept;
			using add_volatile = R ( Obj::* )( Args... ) const volatile noexcept;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args... ) const & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args... ) const && noexcept;
			using add_noexcept = R ( Obj::* )( Args... ) const noexcept;

			using remove_const = R ( Obj::* )( Args... ) noexcept;
			using remove_volatile = R ( Obj::* )( Args... ) const noexcept;
			using remove_const_volatile = R ( Obj::* )( Args... ) noexcept;
			using remove_reference = R ( Obj::* )( Args... ) const noexcept;
			using remove_noexcept = R ( Obj::* )( Args... ) const;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) const>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const;
			using object_t = Obj;
			using free_fn_t = R( Obj const &, Args... );
			using erased_free_fn_t = R( void const *, Args... );

			using add_const = R ( Obj::* )( Args... ) const;
			using add_volatile = R ( Obj::* )( Args... ) const volatile;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile;
			using add_lvalue_reference = R ( Obj::* )( Args... ) const &;
			using add_rvalue_reference = R ( Obj::* )( Args... ) const &&;
			using add_noexcept = R ( Obj::* )( Args... ) const noexcept;

			using remove_const = R ( Obj::* )( Args... );
			using remove_volatile = R ( Obj::* )( Args... ) const;
			using remove_const_volatile = R ( Obj::* )( Args... );
			using remove_reference = R ( Obj::* )( Args... ) const;
			using remove_noexcept = R ( Obj::* )( Args... ) const;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) const & noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const & noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj const &, Args... ) noexcept;
			using erased_free_fn_t = R( void const *, Args... ) noexcept;

			using add_const = R ( Obj::* )( Args... ) const & noexcept;
			using add_volatile = R ( Obj::* )( Args... ) const volatile & noexcept;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile & noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args... ) const & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args... ) const && noexcept;
			using add_noexcept = R ( Obj::* )( Args... ) const & noexcept;

			using remove_const = R ( Obj::* )( Args... ) & noexcept;
			using remove_volatile = R ( Obj::* )( Args... ) const & noexcept;
			using remove_const_volatile = R ( Obj::* )( Args... ) & noexcept;
			using remove_reference = R ( Obj::* )( Args... ) const noexcept;
			using remove_noexcept = R ( Obj::* )( Args... ) const &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) const &>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const &;
			using object_t = Obj;
			using free_fn_t = R( Obj const &, Args... );
			using erased_free_fn_t = R( void const *, Args... );

			using add_const = R ( Obj::* )( Args... ) const &;
			using add_volatile = R ( Obj::* )( Args... ) const volatile &;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile &;
			using add_lvalue_reference = R ( Obj::* )( Args... ) const &;
			using add_rvalue_reference = R ( Obj::* )( Args... ) const &&;
			using add_noexcept = R ( Obj::* )( Args... ) const & noexcept;

			using remove_const = R ( Obj::* )( Args... ) &;
			using remove_volatile = R ( Obj::* )( Args... ) const &;
			using remove_const_volatile = R ( Obj::* )( Args... ) &;
			using remove_reference = R ( Obj::* )( Args... ) const;
			using remove_noexcept = R ( Obj::* )( Args... ) const &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) const && noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const && noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj const &&, Args... ) noexcept;
			using erased_free_fn_t = R( void const *, Args... ) noexcept;

			using add_const = R ( Obj::* )( Args... ) const && noexcept;
			using add_volatile = R ( Obj::* )( Args... ) const volatile && noexcept;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile && noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args... ) const & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args... ) const && noexcept;
			using add_noexcept = R ( Obj::* )( Args... ) const && noexcept;

			using remove_const = R ( Obj::* )( Args... ) && noexcept;
			using remove_volatile = R ( Obj::* )( Args... ) const && noexcept;
			using remove_const_volatile = R ( Obj::* )( Args... ) && noexcept;
			using remove_reference = R ( Obj::* )( Args... ) const noexcept;
			using remove_noexcept = R ( Obj::* )( Args... ) const &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) const &&>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) const &&;
			using object_t = Obj;
			using free_fn_t = R( Obj const &&, Args... );
			using erased_free_fn_t = R( void const *, Args... );

			using add_const = R ( Obj::* )( Args... ) const &&;
			using add_volatile = R ( Obj::* )( Args... ) const volatile &&;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile &&;
			using add_lvalue_reference = R ( Obj::* )( Args... ) const &;
			using add_rvalue_reference = R ( Obj::* )( Args... ) const &&;
			using add_noexcept = R ( Obj::* )( Args... ) const && noexcept;

			using remove_const = R ( Obj::* )( Args... ) &&;
			using remove_volatile = R ( Obj::* )( Args... ) const &&;
			using remove_const_volatile = R ( Obj::* )( Args... ) &&;
			using remove_reference = R ( Obj::* )( Args... ) const;
			using remove_noexcept = R ( Obj::* )( Args... ) const &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = true;
			static constexpr bool is_mutable = false;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) volatile noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) volatile noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj volatile &, Args... ) noexcept;
			using erased_free_fn_t = R( void volatile *, Args... ) noexcept;

			using add_const = R ( Obj::* )( Args... ) const volatile noexcept;
			using add_volatile = R ( Obj::* )( Args... ) volatile noexcept;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args... ) volatile & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args... ) volatile && noexcept;
			using add_noexcept = R ( Obj::* )( Args... ) volatile noexcept;

			using remove_const = R ( Obj::* )( Args... ) volatile noexcept;
			using remove_volatile = R ( Obj::* )( Args... ) noexcept;
			using remove_const_volatile = R ( Obj::* )( Args... ) noexcept;
			using remove_reference = R ( Obj::* )( Args... ) volatile noexcept;
			using remove_noexcept = R ( Obj::* )( Args... ) volatile;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) volatile>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) volatile;
			using object_t = Obj;
			using free_fn_t = R( Obj volatile &, Args... );
			using erased_free_fn_t = R( void volatile *, Args... );

			using add_const = R ( Obj::* )( Args... ) const volatile;
			using add_volatile = R ( Obj::* )( Args... ) volatile;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile;
			using add_lvalue_reference = R ( Obj::* )( Args... ) volatile &;
			using add_rvalue_reference = R ( Obj::* )( Args... ) volatile &&;
			using add_noexcept = R ( Obj::* )( Args... ) volatile noexcept;

			using remove_const = R ( Obj::* )( Args... ) volatile;
			using remove_volatile = R ( Obj::* )( Args... );
			using remove_const_volatile = R ( Obj::* )( Args... );
			using remove_reference = R ( Obj::* )( Args... ) volatile;
			using remove_noexcept = R ( Obj::* )( Args... ) volatile;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) volatile & noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) volatile & noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj volatile &, Args... ) noexcept;
			using erased_free_fn_t = R( void volatile *, Args... ) noexcept;

			using add_const = R ( Obj::* )( Args... ) const volatile & noexcept;
			using add_volatile = R ( Obj::* )( Args... ) volatile & noexcept;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile & noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args... ) volatile & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args... ) volatile && noexcept;
			using add_noexcept = R ( Obj::* )( Args... ) volatile & noexcept;

			using remove_const = R ( Obj::* )( Args... ) volatile & noexcept;
			using remove_volatile = R ( Obj::* )( Args... ) & noexcept;
			using remove_const_volatile = R ( Obj::* )( Args... ) & noexcept;
			using remove_reference = R ( Obj::* )( Args... ) volatile noexcept;
			using remove_noexcept = R ( Obj::* )( Args... ) volatile &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) volatile &>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) volatile &;
			using object_t = Obj;
			using free_fn_t = R( Obj volatile &, Args... );
			using erased_free_fn_t = R( void volatile *, Args... );

			using add_const = R ( Obj::* )( Args... ) const volatile &;
			using add_volatile = R ( Obj::* )( Args... ) volatile &;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile &;
			using add_lvalue_reference = R ( Obj::* )( Args... ) volatile &;
			using add_rvalue_reference = R ( Obj::* )( Args... ) volatile &&;
			using add_noexcept = R ( Obj::* )( Args... ) volatile & noexcept;

			using remove_const = R ( Obj::* )( Args... ) volatile &;
			using remove_volatile = R ( Obj::* )( Args... ) &;
			using remove_const_volatile = R ( Obj::* )( Args... ) &;
			using remove_reference = R ( Obj::* )( Args... ) volatile;
			using remove_noexcept = R ( Obj::* )( Args... ) volatile &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) volatile && noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) volatile && noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj volatile &&, Args... ) noexcept;
			using erased_free_fn_t = R( void volatile *, Args... ) noexcept;

			using add_const = R ( Obj::* )( Args... ) const volatile && noexcept;
			using add_volatile = R ( Obj::* )( Args... ) volatile && noexcept;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile && noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args... ) volatile & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args... ) volatile && noexcept;
			using add_noexcept = R ( Obj::* )( Args... ) volatile && noexcept;

			using remove_const = R ( Obj::* )( Args... ) volatile && noexcept;
			using remove_volatile = R ( Obj::* )( Args... ) && noexcept;
			using remove_const_volatile = R ( Obj::* )( Args... ) && noexcept;
			using remove_reference = R ( Obj::* )( Args... ) volatile noexcept;
			using remove_noexcept = R ( Obj::* )( Args... ) volatile &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) volatile &&>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) volatile &&;
			using object_t = Obj;
			using free_fn_t = R( Obj volatile &&, Args... );
			using erased_free_fn_t = R( void volatile *, Args... );

			using add_const = R ( Obj::* )( Args... ) const volatile &&;
			using add_volatile = R ( Obj::* )( Args... ) volatile &&;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile &&;
			using add_lvalue_reference = R ( Obj::* )( Args... ) volatile &;
			using add_rvalue_reference = R ( Obj::* )( Args... ) volatile &&;
			using add_noexcept = R ( Obj::* )( Args... ) volatile && noexcept;

			using remove_const = R ( Obj::* )( Args... ) volatile &&;
			using remove_volatile = R ( Obj::* )( Args... ) &&;
			using remove_const_volatile = R ( Obj::* )( Args... ) &&;
			using remove_reference = R ( Obj::* )( Args... ) volatile;
			using remove_noexcept = R ( Obj::* )( Args... ) volatile &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = true;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj &, Args... ) noexcept;
			using erased_free_fn_t = R( void *, Args... ) noexcept;

			using add_const = R ( Obj::* )( Args... ) const noexcept;
			using add_volatile = R ( Obj::* )( Args... ) volatile noexcept;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args... ) & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args... ) && noexcept;
			using add_noexcept = R ( Obj::* )( Args... ) noexcept;

			using remove_const = R ( Obj::* )( Args... ) noexcept;
			using remove_volatile = R ( Obj::* )( Args... ) noexcept;
			using remove_const_volatile = R ( Obj::* )( Args... ) noexcept;
			using remove_reference = R ( Obj::* )( Args... ) noexcept;
			using remove_noexcept = R ( Obj::* )( Args... );

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... )>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... );
			using object_t = Obj;
			using free_fn_t = R( Obj &, Args... );
			using erased_free_fn_t = R( void *, Args... );

			using add_const = R ( Obj::* )( Args... ) const;
			using add_volatile = R ( Obj::* )( Args... ) volatile;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile;
			using add_lvalue_reference = R ( Obj::* )( Args... ) &;
			using add_rvalue_reference = R ( Obj::* )( Args... ) &&;
			using add_noexcept = R ( Obj::* )( Args... ) noexcept;

			using remove_const = R ( Obj::* )( Args... );
			using remove_volatile = R ( Obj::* )( Args... );
			using remove_const_volatile = R ( Obj::* )( Args... );
			using remove_reference = R ( Obj::* )( Args... );
			using remove_noexcept = R ( Obj::* )( Args... );

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) & noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) & noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj &, Args... ) noexcept;
			using erased_free_fn_t = R( void *, Args... ) noexcept;

			using add_const = R ( Obj::* )( Args... ) const & noexcept;
			using add_volatile = R ( Obj::* )( Args... ) volatile & noexcept;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile & noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args... ) & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args... ) && noexcept;
			using add_noexcept = R ( Obj::* )( Args... ) & noexcept;

			using remove_const = R ( Obj::* )( Args... ) & noexcept;
			using remove_volatile = R ( Obj::* )( Args... ) & noexcept;
			using remove_const_volatile = R ( Obj::* )( Args... ) & noexcept;
			using remove_reference = R ( Obj::* )( Args... ) noexcept;
			using remove_noexcept = R ( Obj::* )( Args... ) &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) &>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) &;
			using object_t = Obj;
			using free_fn_t = R( Obj &, Args... );
			using erased_free_fn_t = R( void *, Args... );

			using add_const = R ( Obj::* )( Args... ) const &;
			using add_volatile = R ( Obj::* )( Args... ) volatile &;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile &;
			using add_lvalue_reference = R ( Obj::* )( Args... ) &;
			using add_rvalue_reference = R ( Obj::* )( Args... ) &&;
			using add_noexcept = R ( Obj::* )( Args... ) & noexcept;

			using remove_const = R ( Obj::* )( Args... ) &;
			using remove_volatile = R ( Obj::* )( Args... ) &;
			using remove_const_volatile = R ( Obj::* )( Args... ) &;
			using remove_reference = R ( Obj::* )( Args... );
			using remove_noexcept = R ( Obj::* )( Args... ) &;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = true;
			static constexpr bool is_rvalue = false;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) && noexcept>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) && noexcept;
			using object_t = Obj;
			using free_fn_t = R( Obj &&, Args... ) noexcept;
			using erased_free_fn_t = R( void *, Args... ) noexcept;

			using add_const = R ( Obj::* )( Args... ) const && noexcept;
			using add_volatile = R ( Obj::* )( Args... ) volatile && noexcept;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile && noexcept;
			using add_lvalue_reference = R ( Obj::* )( Args... ) & noexcept;
			using add_rvalue_reference = R ( Obj::* )( Args... ) && noexcept;
			using add_noexcept = R ( Obj::* )( Args... ) && noexcept;

			using remove_const = R ( Obj::* )( Args... ) && noexcept;
			using remove_volatile = R ( Obj::* )( Args... ) && noexcept;
			using remove_const_volatile = R ( Obj::* )( Args... ) && noexcept;
			using remove_reference = R ( Obj::* )( Args... ) noexcept;
			using remove_noexcept = R ( Obj::* )( Args... ) &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = true;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
		template<typename R, typename Obj, typename... Args>
		struct FnTraits<R ( Obj::* )( Args... ) &&>
		{
			using result_t = R;
			using arguments_t = TL<Args...>;
			using abominable_t = R( Args... ) &&;
			using object_t = Obj;
			using free_fn_t = R( Obj &&, Args... );
			using erased_free_fn_t = R( void *, Args... );

			using add_const = R ( Obj::* )( Args... ) const &&;
			using add_volatile = R ( Obj::* )( Args... ) volatile &&;
			using add_const_volatile = R ( Obj::* )( Args... ) const volatile &&;
			using add_lvalue_reference = R ( Obj::* )( Args... ) &;
			using add_rvalue_reference = R ( Obj::* )( Args... ) &&;
			using add_noexcept = R ( Obj::* )( Args... ) && noexcept;

			using remove_const = R ( Obj::* )( Args... ) &&;
			using remove_volatile = R ( Obj::* )( Args... ) &&;
			using remove_const_volatile = R ( Obj::* )( Args... ) &&;
			using remove_reference = R ( Obj::* )( Args... );
			using remove_noexcept = R ( Obj::* )( Args... ) &&;

			static constexpr bool returns_void = std::is_void_v<result_t>;
			static constexpr bool has_variable_arguments = false;
			static constexpr bool is_abominable = false;
			static constexpr bool is_member = true;
			static constexpr bool is_const = false;
			static constexpr bool is_mutable = true;
			static constexpr bool is_volatile = false;
			static constexpr bool is_noexcept = false;
			static constexpr bool is_lvalue = false;
			static constexpr bool is_rvalue = true;
			static constexpr auto arguments_count = sizeof...( Args );
		};
	}
	/**
	 * @brief Get the result of a function.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using GetFnResult = typename Impl::FnTraits<T>::result_t;
	/**
	 * @brief Get the arguments of a function.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using GetFnArgs = typename Impl::FnTraits<T>::arguments_t;
	/**
	 * @brief Get the abominable function form of the given function.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using GetFnAbominable = typename Impl::FnTraits<T>::abominable_t;
	/**
	 * @brief Get the object associated with some object (or void if there isn't one).
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using GetFnObject = typename Impl::FnTraits<T>::object_t;
	/**
	 * @brief Get the free function that would be required to call the given function, if given a member function this produces a trampoline style function call.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using GetFreeFn = typename Impl::FnTraits<T>::free_fn_t;
	/**
	 * @brief Get the type erased free function that would be required to call a given function.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using GetErasedFreeFn = typename Impl::FnTraits<T>::erased_free_fn_t;
	/**
	 * @brief Get the result of an invokation fo some callable `F`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 * @tparam Args... The arguments to apply.
	 */
	template <typename F, typename ... Args>
	using GetFnInvokeResult = std::invoke_result_t<F, Args...>;
	/**
	 * @brief Add const to some function signature, or do nothing when not applicable.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using FnAddConst = typename Impl::FnTraits<T>::add_const;
	/**
	 * @brief Add volatile to the function signature, or do nothing if not applicable.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using FnAddVolatile = typename Impl::FnTraits<T>::add_volatile;
	/**
	 * @brief Add const and volatile to a function signature, or do nothing if not applicable.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using FnAddConstVolatile = typename Impl::FnTraits<T>::add_const_volatile;
	/**
	 * @brief Qualify the function with lvalue reference, or do nothing if not applicable.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using FnAddLvalueReference = typename Impl::FnTraits<T>::add_lvalue_reference;
	/**
	 * @brief Qualify the function with rvalue reference, or do nothing if not applicable.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using FnAddRvalueReference = typename Impl::FnTraits<T>::add_rvalue_reference;
	/**
	 * @brief Add noexcept to the function signature.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using FnAddNoexcept = typename Impl::FnTraits<T>::add_noexcept;
	/**
	 * @brief Remove const from the function signature; when present, otherwise do nothing.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using FnRemoveConst = typename Impl::FnTraits<T>::remove_const;
	/**
	 * @brief Remove volatile from the function signature; when present, otherwise do nothing.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using FnRemoveVolatile = typename Impl::FnTraits<T>::remove_volatile;
	/**
	 * @brief Remove const and volatile from the function signature; when present, otherwise do nothing.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using FnRemoveConstVolatile = typename Impl::FnTraits<T>::remove_const_volatile;
	/**
	 * @brief Remove reference qualifiers from the function signature; when present, otherwise do nothing.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using FnRemoveReference = typename Impl::FnTraits<T>::remove_reference;
	/**
	 * @brief Remove noexcept from the function signature; when present, otherwise do nothing.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using FnRemoveNoexcept = typename Impl::FnTraits<T>::remove_noexcept;
	/**
	 * @brief Get the first argument of a function.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using GetFirstArg = TLFront<GetFnArgs<T>>;
	/**
	 * @brief Get the last argument of a function.
	 *
	 * @ingroup Aliases
	 * @tparam T The type of the function.
	 */
	template<typename T>
	using GetLastArg = TLBack<GetFnArgs<T>>;
	/**
	 * @brief Get the number of arguments in a type.
	 * 
	 * @ingroup Variables
	 * @tparam T The type to get the number of arguments from.
	 */
	template <typename T>
	inline constexpr auto get_fn_arg_count_v = Impl::FnTraits<T>::arguments_count;
	/**
	 * @brief Check if a callable returns void.
	 * 
	 * @ingroup Variables
	 * @tparam T The callable type.
	 */
	template <typename T>
	inline constexpr auto get_fn_returns_void_v = Impl::FnTraits<T>::returns_void;
	/**
	 * @brief Get whether or not this has variable arguments.
	 * 
	 * @ingroup Variables
	 * @tparam T The callable type.
	 */
	template <typename T>
	inline constexpr auto get_fn_has_variable_arguments_v = Impl::FnTraits<T>::has_variable_arguments;
	/**
	 * @brief Get the number of arguments in a type.
	 * 
	 * @ingroup Variables
	 * @tparam T The callable type.
	 */
	template <typename T>
	inline constexpr auto get_fn_arguments_count_v = Impl::FnTraits<T>::arguments_count;
}