#pragma once
#include "Hz/Traits/ValueType.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename Current, std::size_t D, std::size_t N = D>
		struct GetDimensionType
		{
			using type = typename GetDimensionType<ValueType<Current>, D, N - 1>::type;
		};
		template <typename Current, std::size_t D>
		struct GetDimensionType<Current, D, 0>
		{
			using type = Current;
		};
	}
	/**
	 * @brief Get the type at some "Dimension".
	 *
	 * For example, when given the type `T = std::array<std::array<int, 3>, 2>`:
	 * @li `GetDimensionType<T, 0>` dimension would be `std::array<std::array<int, 3>, 2>`
	 * @li `GetDimensionType<T, 1>` dimension would be `std::array<int, 2>`
	 * @li `GetDimensionType<T, 2>` dimension would be `int`
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type to get the dimension from.
	 * @tparam D The dimension index.
	 */
	template <typename T, std::size_t D>
	using GetDimensionType = typename Impl::GetDimensionType<T, D>::type;
}