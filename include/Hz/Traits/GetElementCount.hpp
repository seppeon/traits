#pragma once
#include "Hz/Traits/ConceptsIterator.hpp"
#include "Hz/Traits/ValueType.hpp"
#include "Hz/Traits/ArraySizeTraits.hpp"

namespace Hz
{
	namespace Impl
	{
		template<typename T>
		struct GetElementCount
		{
			static constexpr size_t value = 1;
		};
		template<typename T>
			requires ( (Indexable<T> or ForIterable<T>) and requires { array_size_traits_v<T>; } )
		struct GetElementCount<T>
		{
			static constexpr size_t value = ArraySizeTraits<T>::value * GetElementCount<ValueType<T>>::value;
		};
	}
	/**
	 * @brief Get the number of elements in a type.
	 *
	 * This is simply a multiplication of each dimensions size. 
	 *
	 * @ingroup Variables
	 * @tparam T The type to get the number of elements from.
	 */
	template<typename T>
	inline constexpr size_t get_element_count_v = Impl::GetElementCount<T>::value;
}