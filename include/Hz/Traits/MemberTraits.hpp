#pragma once
#include <type_traits>

namespace Hz
{
	namespace Impl
	{
		template<typename M, typename = void>
		struct MemberTraits;
		template<typename M, typename V>
			requires requires{ { MemberTraits<std::remove_cvref_t<M>>{} }; }
		struct MemberTraits<M, V>  : MemberTraits<std::remove_cvref_t<M>> {};
		template<class T, class R>
		struct MemberTraits<T R::*>
		{
			using value_t = T;
			using object_t = R;
			static constexpr auto is_const = false;
			static constexpr auto is_volatile = false;
		};
		template<class T, class R>
		struct MemberTraits<T const R::*>
		{
			using value_t = T const;
			using object_t = R;
			static constexpr auto is_const = true;
			static constexpr auto is_volatile = false;
		};
		template<class T, class R>
		struct MemberTraits<T volatile R::*>
		{
			using value_t = T volatile;
			using object_t = R;
			static constexpr auto is_const = false;
			static constexpr auto is_volatile = true;

		};
		template<class T, class R>
		struct MemberTraits<T const volatile R::*>
		{
			using value_t = T const volatile;
			using object_t = R;
			static constexpr auto is_const = true;
			static constexpr auto is_volatile = true;
		};
	}
	/**
	 * @brief Get the type of the given data member from its data member pointer.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam M The data member pointer.
	 */
	template <typename M>
	using GetMemberType = typename Impl::MemberTraits<M>::value_t;
	/**
	 * @brief Get the type of the object associated with a data member pointer.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam M The data member pointer.
	 */
	template <typename M>
	using GetMemberObjectType = typename Impl::MemberTraits<M>::object_t;
	/**
	 * @brief Get whether or not some data member pointer is to a `const` data member.
	 * 
	 * @ingroup Variables
	 * @tparam M The data member pointer.
	 */
	template <typename M>
	inline constexpr bool get_member_is_const_v = Impl::MemberTraits<M>::is_const;
	/**
	 * @brief Get whether or not some data member pointer is to a `volatile` data member.
	 * 
	 * @ingroup Variables
	 * @tparam M The data member pointer.
	 */
	template <typename M>
	inline constexpr bool get_member_is_volatile_v = Impl::MemberTraits<M>::is_volatile;
	/**
	 * @brief Check if some type is a data member pointer.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept DataMemberPointer = requires
	{
		{ Impl::MemberTraits<std::remove_cvref_t<T>>{} };
	};
}