#pragma once

namespace Hz
{
	/**
	 * @brief Just an identity alias.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type to return.
	 */
	template <typename T>
	using Mirror = T;
	/**
	 * @brief A type with a type template argument.
	 *
	 * Used like this:
	 * @code
	 * template <typename ... Args>
     * void foo(Mirror2<Args, int>...args);
	 * @endcode
	 * To produce the same number of arguments there are `Args`, but of type `int`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam A The type that is folded.
	 * @tparam T The type that will be stamped.
	 */
	template <typename A, typename T>
	using Mirror2 = T;
	/**
	 * @brief A type with a value template argument.
	 *
	 * Used like this:
	 * @code
	 * template <auto ... Args>
     * void foo(Mirror2<Args, int>...args);
	 * @endcode
	 * To produce the same number of arguments there are `Args`, but of type `int`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam I The value that is folded.
	 * @tparam T The type that will be stamped.
	 */
	template <auto V, typename T>
	using Mirror2V = T;
}