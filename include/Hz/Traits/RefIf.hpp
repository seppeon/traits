#pragma once

namespace Hz
{
	namespace Impl
	{
		template <bool, typename>
		struct RefIf;
		template <typename T>
		struct RefIf<true, T> { using type = T &; };
		template <typename T>
		struct RefIf<false, T> { using type = T; };
	}
	/**
	 * @brief Apply a reference if `ref` is true.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam ref Whether or not to apply a reference.
	 * @tparam T The type that may have a reference applied.
	 */
	template <bool ref, typename T>
	using RefIf = typename Impl::RefIf<ref, T>::type;
}