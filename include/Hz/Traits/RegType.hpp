#pragma once
#include <iterator>
#include <type_traits>
#include <variant>

namespace Hz
{
	/**
	 * @brief A customization point, allowing users of the library to register a type as a string.
	 * 
	 * @tparam T The type of the specialized string.
	 */
	template <typename T>
	struct RegString : std::false_type {};
	/**
	 * @brief A customization point, allowing users of the library to register a type as having `Hz::Value` support.
	 * 
	 * @tparam T The type of the specialized Value.
	 */
	template <typename T>
	struct RegValue : std::false_type {};
	/**
	 * @brief A customization point, allowing users of the library to register a type as having `Hz::HasValue` support.
	 * 
	 * @tparam T The type of the specialized HasValue.
	 */
	template <typename T>
	struct RegHasValue : std::false_type {};
	/**
	 * @brief A customization point, allowing users of the library to register a type as having nullable support.
	 * 
	 * @tparam T The type of the specialized nullable.
	 */
	template <typename T>
    struct RegNullable : std::false_type {};
	/**
	 * @brief A customization point, allowing users of the library to register a type as being a variant.
	 * 
	 * @tparam T The type of the specialized variant.
	 */
	template <typename T>
	struct RegVariant : std::false_type {};
	/**
	 * @brief A specialization of `Hz::RegString` declaring c-string support.
	 * 
	 * @tparam T The type of the specialized string.
	 */
	template <typename T>
		requires (std::is_array_v<T> and std::is_same_v<char, std::remove_cvref_t<std::iter_value_t<T>>>)
	struct RegString<T> : std::true_type {};
	/**
	 * @brief Check if a type has been registered as a string type.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasRegString = RegString<std::remove_cvref_t<T>>::value;
	/**
	 * @brief Check if a type is registered as a variant.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasRegVariant = RegVariant<std::remove_cvref_t<T>>::value;
	/**
	 * @brief Check if a type has been registered as an object with a value function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template <typename T>
	concept HasRegValue = RegValue<std::remove_cvref_t<T>>::value;
	/**
	 * @brief Check if a type has been registered as an object with a has_value function.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template <typename T>
	concept HasRegHasValue = RegHasValue<std::remove_cvref_t<T>>::value;
	/**
	 * @brief Check if a type has been registered as nullable.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check.
	 */
	template<typename T>
	concept HasRegNullable = RegNullable<std::remove_cvref_t<T>>::value;
}

namespace Hz
{
	/**
	 * @brief A specialization of `Hz::RegVariant` declaring `std::variant` support.
	 * 
	 * @tparam T The type of the specialized `std::variant`.
	 */
	template<class... Types>
	struct RegVariant<::std::variant<Types...>> : std::true_type {};
}