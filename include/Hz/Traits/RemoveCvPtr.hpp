#pragma once
#include <type_traits>

namespace Hz
{
	/**
	 * @brief Remove a single pointer and const/volatile qualifications.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type to remove the pointer and qualifiers.
	 */
	template <typename T>
	using RemoveCvPtr = std::remove_cv_t<std::remove_pointer_t<T>>;

	namespace Impl
	{
		template <typename T>
		struct RemoveAllCvPtr
		{
			using type = std::remove_cv_t<T>;
		};
		template <typename T>
		struct RemoveAllCvPtr<T*>
		{
			using type = typename RemoveAllCvPtr<std::remove_cv_t<T>>::type;
		};
	}
	/**
	 * @brief Remove all const/volatile pointers from a given type.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type to strip pointers from.
	 */
	template <typename T>
	using RemoveAllCvPtr = typename Impl::RemoveAllCvPtr<std::remove_cv_t<T>>::type;
}