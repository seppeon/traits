#pragma once
#include <type_traits>

namespace Hz
{
	/**
	 * @brief Remove one layer of const/volatile for referneces/pointers from `T`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type to remove qualfiers and indirections.
	 */
	template <typename T>
	using RemoveCvRefPtr = std::remove_cvref_t<std::remove_pointer_t<std::remove_cvref_t<T>>>;

	namespace Impl
	{
		template <typename T>
		struct RemoveAllCvRefPtr
		{
			using type = std::remove_cv_t<T>;
		};
		template <typename T>
		struct RemoveAllCvRefPtr<T*>
		{
			using type = typename RemoveAllCvRefPtr<RemoveCvRefPtr<T>>::type;
		};
		template <typename T>
		struct RemoveAllCvRefPtr<T&>
		{
			using type = typename RemoveAllCvRefPtr<RemoveCvRefPtr<T>>::type;
		};
	}
	/**
	 * @brief Remove all qualifiers and indirections (const/volatile) from a type.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type to strip indirections and qualifiers.
	 */
	template <typename T>
	using RemoveAllCvRefPtr = typename Impl::RemoveAllCvRefPtr<RemoveCvRefPtr<T>>::type;
}