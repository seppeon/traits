#pragma once

namespace Hz
{
	namespace Impl
	{
		template<typename T>
		struct RemoveLValueRef
		{
			using type = T;
		};
		template<typename T>
		struct RemoveLValueRef<T&>
		{
			using type = T;
		};
	}
	/**
	 * @brief Remove lvalue references from a type (if it is lvalue qualified).
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type to remove lvalue references from.
	 */
	template <typename T>
	using RemoveLValueRef = typename Impl::RemoveLValueRef<T>::type;
}