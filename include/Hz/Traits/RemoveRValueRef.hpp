#pragma once

namespace Hz
{
	namespace Impl
	{
		template<typename T>
		struct RemoveRValueRef
		{
			using type = T;
		};
		template<typename T>
		struct RemoveRValueRef<T&&>
		{
			using type = T;
		};
	}
	/**
	 * @brief Remove rvalue references from a type (if it is rvalue qualified).
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type to remove rvalue references from.
	 */
	template <typename T>
	using RemoveRValueRef = typename Impl::RemoveRValueRef<T>::type;
}