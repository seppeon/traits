#pragma once
#include <concepts>

namespace Hz
{
    namespace Impl
    {
        template <typename T, typename Match, typename Replacement>
        struct ReplaceOnMatch
        {
            using type = T;
        };
        template <typename T, typename Replacement>
        struct ReplaceOnMatch<T, T, Replacement>
        {
            using type = Replacement;
        };
    }
	/**
	 * @brief Replace `T` with `Replacement` if `T` is `Match`, otherwise return `T`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The test type.
	 * @tparam Match The match type.
	 * @tparam Replacement The replacement.
	 */
    template <typename T, typename Match, typename Replacement>
    using ReplaceOnMatch = typename Impl::ReplaceOnMatch<T, Match, Replacement>::type;

    namespace Impl
    {
        template <typename T, typename Match, typename Replacement>
        struct DeepReplaceOnMatch
        {
            using type = ::Hz::ReplaceOnMatch<T, Match, Replacement>;
        };
        template <typename T, typename Match, typename Replacement>
        struct DeepReplaceOnMatch<T const, Match, Replacement>
        {
            using type = typename DeepReplaceOnMatch<T, Match, Replacement>::type const;
        };
        template <typename T, typename Match, typename Replacement>
        struct DeepReplaceOnMatch<T volatile, Match, Replacement>
        {
            using type = typename DeepReplaceOnMatch<T, Match, Replacement>::type volatile;
        };
        template <typename T, typename Match, typename Replacement>
        struct DeepReplaceOnMatch<T const volatile, Match, Replacement>
        {
            using type = typename DeepReplaceOnMatch<T, Match, Replacement>::type const volatile;
        };
        template <typename T, typename Match, typename Replacement>
            requires (not std::same_as<T&, Match>)
        struct DeepReplaceOnMatch<T &, Match, Replacement>
        {
            using type = typename DeepReplaceOnMatch<T, Match, Replacement>::type &;
        };
        template <typename T, typename Match, typename Replacement>
            requires (not std::same_as<T&&, Match>)
        struct DeepReplaceOnMatch<T &&, Match, Replacement>
        {
            using type = typename DeepReplaceOnMatch<T, Match, Replacement>::type &&;
        };
        template <typename T, typename Match, typename Replacement>
            requires (not std::same_as<T*, Match>)
        struct DeepReplaceOnMatch<T *, Match, Replacement>
        {
            using type = typename DeepReplaceOnMatch<T, Match, Replacement>::type *;
        };
        template <typename T, typename Match, typename Replacement>
            requires (not std::same_as<T * const, Match>)
        struct DeepReplaceOnMatch<T * const, Match, Replacement>
        {
            using type = typename DeepReplaceOnMatch<T, Match, Replacement>::type * const;
        };
        template <typename T, typename Match, typename Replacement>
            requires (not std::same_as<T * volatile, Match>)
        struct DeepReplaceOnMatch<T * volatile, Match, Replacement>
        {
            using type = typename DeepReplaceOnMatch<T, Match, Replacement>::type * volatile;
        };
        template <typename T, typename Match, typename Replacement>
            requires (not std::same_as<T * const volatile, Match>)
        struct DeepReplaceOnMatch<T * const volatile, Match, Replacement>
        {
            using type = typename DeepReplaceOnMatch<T, Match, Replacement>::type * const volatile;
        };

    }
	/**
	 * @brief Replace `T` with `Replacement` if `T` is `Match`, otherwise return `T`; this includes those behind pointers of references.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The test type.
	 * @tparam Match The match type.
	 * @tparam Replacement The replacement.
	 */
    template <typename T, typename Match, typename Replacement>
    using DeepReplaceOnMatch = typename Impl::DeepReplaceOnMatch<T, Match, Replacement>::type;
}