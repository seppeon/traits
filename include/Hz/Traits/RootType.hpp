#pragma once
#include "Hz/Traits/ValueType.hpp"
#include "Hz/Traits/ConceptsIterator.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename T>
		struct RootType
		{
			using type = T;
		};
		template <SupportsValueType T>
		struct RootType<T>
		{
			using type = typename RootType<ValueType<T>>::type;
		};
	}
	/**
	 * @brief Get the "root type", which is the first type that does not have a `Hz::ValueType`.
	 *
	 * This can be used to get the underlying type of nested containers `int[3][4]`'s root type is `int`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type to get the root type.
	 */
	template <typename T>
	using RootType = typename Impl::RootType<T>::type;
}