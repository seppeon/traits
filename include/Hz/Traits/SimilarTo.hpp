#pragma once
#include <type_traits>
#include <concepts>

namespace Hz
{
	namespace Impl
	{
		struct ThisHasNoConversions{};
		template <typename T>
		struct R { using type = T; };
		template <typename T>
		struct R<T &> { using type = ThisHasNoConversions &; };
		template <typename T>
		struct R<T &&> { using type = ThisHasNoConversions &&; };
		template <typename T>
		struct R<T const &> { using type = ThisHasNoConversions const &; };
		template <typename T>
		struct R<T const &&> { using type = ThisHasNoConversions const &&; };
		template <typename T>
		struct R<T volatile &> { using type = ThisHasNoConversions volatile &; };
		template <typename T>
		struct R<T volatile &&> { using type = ThisHasNoConversions volatile &&; };
		template <typename T>
		struct R<T const volatile &> { using type = ThisHasNoConversions const volatile &; };
		template <typename T>
		struct R<T const volatile &&> { using type = ThisHasNoConversions const volatile &&; };
	}
	/**
	 * @brief Check if a type `T` has a convertible reference `V`
	 *
	 * @note Unlike `std::convertible_to`, this does not invoke user implicit conversions. 
	 *
	 * @ingroup Concepts
	 * @tparam T The type to convert from.
	 * @tparam V The type to convert to.
	 */
	template <typename T, typename V>
	concept SimilarTo = (
		std::same_as<std::remove_cvref_t<std::remove_pointer_t<T>>, std::remove_cvref_t<std::remove_pointer_t<V>>> and
		requires(typename Impl::R<T>::type test_type, typename Impl::R<V>::type default_type) { { static_cast<typename Impl::R<V>::type>(test_type) }; }
	);
}