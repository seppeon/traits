#pragma once
#include "Hz/Traits/ValueType.hpp"

namespace Hz
{
	namespace Impl
	{
		template <typename Rule, typename T>
		struct StopType
		{
			using type = T;
		};
		template <typename Rule, SupportsValueType T> requires (Rule::template Test<T>)
		struct StopType<Rule, T>
		{
			using type = typename Impl::StopType<Rule, Hz::ValueType<T>>::type;
		};
	}
	/**
	 * @brief Get the "stop type", which is the first type which is either a root type, or that fails the `Rule`s test.
	 *
	 * Lets assume we have a `Rule` that only returns true when the given type is not a string.
	 * With this rule, the stop type of:
	 * @code
	 * std::vector<std::string>
	 * @endcode
	 * is `std::string`, this is different to the root type, which would be `char`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam Rule The rule to apply to the search.
	 * @tparam T The type that will be searched for a "stop type".
	 */
	template <typename Rule, typename T>
	using StopType = typename Impl::StopType<Rule, T>::type;
}