#pragma once
/**
 * @file Traits.hpp
 * @author David Ledger (davidledger@live.com.au)
 * @brief A traits library.
 * 
 * @defgroup Aliases Metaprogramming Traits
 * @defgroup Concepts General purpose concepts
 * @defgroup Variables Inline variables
 */
#include "Hz/Traits/ApplyCvref.hpp"
#include "Hz/Traits/CArrayLen.hpp"
#include "Hz/Traits/CloneCvref.hpp"
#include "Hz/Traits/Concepts.hpp"
#include "Hz/Traits/Conditional.hpp"
#include "Hz/Traits/ConstIf.hpp"
#include "Hz/Traits/CountDimensions.hpp"
#include "Hz/Traits/CvIf.hpp"
#include "Hz/Traits/Cvref.hpp"
#include "Hz/Traits/FirstConstructible.hpp"
#include "Hz/Traits/ConceptsFn.hpp"
#include "Hz/Traits/FnTraits.hpp"
#include "Hz/Traits/Fwd.hpp"
#include "Hz/Traits/GetDimensionType.hpp"
#include "Hz/Traits/GetElementCount.hpp"
#include "Hz/Traits/ConceptsIterator.hpp"
#include "Hz/Traits/MemberTraits.hpp"
#include "Hz/Traits/Mirror.hpp"
#include "Hz/Traits/RefIf.hpp"
#include "Hz/Traits/RegType.hpp"
#include "Hz/Traits/RemoveCvPtr.hpp"
#include "Hz/Traits/RemoveCvRefPtr.hpp"
#include "Hz/Traits/RemoveLValueRef.hpp"
#include "Hz/Traits/RemoveRValueRef.hpp"
#include "Hz/Traits/ReplaceOnMatch.hpp"
#include "Hz/Traits/RootType.hpp"
#include "Hz/Traits/SimilarTo.hpp"
#include "Hz/Traits/StopType.hpp"
#include "Hz/Traits/TryTypes.hpp"
#include "Hz/Traits/Value.hpp"
#include "Hz/Traits/ValueType.hpp"
#include "Hz/Traits/VL.hpp"
#include "Hz/Traits/VolatileIf.hpp"