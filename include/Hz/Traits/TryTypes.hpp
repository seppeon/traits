#pragma once

namespace Hz
{
	namespace Impl
	{
		template <typename T, template <typename> class A>
		concept ViableAlt = requires
		{
			{ A<T>{} };
		};
		template <typename T, template <typename> class ... Alts>
		struct TryTypes;
		template <typename T, template <typename> class Alt, template <typename> class ... Alts>
		struct TryTypes<T, Alt, Alts...>
		{
			using type = typename TryTypes<T, Alts...>::type;
		};
		template <typename T, template <typename> class Alt, template <typename> class ... Alts>
			requires ViableAlt<T, Alt>
		struct TryTypes<T, Alt, Alts...>
		{
			using type = typename Alt<T>::type;
		};
	}
	/**
	 * @brief Try to instantiate various `Alt` types by applying the template type `T`, return the first viable type.
	 *
	 * @note This is used to avoid ambigious specializations that occasionally occur with `concepts`.
	 * See @ref Hz::ValueType for an example of the usage.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type to apply to the various alternatives.
	 * @tparam Alts... The alternatives which will each have `T` applied.
	 */
	template <typename T, template <typename> class... Alts>
	using TryTypes = typename Impl::TryTypes<T, Alts...>::type;
}