#pragma once
#include "Hz/Traits/Value.hpp"
#include <Hz/TL/TLCommon.hpp>
#include <array>

namespace Hz
{
	/**
	 * @brief A list of non-type template parameters (NTTP).
	 * 
	 * @tparam v... The NTTPs.
	 */
	template<auto ... v>
	struct VL {};
	
	namespace Impl
	{
		template <typename...Args>
		auto ToVLImpl(TL<Args...>)->VL<Args::value...>;
		template <auto...Args>
		auto ToTLImpl(VL<Args...>)->TL<ValueTag<Args>...>;
	}
	/**
	 * @brief Convert a type list (of `Value<V>`s) to a value list.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type list type.
	 */
	template<typename T>
	using ToVL = decltype(Impl::ToVLImpl(T{}));
	/**
	 * @brief Convert a value list to a type list (of `Value<V>`s).
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The value list type.
	 */
	template<typename T>
	using ToTL = decltype(Impl::ToTLImpl(T{}));

	namespace Impl
	{
		using sz = decltype(sizeof(int));
	}
	/**
	 * @brief Create an index list.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam Sz... The indexes.
	 */
	template <Impl::sz ... Sz>
	using IL = VL<Sz...>;

	namespace Impl
	{
		template <typename L1>
		struct MakeIlImpl;

		template <sz ... Ls>
		struct MakeIlImpl<std::index_sequence<Ls...>>
		{
			using type = IL<Ls...>;
		};

		template <auto Result, typename Sequence>
		struct ILFilterImpl;
		
		template <auto Result, Impl::sz ... Is>
		struct ILFilterImpl<Result, IL<Is...>>
		{
			using type = IL<Result[Is]...>;
		};
	}
	/**
	 * @brief Create an index sequence.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam L The number of indexes in the sequence (same as std::make_index_sequence).
	 */
	template <Impl::sz L>
	using ILMake = typename Impl::MakeIlImpl<std::make_index_sequence<L>>::type;

	namespace Impl
	{
		template <typename Filter, Impl::sz ... Is>
		static constexpr auto IlFilterResultLength(Hz::IL<Is...>) ->Impl::sz
		{
			Impl::sz count = 0; 
			((Filter::Check(Hz::IL<Is>{}) and ++count), ...);
			return count;
		}
	}
	/**
	 * @brief Get the number of indexes that pass the filter in the given index length.
	 * 
	 * @ingroup Variables
	 * @tparam List The list to extract the indexes.
	 * @tparam Filter The filter that will be applied.
	 */
	template <typename List, typename Filter>
	inline constexpr Impl::sz il_filter_result_length = Impl::IlFilterResultLength<Filter>(List{});

	namespace Impl
	{
		template <typename Filter, Impl::sz ... Is>
		static constexpr auto IlFilterResultArray(IL<Is...>) -> std::array<Impl::sz, il_filter_result_length<IL<Is...>, Filter>>
		{
			auto output = std::array<Impl::sz, il_filter_result_length<IL<Is...>, Filter>>{};
			Impl::sz index = 0; 
			((Filter::Check(IL<Is>{}) and (output[index++] = Is, true)), ...);
			return output;
		}
	}
	/**
	 * @brief Filter the given indexes with the provided filter, removing those that fail the filter.
	 * 
	 * @ingroup Variables
	 * @tparam List The list type.
	 * @tparam Filter The filter type.
	 */
	template <typename List, typename Filter>
	inline constexpr auto il_filter_result_array = Impl::IlFilterResultArray<Filter>(List{});
	/**
	 * @brief Filter a given index list with the provided filter.
	 *
	 * @note The filter type must have a static constexpr data member `Filter::Check`,
	 * which takes a single item index list and returns true when the item should be kept, and false when it should be removed.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam List The index list.
	 * @tparam Filter The filter that can be applied to the index list.
	 */
	template <typename List, typename Filter>
	using ILFilter = typename Impl::ILFilterImpl<il_filter_result_array<List, Filter>, ILMake<il_filter_result_length<List, Filter>>>::type;
}