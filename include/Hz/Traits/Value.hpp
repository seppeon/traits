#pragma once

namespace Hz
{
	/**
	 * @brief A type to contain a non-type template parameter `v` .
	 * 
	 * @tparam v The non-type template parameter.
	 */
	template<auto v>
	struct ValueTag
	{
		using value_type = decltype( v );
		static constexpr value_type value = v;
	};
	/**
	 * @brief A type to contain a template std::size_t argument.
	 * 
	 * @tparam v The non-type template parameter.
	 */
	template<decltype(sizeof(char)) v>
	struct SizeTag
	{
		using value_type = decltype(sizeof(char));
		static constexpr value_type value = v;
	};
}