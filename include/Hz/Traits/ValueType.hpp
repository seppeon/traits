#pragma once
#include "Hz/Traits/ConceptsStdLib.hpp"
#include "Hz/Traits/MemberTraits.hpp"
#include "Hz/Traits/ConceptsIterator.hpp"
#include "Hz/Traits/CloneCvref.hpp"
#include "Hz/Traits/TryTypes.hpp"
#include "Hz/Traits/ApplyCvref.hpp"
#include <array>
#include <type_traits>

namespace Hz
{
	namespace Impl
	{
		using std::begin;
		using std::end;

		template <typename T>
		struct ArrayValue;
		template <typename T, std::size_t N>
		struct ArrayValue<T[N]>
		{
			using type = T;
		};
		template <typename T, std::size_t N>
		struct ArrayValue<T (* const volatile)[N]>
		{
			using type = T &;
		};
		template <typename T, std::size_t N>
		struct ArrayValue<T (* volatile)[N]>
		{
			using type = T &;
		};
		template <typename T, std::size_t N>
		struct ArrayValue<T (* const)[N]>
		{
			using type = T &;
		};
		template <typename T, std::size_t N>
		struct ArrayValue<T (*)[N]>
		{
			using type = T &;
		};
		template <typename T, std::size_t N>
		struct ArrayValue<T (&)[N]>
		{
			using type = T &;
		};
		template <Hz::DataMemberPointer T>
		struct DataMemberPointerType
		{
			using type = Hz::ApplyCvref<Hz::GetMemberObjectType<T>, Hz::GetMemberType<T>> &;
		};
		template <Hz::Std::HasValueType T>
		struct ValueTypeMember
		{
			using type = Hz::ApplyCvref<std::remove_reference_t<T>, typename std::remove_cvref_t<T>::value_type>;
		};
		template <Hz::Std::HasValueType T>
		struct ValueTypeMember<T&>
		{
			using type = Hz::ApplyCvref<std::remove_reference_t<T>, typename std::remove_cvref_t<T>::value_type> &;
		};

		template <Hz::Indexable T>
		struct IndexableElementType
		{
			using type = decltype((std::declval<T&&>()[std::declval<size_t>()]));
		};		
		template <Hz::ForIterable T>
		struct ForElementType
		{
			using type = decltype((*begin(std::declval<T&>())));
		};
		template <Hz::Std::HasElementType T>
		struct ElementTypeMember
		{
			using type = typename std::remove_cvref_t<T>::element_type &;
		};
	}
	/**
	 * @brief Get the "value type" of some given type `T`, or prevent compilation if there is no viable value type.
	 *
	 * This will check the following possible options for a value type, in this order returning the first one:
	 * @li `typename T::value_type`.
	 * @li `T[N]`, returning T.
	 * @li `T Obj::*`, returning `T`.
	 * @li `typename T::element_type`.
	 * @li `decltype(object[index])`.
	 * @li `decltype(*begin(object))`.
	 *
	 * This is created to unify various standard library types, and c-style types into a single "value type" alias.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam T The type to get the value type from.
	 */
	template <typename T>
	using ValueType = TryTypes<
		T,
		Impl::ValueTypeMember,
		Impl::ArrayValue,
		Impl::DataMemberPointerType,
		Impl::ElementTypeMember,	// Defined in: ElementType.hpp
		Impl::IndexableElementType,	// Defined in: ElementType.hpp
		Impl::ForElementType		// Defined in: ElementType.hpp
	>;
	/**
	 * @brief Check whether there is a value type for a given T.
	 * 
	 * @ingroup Concepts
	 * @tparam T The type to check for a value type.
	 */
	template <typename T>
	concept SupportsValueType = 
	(
		requires { typename Impl::ArrayValue<T>::type; } or
		requires { typename Impl::ValueTypeMember<T>::type; } or
		requires { typename Impl::DataMemberPointerType<T>::type; } or
		requires { typename Impl::ElementTypeMember<T>::type; } or
		requires { typename Impl::IndexableElementType<T>::type; } or
		requires { typename Impl::ForElementType<T>::type; }
	);
}