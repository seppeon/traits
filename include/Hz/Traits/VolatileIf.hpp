#pragma once
#include <type_traits>

namespace Hz
{
	namespace Impl
	{
		template <bool, typename>
		struct VolatileIf;
		template <typename T>
		struct VolatileIf<true, T>
		{
			using type = T volatile;
		};
		template <typename T>
		struct VolatileIf<false, T>
		{
			using type = T;
		};
	}
	/**
	 * @brief Apply volatile to `T` if `IsVolatile` is true.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam IsVolatile Whether or not to apply volatile.
	 * @tparam T The type to apply volatile.
	 */
	template <bool IsVolatile, typename T>
	using VolatileIf = typename Impl::VolatileIf<IsVolatile, T>::type;
	/**
	 * @brief Take the volatile qualifier from `Match` and apply it to `T`.
	 *
	 * @ingroup Aliases
	 * 
	 * @tparam Match The type to extract the volatile qualifier.
	 * @tparam T The type to apply the volatile qualifier.
	 */
	template <typename Match, typename T>
	using MatchVolatileOf = VolatileIf<std::is_volatile_v<Match>, T>;
}