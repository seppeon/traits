#include "Hz/Traits/ApplyCvref.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("ApplyCvref", "[Hz::ApplyCvref]")
{
	STATIC_REQUIRE(std::same_as<Hz::ApplyCvref<int,         int      >, int>);
	STATIC_REQUIRE(std::same_as<Hz::ApplyCvref<int const,   int      >, int const>);
	STATIC_REQUIRE(std::same_as<Hz::ApplyCvref<int,         int const>, int const>);
	STATIC_REQUIRE(std::same_as<Hz::ApplyCvref<int &,       int      >, int &>);
	STATIC_REQUIRE(std::same_as<Hz::ApplyCvref<int const &, int      >, int const &>);
	STATIC_REQUIRE(std::same_as<Hz::ApplyCvref<int &,       int const>, int const &>);
	STATIC_REQUIRE(std::same_as<Hz::ApplyCvref<int,         int       &>, int &>);
	STATIC_REQUIRE(std::same_as<Hz::ApplyCvref<int const,   int       &>, int const &>); // This is where this differs from std::common_type_t
	STATIC_REQUIRE(std::same_as<Hz::ApplyCvref<int,         int const &>, int const &>);
	STATIC_REQUIRE(std::same_as<Hz::ApplyCvref<int &,       int       &>, int &>);
	STATIC_REQUIRE(std::same_as<Hz::ApplyCvref<int const &, int       &>, int const &>);
	STATIC_REQUIRE(std::same_as<Hz::ApplyCvref<int &,       int const &>, int const &>);
}