#include "Hz/Traits/ArraySizeTraits.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("ArraySizeTraits", "[Hz::ArraySizeTraits]")
{
	STATIC_REQUIRE(Hz::array_size_traits_v<int[4]> == 4);
	STATIC_REQUIRE(Hz::array_size_traits_v<std::array<int, 4>> == 4);
	STATIC_REQUIRE(Hz::array_size_traits_v<std::span<int, 4>> == 4);
}