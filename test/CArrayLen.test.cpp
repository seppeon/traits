#include "Hz/Traits/CArrayLen.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("CArrayLen can get std::array and c-array lengths", "[Hz::CArrayLen]")
{
	STATIC_REQUIRE(Hz::get_array_length_v<int[4]> == 4);
	STATIC_REQUIRE(Hz::get_array_length_v<int(*)[4]> == 4);
	STATIC_REQUIRE(Hz::get_array_length_v<int(&)[4]> == 4);
	STATIC_REQUIRE(Hz::get_array_length_v<int const [4]> == 4);
	STATIC_REQUIRE(Hz::get_array_length_v<int const (*)[4]> == 4);
	STATIC_REQUIRE(Hz::get_array_length_v<int const (&)[4]> == 4);
	STATIC_REQUIRE(Hz::get_array_length_v<std::array<int, 4>> == 4);
	STATIC_REQUIRE(Hz::get_array_length_v<std::array<int, 4> &> == 4);
	STATIC_REQUIRE(Hz::get_array_length_v<std::array<int, 4> const &> == 4);
	STATIC_REQUIRE(Hz::get_array_length_v<int volatile [4]> == 4);
	STATIC_REQUIRE(Hz::get_array_length_v<int volatile (*)[4]> == 4);
	STATIC_REQUIRE(Hz::get_array_length_v<int volatile (&)[4]> == 4);
	STATIC_REQUIRE(Hz::get_array_length_v<std::array<int, 4> volatile &> == 4);
	STATIC_REQUIRE(Hz::get_array_length_v<int const volatile [4]> == 4);
	STATIC_REQUIRE(Hz::get_array_length_v<int const volatile (*)[4]> == 4);
	STATIC_REQUIRE(Hz::get_array_length_v<int const volatile (&)[4]> == 4);
	STATIC_REQUIRE(Hz::get_array_length_v<std::array<int, 4> const volatile &> == 4);
}