cmake_minimum_required(VERSION 3.22)
project(Hz_Traits_Test LANGUAGES CXX)

find_package(Catch2 REQUIRED)
file(GLOB_RECURSE SRC_FILES CONFIGURE_DEPENDS *.test.cpp)

enable_testing()
add_executable(Hz_Traits_Test)
add_executable(Hz::Traits::Test ALIAS Hz_Traits_Test)
target_sources(Hz_Traits_Test PRIVATE ${SRC_FILES})
target_compile_features(Hz_Traits_Test PRIVATE cxx_std_23)
target_link_libraries(Hz_Traits_Test PRIVATE Hz::Traits Catch2::Catch2WithMain)
add_test(NAME Hz_Traits_Test COMMAND Hz_Traits_Test)