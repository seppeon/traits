#include "Hz/Traits/CloneCvref.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("Clone CV moves the CV from one type to another")
{
	using T = char;

	REQUIRE(std::is_same_v<Hz::CloneCvref<T&, int>, int&>);
	REQUIRE(std::is_same_v<Hz::CloneCvref<T, int>, int>);
	REQUIRE(std::is_same_v<Hz::CloneCvref<T&&, int>, int&&>);
	REQUIRE(std::is_same_v<Hz::CloneCvref<T&, int&>, int&>);
	REQUIRE(std::is_same_v<Hz::CloneCvref<T const &&, int>, int const &&>);
	REQUIRE(std::is_same_v<Hz::CloneCvref<T const &, int>, int const &>);
	REQUIRE(std::is_same_v<Hz::CloneCvref<T volatile &, int>, int volatile &>);
	REQUIRE(std::is_same_v<Hz::CloneCvref<T volatile, int>, int volatile >);
	REQUIRE(std::is_same_v<Hz::CloneCvref<T volatile &&, int>, int volatile &&>);
	REQUIRE(std::is_same_v<Hz::CloneCvref<T volatile &, int&>, int volatile &>);
	REQUIRE(std::is_same_v<Hz::CloneCvref<T volatile const &&, int>, int volatile  const &&>);
	REQUIRE(std::is_same_v<Hz::CloneCvref<T volatile const &, int>, int volatile  const &>);
}