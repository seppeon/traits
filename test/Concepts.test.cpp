#include "Hz/Traits/Concepts.hpp"
#include "Hz/Traits/RemoveRValueRef.hpp"
#include <catch2/catch_all.hpp>

namespace
{
	using Hz::Property;

	template <typename T>
	struct ValuePropertySetTypeImpl
	{
		using type = std::add_lvalue_reference_t<std::add_const_t<std::remove_reference_t<T>>>;
	};
	template <typename T>
	using ValuePropertySetType = typename ValuePropertySetTypeImpl<T>::type;

	template <typename T>
	struct ValuePropertyGetTypeImpl { using type = T; };

	template <Hz::Optional T>
	struct ValuePropertyGetTypeImpl<T>
	{
		using type = decltype(*std::declval<T>());
	};
	template <typename T>
	using ValuePropertyGetType = typename ValuePropertyGetTypeImpl<T>::type;

	template <typename T>
	struct ValuePropertyTraits
	{
		static constexpr bool settable = not std::is_const_v<std::remove_reference_t<T>>;
		using construct_type = T;
		using value_type = T;
		using set_type = ValuePropertySetType<T>;
		using get_type = ValuePropertyGetType<T>;
	};
	template <Hz::LValue T>
	struct ValuePropertyTraits<T>
	{
		static constexpr bool settable = not std::is_const_v<std::remove_reference_t<T>>;
		using construct_type = T;
		using value_type = T;
		using set_type = ValuePropertySetType<T>;
		using get_type = ValuePropertyGetType<T>;
	};
	template <Hz::RValue T>
	struct ValuePropertyTraits<T>
	{
		static constexpr bool settable = true;
		using construct_type = Hz::RemoveRValueRef<std::remove_const_t<T>>;
		using value_type = Hz::RemoveRValueRef<std::remove_const_t<T>>;
		using set_type = ValuePropertySetType<value_type>;
		using get_type = ValuePropertyGetType<value_type>;
	};

	template <typename V>
	struct ValueProperty
	{
		using traits = ValuePropertyTraits<V>;
		using construct_type = typename traits::construct_type;
		using value_type = typename traits::value_type;
		using set_type = typename traits::set_type;
		using get_type = typename traits::get_type;
	
		static_assert(not std::is_array_v<value_type> or std::is_lvalue_reference_v<value_type>, "C style arrays cannot be ValueProperties unless as a reference.");

		value_type value;

		static constexpr bool Settable() noexcept
		{
			return traits::settable;
		}
		constexpr bool Set(auto && new_value) const noexcept requires( traits::settable )
		{
			Copy(FWD(new_value), value);
			return true;
		}

		static constexpr bool Gettable() noexcept
		{
			return true;
		}
		#pragma clang diagnostic push
		#pragma clang diagnostic ignored "-Wdeprecated-volatile" // This is not an issue with this function, its an issue with the caller
		constexpr get_type Get() const noexcept
		{
			return value;
		}
		#pragma clang diagnostic pop
	};
	template <typename V>
	ValueProperty(V&&)->ValueProperty<V>;
}

TEMPLATE_TEST_CASE(
	"Property traits correctly detect primitives Hz::Fn::ValuePropertys", "[Hz::Traits]", 
	std::uint8_t, std::int8_t,
	std::uint16_t, std::int16_t,
	std::uint32_t, std::int32_t,
	std::uint64_t, std::int64_t,
	float, double, long double,
	bool, char
	)
{
	REQUIRE(Property<ValueProperty<TestType>>);
	REQUIRE(Property<ValueProperty<TestType &>>);
	REQUIRE(Property<ValueProperty<TestType &&>>);
	REQUIRE(Property<ValueProperty<TestType const>>);
	REQUIRE(Property<ValueProperty<TestType const &>>);
	REQUIRE(Property<ValueProperty<TestType const &&>>);
	REQUIRE(Property<ValueProperty<TestType volatile>>);
	REQUIRE(Property<ValueProperty<TestType volatile &>>);
	REQUIRE(Property<ValueProperty<TestType volatile &&>>);
	REQUIRE(Property<ValueProperty<TestType volatile const>>);
	REQUIRE(Property<ValueProperty<TestType volatile const &>>);
	REQUIRE(Property<ValueProperty<TestType volatile const &&>>);
}

TEMPLATE_TEST_CASE(
	"Property traits correctly detect primitive array Hz::Fn::ValuePropertys", "[Hz::Traits]", 
	std::uint8_t, std::int8_t,
	std::uint16_t, std::int16_t,
	std::uint32_t, std::int32_t,
	std::uint64_t, std::int64_t,
	float, double, long double,
	bool, char
	)
{
	using type = TestType[3];

	// Note: Array of non-lvalue-reference type is not allowed by value property.
	STATIC_REQUIRE(Property<ValueProperty<type &>>);
	STATIC_REQUIRE(Property<ValueProperty<type const &>>);
	STATIC_REQUIRE(Property<ValueProperty<type volatile &>>);
	STATIC_REQUIRE(Property<ValueProperty<type volatile const &>>);
}