#include "Hz/Traits/ConceptsStdLib.hpp"
#include <catch2/catch_all.hpp>

namespace
{
	struct Foo
	{
	};
	struct FooValueType
	{
		using value_type = int;
	};
	struct FooReference
	{
		using reference = int &;
	};
	struct FooConstReference
	{
		using const_reference = int const &;
	};
	struct FooElementType
	{
		using element_type = int;
	};
}

TEST_CASE("ObjectTraits", "[Hz::ObjectTraits]")
{
	STATIC_REQUIRE_FALSE(Hz::Std::HasValueType<Foo>);
	STATIC_REQUIRE(Hz::Std::HasValueType<FooValueType>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasRefType<Foo>);
	STATIC_REQUIRE(Hz::Std::HasRefType<FooReference>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasConstRefType<Foo>);
	STATIC_REQUIRE(Hz::Std::HasConstRefType<FooConstReference>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasElementType<Foo>);
	STATIC_REQUIRE(Hz::Std::HasElementType<FooElementType>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasValueType<Foo &>);
	STATIC_REQUIRE(Hz::Std::HasValueType<FooValueType &>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasRefType<Foo &>);
	STATIC_REQUIRE(Hz::Std::HasRefType<FooReference &>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasConstRefType<Foo &>);
	STATIC_REQUIRE(Hz::Std::HasConstRefType<FooConstReference &>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasElementType<Foo &>);
	STATIC_REQUIRE(Hz::Std::HasElementType<FooElementType &>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasValueType<Foo const &>);
	STATIC_REQUIRE(Hz::Std::HasValueType<FooValueType const &>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasRefType<Foo const &>);
	STATIC_REQUIRE(Hz::Std::HasRefType<FooReference const &>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasConstRefType<Foo const &>);
	STATIC_REQUIRE(Hz::Std::HasConstRefType<FooConstReference const &>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasElementType<Foo const &>);
	STATIC_REQUIRE(Hz::Std::HasElementType<FooElementType const &>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasValueType<Foo volatile &>);
	STATIC_REQUIRE(Hz::Std::HasValueType<FooValueType volatile &>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasRefType<Foo volatile &>);
	STATIC_REQUIRE(Hz::Std::HasRefType<FooReference volatile &>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasConstRefType<Foo volatile &>);
	STATIC_REQUIRE(Hz::Std::HasConstRefType<FooConstReference volatile &>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasElementType<Foo volatile &>);
	STATIC_REQUIRE(Hz::Std::HasElementType<FooElementType volatile &>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasValueType<Foo const volatile &>);
	STATIC_REQUIRE(Hz::Std::HasValueType<FooValueType const volatile &>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasRefType<Foo const volatile &>);
	STATIC_REQUIRE(Hz::Std::HasRefType<FooReference const volatile &>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasConstRefType<Foo const volatile &>);
	STATIC_REQUIRE(Hz::Std::HasConstRefType<FooConstReference const volatile &>);
	STATIC_REQUIRE_FALSE(Hz::Std::HasElementType<Foo const volatile &>);
	STATIC_REQUIRE(Hz::Std::HasElementType<FooElementType const volatile &>);
}