#include "Hz/Traits/Conditional.hpp"
#include <catch2/catch_all.hpp>
#include <catch2/catch_test_macros.hpp>

TEST_CASE("Conditional", "[Hz::Conditional]")
{
	STATIC_REQUIRE(std::same_as<Hz::Cond<true, [](auto v)
	{
		if constexpr (v) { return 10; }
		else { return true; }
	}>, int>);
	STATIC_REQUIRE(std::same_as<Hz::Cond<false, [](auto v)
	{
		if constexpr (v) { return 10; }
		else { return true; }
	}>, bool>);
}