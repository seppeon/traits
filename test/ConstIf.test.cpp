#include "Hz/Traits/ConstIf.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("ConstIf", "[ConstIf]")
{
	using namespace Hz;

	using base_type1		= int;
	using base_test1_true	= ConstIf<true, base_type1>;
	using base_test1_false	= ConstIf<false, base_type1>;
	REQUIRE(std::is_same_v<base_test1_true, std::add_const_t<base_type1>>);
	REQUIRE(std::is_same_v<base_test1_false, base_type1>);
	
	using base_type2		= int const;
	using base_test2_true	= ConstIf<true, base_type2>;
	using base_test2_false	= ConstIf<false, base_type2>;
	REQUIRE(std::is_same_v<base_test1_true, std::add_const_t<base_type1>>);
	REQUIRE(std::is_same_v<base_test1_false, base_type1>);
	
	using base_type3		= int const *;
	using base_test3_true	= ConstIf<true, base_type3>;
	using base_test3_false	= ConstIf<false, base_type3>;
	REQUIRE(std::is_same_v<base_test1_true, std::add_const_t<base_type1>>);
	REQUIRE(std::is_same_v<base_test1_false, base_type1>);
	
	using base_type4		= int const * const;
	using base_test4_true	= ConstIf<true, base_type4>;
	using base_test4_false	= ConstIf<false, base_type4>;
	REQUIRE(std::is_same_v<base_test1_true, std::add_const_t<base_type1>>);
	REQUIRE(std::is_same_v<base_test1_false, base_type1>);
	
	using base_type5		= volatile int const * const;
	using base_test5_true	= ConstIf<true, base_type5>;
	using base_test5_false	= ConstIf<false, base_type5>;
	REQUIRE(std::is_same_v<base_test1_true, std::add_const_t<base_type1>>);
	REQUIRE(std::is_same_v<base_test1_false, base_type1>);
}