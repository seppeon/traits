#include "Hz/Traits/CountDimensions.hpp"
#include <catch2/catch_all.hpp>

#include <array>
#include <catch2/catch_test_macros.hpp>
TEST_CASE("count_dimensions_v can count trivial containers dimensions", "[Hz::count_dimensions_v]")
{
	STATIC_REQUIRE(Hz::count_dimensions_v<int> == 0);
	STATIC_REQUIRE(Hz::count_dimensions_v<int[3]> == 1);
	STATIC_REQUIRE(Hz::count_dimensions_v<int(&)[3]> == 1);
	STATIC_REQUIRE(Hz::count_dimensions_v<std::array<int[3][4], 4>> == 3);
	STATIC_REQUIRE(Hz::count_dimensions_v<int(&)[3][4]> == 2);
	STATIC_REQUIRE(Hz::count_dimensions_v<std::array<int[3][4], 4>> == 3);
	STATIC_REQUIRE(Hz::count_dimensions_v<int(&)[3][4][6][7]> == 4);
}
TEST_CASE("WithDimensions can check dimensions", "[Hz::WithDimensions]")
{
	STATIC_REQUIRE(Hz::WithDimensions<int, 0>);
	STATIC_REQUIRE(Hz::WithDimensions<int[3], 1>);
	STATIC_REQUIRE(Hz::WithDimensions<int(&)[3], 1>);
	STATIC_REQUIRE(Hz::WithDimensions<std::array<int[3][4], 4>, 3>);
	STATIC_REQUIRE(Hz::WithDimensions<int(&)[3][4], 2>);
	STATIC_REQUIRE(Hz::WithDimensions<int(&)[3][4][6][7], 4>);
}
TEST_CASE("WithOneDimension can check dimensions", "[Hz::WithOneDimension]")
{
	STATIC_REQUIRE_FALSE(Hz::WithOneDimension<int>);
	STATIC_REQUIRE(Hz::WithOneDimension<int[3]>);
	STATIC_REQUIRE(Hz::WithOneDimension<int(&)[3]>);
	STATIC_REQUIRE_FALSE(Hz::WithOneDimension<std::array<int[3][4], 3>>);
	STATIC_REQUIRE_FALSE(Hz::WithOneDimension<int(&)[3][4]>);
	STATIC_REQUIRE_FALSE(Hz::WithOneDimension<int(&)[3][4][6][7]>);
}
TEST_CASE("WithZeroDimension can check dimensions", "[Hz::WithZeroDimension]")
{
	STATIC_REQUIRE(Hz::WithZeroDimension<int>);
	STATIC_REQUIRE_FALSE(Hz::WithZeroDimension<int[3]>);
	STATIC_REQUIRE_FALSE(Hz::WithZeroDimension<int(&)[3]>);
	STATIC_REQUIRE_FALSE(Hz::WithZeroDimension<std::array<int[3][4], 3>>);
	STATIC_REQUIRE_FALSE(Hz::WithZeroDimension<int(&)[3][4]>);
	STATIC_REQUIRE_FALSE(Hz::WithZeroDimension<int(&)[3][4][6][7]>);
}
TEST_CASE("WithOneOrMoreDimensions can check dimensions", "[Hz::WithOneOrMoreDimensions]")
{
	STATIC_REQUIRE_FALSE(Hz::WithOneOrMoreDimensions<int>);
	STATIC_REQUIRE(Hz::WithOneOrMoreDimensions<int[3]>);
	STATIC_REQUIRE(Hz::WithOneOrMoreDimensions<int(&)[3]>);
	STATIC_REQUIRE(Hz::WithOneOrMoreDimensions<std::array<int[3][4], 3>>);
	STATIC_REQUIRE(Hz::WithOneOrMoreDimensions<int(&)[3][4]>);
	STATIC_REQUIRE(Hz::WithOneOrMoreDimensions<int(&)[3][4][6][7]>);
}
TEST_CASE("WithMultipleDimensions can check dimensions", "[Hz::WithMultipleDimensions]")
{
	STATIC_REQUIRE_FALSE(Hz::WithMultipleDimensions<int>);
	STATIC_REQUIRE_FALSE(Hz::WithMultipleDimensions<int[3]>);
	STATIC_REQUIRE_FALSE(Hz::WithMultipleDimensions<int(&)[3]>);
	STATIC_REQUIRE(Hz::WithMultipleDimensions<std::array<int[3][4], 3>>);
	STATIC_REQUIRE(Hz::WithMultipleDimensions<int(&)[3][4]>);
	STATIC_REQUIRE(Hz::WithMultipleDimensions<int(&)[3][4][6][7]>);
}