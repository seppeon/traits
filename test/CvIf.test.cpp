#include "Hz/Traits/CvIf.hpp"
#include <catch2/catch_all.hpp>
#include <catch2/catch_test_macros.hpp>

TEST_CASE("CvIf", "[Hz::CvIf]")
{
	STATIC_REQUIRE(std::same_as<Hz::CvIf<true, true, int>, int const volatile>);
	STATIC_REQUIRE(std::same_as<Hz::CvIf<false, true, int>, int volatile>);
	STATIC_REQUIRE(std::same_as<Hz::CvIf<true, false, int>, int const>);
	STATIC_REQUIRE(std::same_as<Hz::CvIf<false, false, int>, int>);
}