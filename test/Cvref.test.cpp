#include "Hz/Traits/Cvref.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("Cvref can apply various reference categories", "[Hz::Cvref]")
{
	using Hz::Qualifiers;
	using Hz::ValueCategory;

	SECTION("apply_cv_value_category_t")
	{
		using Hz::ApplyCvQualifiersAndValueCategory;
		
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int, Qualifiers::None, ValueCategory::Value>, int>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int, Qualifiers::Const, ValueCategory::Value>, int const>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int, Qualifiers::Volatile, ValueCategory::Value>, int volatile>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int, Qualifiers::ConstVolatile, ValueCategory::Value>, int const volatile>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int, Qualifiers::None, ValueCategory::LValue>, int &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int, Qualifiers::Const, ValueCategory::LValue>, int const &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int, Qualifiers::Volatile, ValueCategory::LValue>, int volatile &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int, Qualifiers::ConstVolatile, ValueCategory::LValue>, int const volatile &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int, Qualifiers::None, ValueCategory::RValue>, int &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int, Qualifiers::Const, ValueCategory::RValue>, int const &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int, Qualifiers::Volatile, ValueCategory::RValue>, int volatile &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int, Qualifiers::ConstVolatile, ValueCategory::RValue>, int const volatile &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &, Qualifiers::None, ValueCategory::Value>, int>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &, Qualifiers::Const, ValueCategory::Value>, int const>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &, Qualifiers::Volatile, ValueCategory::Value>, int volatile>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &, Qualifiers::ConstVolatile, ValueCategory::Value>, int const volatile>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &, Qualifiers::None, ValueCategory::LValue>, int &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &, Qualifiers::Const, ValueCategory::LValue>, int const &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &, Qualifiers::Volatile, ValueCategory::LValue>, int volatile &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &, Qualifiers::ConstVolatile, ValueCategory::LValue>, int const volatile &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &, Qualifiers::None, ValueCategory::RValue>, int &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &, Qualifiers::Const, ValueCategory::RValue>, int const &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &, Qualifiers::Volatile, ValueCategory::RValue>, int volatile &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &, Qualifiers::ConstVolatile, ValueCategory::RValue>, int const volatile &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &&, Qualifiers::None, ValueCategory::Value>, int>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &&, Qualifiers::Const, ValueCategory::Value>, int const>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &&, Qualifiers::Volatile, ValueCategory::Value>, int volatile>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &&, Qualifiers::ConstVolatile, ValueCategory::Value>, int const volatile>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &&, Qualifiers::None, ValueCategory::LValue>, int &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &&, Qualifiers::Const, ValueCategory::LValue>, int const &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &&, Qualifiers::Volatile, ValueCategory::LValue>, int volatile &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &&, Qualifiers::ConstVolatile, ValueCategory::LValue>, int const volatile &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &&, Qualifiers::None, ValueCategory::RValue>, int &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &&, Qualifiers::Const, ValueCategory::RValue>, int const &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &&, Qualifiers::Volatile, ValueCategory::RValue>, int volatile &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int &&, Qualifiers::ConstVolatile, ValueCategory::RValue>, int const volatile &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &, Qualifiers::None, ValueCategory::Value>, int>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &, Qualifiers::Const, ValueCategory::Value>, int const>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &, Qualifiers::Volatile, ValueCategory::Value>, int volatile>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &, Qualifiers::ConstVolatile, ValueCategory::Value>, int const volatile>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &, Qualifiers::None, ValueCategory::LValue>, int &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &, Qualifiers::Const, ValueCategory::LValue>, int const &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &, Qualifiers::Volatile, ValueCategory::LValue>, int volatile &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &, Qualifiers::ConstVolatile, ValueCategory::LValue>, int const volatile &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &, Qualifiers::None, ValueCategory::RValue>, int &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &, Qualifiers::Const, ValueCategory::RValue>, int const &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &, Qualifiers::Volatile, ValueCategory::RValue>, int volatile &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &, Qualifiers::ConstVolatile, ValueCategory::RValue>, int const volatile &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &&, Qualifiers::None, ValueCategory::Value>, int>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &&, Qualifiers::Const, ValueCategory::Value>, int const>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &&, Qualifiers::Volatile, ValueCategory::Value>, int volatile>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &&, Qualifiers::ConstVolatile, ValueCategory::Value>, int const volatile>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &&, Qualifiers::None, ValueCategory::LValue>, int &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &&, Qualifiers::Const, ValueCategory::LValue>, int const &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &&, Qualifiers::Volatile, ValueCategory::LValue>, int volatile &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &&, Qualifiers::ConstVolatile, ValueCategory::LValue>, int const volatile &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &&, Qualifiers::None, ValueCategory::RValue>, int &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &&, Qualifiers::Const, ValueCategory::RValue>, int const &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &&, Qualifiers::Volatile, ValueCategory::RValue>, int volatile &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiersAndValueCategory<int const &&, Qualifiers::ConstVolatile, ValueCategory::RValue>, int const volatile &&>);
	}

	SECTION("apply_cv_t")
	{
		using Hz::ApplyCvQualifiers;

		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int, Qualifiers::None>, int>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int, Qualifiers::Const>, int const>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int, Qualifiers::Volatile>, int volatile>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int, Qualifiers::ConstVolatile>, int const volatile>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int &, Qualifiers::None>, int &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int &, Qualifiers::Const>, int const &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int &, Qualifiers::Volatile>, int volatile &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int &, Qualifiers::ConstVolatile>, int const volatile &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int &&, Qualifiers::None>, int &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int &&, Qualifiers::Const>, int const &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int &&, Qualifiers::Volatile>, int volatile &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int &&, Qualifiers::ConstVolatile>, int const volatile &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int const &, Qualifiers::None>, int &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int const &, Qualifiers::Const>, int const &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int const &, Qualifiers::Volatile>, int volatile &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int const &, Qualifiers::ConstVolatile>, int const volatile &>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int const &&, Qualifiers::None>, int &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int const &&, Qualifiers::Const>, int const &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int const &&, Qualifiers::Volatile>, int volatile &&>);
		STATIC_REQUIRE(std::same_as<ApplyCvQualifiers<int const &&, Qualifiers::ConstVolatile>, int const volatile &&>);
	}

	SECTION("apply_value_category_t")
	{
		using Hz::ApplyValueCategory;

		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int, ValueCategory::Value>, int>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int, ValueCategory::LValue>, int &>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int, ValueCategory::RValue>, int &&>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int &, ValueCategory::Value>, int>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int &, ValueCategory::LValue>, int &>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int &, ValueCategory::RValue>, int &&>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int &&, ValueCategory::Value>, int>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int &&, ValueCategory::LValue>, int &>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int &&, ValueCategory::RValue>, int &&>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int const &, ValueCategory::Value>, int const>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int const &, ValueCategory::LValue>, int const &>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int const &, ValueCategory::RValue>, int const &&>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int const &&, ValueCategory::Value>, int const>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int const &&, ValueCategory::LValue>, int const &>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int const &&, ValueCategory::RValue>, int const &&>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int volatile &, ValueCategory::Value>, int volatile>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int volatile &, ValueCategory::LValue>, int volatile &>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int volatile &, ValueCategory::RValue>, int volatile &&>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int const volatile &&, ValueCategory::Value>, int const volatile>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int const volatile &&, ValueCategory::LValue>, int const volatile &>);
		STATIC_REQUIRE(std::same_as<ApplyValueCategory<int const volatile &&, ValueCategory::RValue>, int const volatile &&>);
	}
}
