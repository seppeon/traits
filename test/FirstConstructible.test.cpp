#include "Hz/Traits/FirstConstructible.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("FirstConstructible", "[Hz::FirstConstructible]")
{
	struct Foo { Foo(int, int){} };
	struct Bar { Bar(int *){} };
	struct Laa { Laa(bool, bool, bool){} };
	using first_type = Hz::FirstConstructibleType<Hz::TL<Foo, Bar, Laa>, int, int>;
	using second_type_1 = Hz::FirstConstructibleType<Hz::TL<Foo, Bar, Laa>, int *>;
	using second_type_2 = Hz::FirstConstructibleType<Hz::TL<Foo, Bar, Laa>, std::nullptr_t>;
	using third_type_1 = Hz::FirstConstructibleType<Hz::TL<Foo, Bar, Laa>, bool, bool, bool>;
	using third_type_2 = Hz::FirstConstructibleType<Hz::TL<Foo, Bar, Laa>, bool, bool, int>;
	STATIC_REQUIRE(std::same_as<first_type, Foo>);
	STATIC_REQUIRE(std::same_as<second_type_1, Bar>);
	STATIC_REQUIRE(std::same_as<second_type_2, Bar>);
	STATIC_REQUIRE(std::same_as<third_type_1, Laa>);
	STATIC_REQUIRE(not std::same_as<third_type_2, Laa>);
}