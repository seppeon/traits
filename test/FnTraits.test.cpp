#include "Hz/Traits/ConceptsFn.hpp"
#include <Hz/TL/TLCommon.hpp>
#include <catch2/catch_all.hpp>

TEST_CASE("FnConcepts", "[FnTraits]")
{
	using namespace Hz;
	using fn0 = decltype([](int, char, bool)->short{ return{}; });
	using fn1 = decltype([]() noexcept ->short{ return{}; });
	using fn2 = decltype([](int, char, bool){});

	REQUIRE(FnNoexcept<fn1>);
	REQUIRE(not FnNoexcept<fn0>);
	REQUIRE(not FnAbominable<fn0>);
	REQUIRE(FnMember<fn0>);
	REQUIRE(FnConst<fn0>);
	REQUIRE(not FnMutable<fn0>);
	REQUIRE(not FnVolatile<fn0>);
	REQUIRE(not FnNoexcept<fn0>);
	REQUIRE(not FnLvalue<fn0>);
	REQUIRE(not FnRvalue<fn0>);
	REQUIRE(FnSameArgs<fn0, fn2>);
	REQUIRE(not FnSameArgs<fn0, fn1>);
	REQUIRE(FnSameResult<fn0, fn1>);
	REQUIRE(not FnSameResult<fn0, fn2>);
	REQUIRE(FnSame<fn0, fn0>);
	REQUIRE(not FnSame<fn0, fn1>);
	REQUIRE(not FnSame<fn0, fn2>);
}

namespace
{
	struct AddConstPlaceholderStruct{};

	template <typename A, typename B>
	static constexpr bool fn_add_const = std::is_same_v<Hz::FnAddConst<A>, B>;
}
TEST_CASE("FnAddConst can remove cvref from abominable functions")
{
	STATIC_REQUIRE(fn_add_const<void(int, char, char) const noexcept, 					void(int, char, char) const noexcept>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char) noexcept, 						void(int, char, char) const noexcept>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char) const volatile noexcept, 			void(int, char, char) const volatile noexcept>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char) const, 							void(int, char, char) const>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char), 									void(int, char, char) const>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char) const volatile, 					void(int, char, char) const volatile>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char) const & noexcept, 				void(int, char, char) const & noexcept>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char) & noexcept, 						void(int, char, char) const & noexcept>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char) const volatile &  noexcept, 		void(int, char, char) const volatile &  noexcept>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char) const & , 						void(int, char, char) const &>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char) &, 								void(int, char, char) const &>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char) const volatile & , 				void(int, char, char) const volatile &>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char) const &&  noexcept, 				void(int, char, char) const && noexcept>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char) && noexcept, 						void(int, char, char) const && noexcept>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char) const volatile &&  noexcept, 		void(int, char, char) const volatile && noexcept>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char) const && , 						void(int, char, char) const &&>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char) &&, 								void(int, char, char) const &&>);
	STATIC_REQUIRE(fn_add_const<void(int, char, char) const volatile && , 				void(int, char, char) const volatile &&>);
}
TEST_CASE("FnAddConst can remove cvref from member functions")
{
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) const noexcept, 				void (AddConstPlaceholderStruct::*)(int, char, char) const noexcept>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) noexcept, 						void (AddConstPlaceholderStruct::*)(int, char, char) const noexcept>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) const volatile noexcept, 		void (AddConstPlaceholderStruct::*)(int, char, char) const volatile noexcept>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) const, 						void (AddConstPlaceholderStruct::*)(int, char, char) const>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char), 								void (AddConstPlaceholderStruct::*)(int, char, char) const>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) const volatile, 				void (AddConstPlaceholderStruct::*)(int, char, char) const volatile>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) const &  noexcept, 			void (AddConstPlaceholderStruct::*)(int, char, char) const & noexcept>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) & noexcept, 					void (AddConstPlaceholderStruct::*)(int, char, char) const & noexcept>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) const volatile &  noexcept, 	void (AddConstPlaceholderStruct::*)(int, char, char) const volatile &  noexcept>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) const & , 						void (AddConstPlaceholderStruct::*)(int, char, char) const &>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) &, 							void (AddConstPlaceholderStruct::*)(int, char, char) const &>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) const volatile & , 			void (AddConstPlaceholderStruct::*)(int, char, char) const volatile &>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) const &&  noexcept, 			void (AddConstPlaceholderStruct::*)(int, char, char) const && noexcept>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) && noexcept, 					void (AddConstPlaceholderStruct::*)(int, char, char) const && noexcept>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) const volatile && noexcept,	void (AddConstPlaceholderStruct::*)(int, char, char) const volatile && noexcept>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) const && , 					void (AddConstPlaceholderStruct::*)(int, char, char) const &&>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) &&, 							void (AddConstPlaceholderStruct::*)(int, char, char) const &&>);
	STATIC_REQUIRE(fn_add_const<void (AddConstPlaceholderStruct::*)(int, char, char) const volatile && , 			void (AddConstPlaceholderStruct::*)(int, char, char) const volatile &&>);
}
TEST_CASE("FnAddConst can remove cvref from free functions (pointer and references)")
{
	STATIC_REQUIRE(fn_add_const<void(*)(int, char, char) noexcept, void(*)(int, char, char) noexcept>);
	STATIC_REQUIRE(fn_add_const<void(&)(int, char, char) noexcept, void(&)(int, char, char) noexcept>);
	STATIC_REQUIRE(fn_add_const<void(*)(int, char, char), 			void(*)(int, char, char)>);
	STATIC_REQUIRE(fn_add_const<void(&)(int, char, char), 			void(&)(int, char, char)>);
}

namespace
{
	struct AbomPlaceholderStruct{};

	template <typename A, typename B>
	static constexpr bool abom = std::is_same_v<Hz::GetFnAbominable<A>, B>;
}
TEST_CASE("GetFnAbominable can remove cvref from abominable functions")
{
	STATIC_REQUIRE(abom<void(int, char, char) const noexcept, 					void(int, char, char) const noexcept>);
	STATIC_REQUIRE(abom<void(int, char, char) noexcept, 						void(int, char, char) noexcept>);
	STATIC_REQUIRE(abom<void(int, char, char) const volatile noexcept, 			void(int, char, char) const volatile noexcept>);
	STATIC_REQUIRE(abom<void(int, char, char) const, 							void(int, char, char) const>);
	STATIC_REQUIRE(abom<void(int, char, char), 									void(int, char, char)>);
	STATIC_REQUIRE(abom<void(int, char, char) const volatile, 					void(int, char, char) const volatile>);
	STATIC_REQUIRE(abom<void(int, char, char) const &  noexcept, 				void(int, char, char) const &  noexcept>);
	STATIC_REQUIRE(abom<void(int, char, char) & noexcept, 						void(int, char, char) & noexcept>);
	STATIC_REQUIRE(abom<void(int, char, char) const volatile &  noexcept, 		void(int, char, char) const volatile &  noexcept>);
	STATIC_REQUIRE(abom<void(int, char, char) const & , 						void(int, char, char) const & >);
	STATIC_REQUIRE(abom<void(int, char, char) &, 								void(int, char, char) &>);
	STATIC_REQUIRE(abom<void(int, char, char) const volatile & , 				void(int, char, char) const volatile & >);
	STATIC_REQUIRE(abom<void(int, char, char) const &&  noexcept, 				void(int, char, char) const &&  noexcept>);
	STATIC_REQUIRE(abom<void(int, char, char) && noexcept, 						void(int, char, char) && noexcept>);
	STATIC_REQUIRE(abom<void(int, char, char) const volatile &&  noexcept, 		void(int, char, char) const volatile &&  noexcept>);
	STATIC_REQUIRE(abom<void(int, char, char) const && , 						void(int, char, char) const && >);
	STATIC_REQUIRE(abom<void(int, char, char) &&, 								void(int, char, char) &&>);
	STATIC_REQUIRE(abom<void(int, char, char) const volatile && , 				void(int, char, char) const volatile && >);
}
TEST_CASE("GetFnAbominable can remove cvref from member functions")
{
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) const noexcept, 				void (int, char, char) const noexcept>);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) noexcept, 						void (int, char, char) noexcept>);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) const volatile noexcept, 		void (int, char, char) const volatile noexcept>);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) const, 						void (int, char, char) const>);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char), 								void (int, char, char)>);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) const volatile, 				void (int, char, char) const volatile>);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) const &  noexcept, 			void (int, char, char) const &  noexcept>);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) & noexcept, 					void (int, char, char) & noexcept>);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) const volatile &  noexcept, 	void (int, char, char) const volatile &  noexcept>);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) const & , 						void (int, char, char) const & >);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) &, 							void (int, char, char) &>);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) const volatile & , 			void (int, char, char) const volatile & >);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) const &&  noexcept, 			void (int, char, char) const &&  noexcept>);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) && noexcept, 					void (int, char, char) && noexcept>);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) const volatile && noexcept,	void (int, char, char) const volatile && noexcept>);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) const && , 					void (int, char, char) const && >);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) &&, 							void (int, char, char) &&>);
	STATIC_REQUIRE(abom<void (AbomPlaceholderStruct::*)(int, char, char) const volatile && , 			void (int, char, char) const volatile && >);
}
TEST_CASE("GetFnAbominable can remove cvref from free functions (pointer and references)")
{
	STATIC_REQUIRE(abom<void(*)(int, char, char) noexcept, void(int, char, char) noexcept>);
	STATIC_REQUIRE(abom<void(&)(int, char, char) noexcept, void(int, char, char) noexcept>);
	STATIC_REQUIRE(abom<void(*)(int, char, char), 			void(int, char, char)>);
	STATIC_REQUIRE(abom<void(&)(int, char, char), 			void(int, char, char)>);
}

namespace
{
	struct RemoveNoexceptPlaceholderStruct{};

	template <typename A, typename B>
	static constexpr bool remove_noexcept = std::is_same_v<Hz::FnRemoveNoexcept<A>, B>;
}
TEST_CASE("FnRemoveNoexcept can remove cvref from abominable functions")
{
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) const noexcept, 				void(int, char, char) const>);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) noexcept, 						void(int, char, char)>);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) const volatile noexcept, 		void(int, char, char) const volatile>);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) const, 						void(int, char, char) const>);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char), 								void(int, char, char)>);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) const volatile, 				void(int, char, char) const volatile>);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) const &  noexcept, 			void(int, char, char) const & >);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) & noexcept, 					void(int, char, char) &>);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) const volatile &  noexcept, 	void(int, char, char) const volatile & >);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) const & , 						void(int, char, char) const & >);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) &, 							void(int, char, char) &>);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) const volatile & , 			void(int, char, char) const volatile & >);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) const &&  noexcept, 			void(int, char, char) const && >);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) && noexcept, 					void(int, char, char) &&>);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) const volatile &&  noexcept, 	void(int, char, char) const volatile &&>);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) const && , 					void(int, char, char) const && >);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) &&, 							void(int, char, char) &&>);
	STATIC_REQUIRE(remove_noexcept<void(int, char, char) const volatile && , 			void(int, char, char) const volatile && >);
}
TEST_CASE("FnRemoveNoexcept can remove cvref from member functions")
{
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const noexcept, 				void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const>);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) noexcept, 					void (RemoveNoexceptPlaceholderStruct::*)(int, char, char)>);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const volatile noexcept, 		void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const volatile>);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const, 						void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const>);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char), 								void (RemoveNoexceptPlaceholderStruct::*)(int, char, char)>);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const volatile, 				void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const volatile>);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const &  noexcept, 			void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const & >);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) & noexcept, 					void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) &>);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const volatile &  noexcept, 	void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const volatile & >);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const & , 					void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const & >);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) &, 							void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) &>);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const volatile & , 			void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const volatile & >);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const &&  noexcept, 			void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const && >);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) && noexcept, 					void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) &&>);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const volatile && noexcept,	void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const volatile &&>);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const && , 					void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const && >);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) &&, 							void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) &&>);
	STATIC_REQUIRE(remove_noexcept<void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const volatile && , 			void (RemoveNoexceptPlaceholderStruct::*)(int, char, char) const volatile && >);
}
TEST_CASE("FnRemoveNoexcept can remove cvref from free functions (pointer and references)")
{
	STATIC_REQUIRE(remove_noexcept<void(*)(int, char, char) noexcept,	void(*)(int, char, char)>);
	STATIC_REQUIRE(remove_noexcept<void(&)(int, char, char) noexcept,	void(&)(int, char, char)>);
	STATIC_REQUIRE(remove_noexcept<void(*)(int, char, char), 			void(*)(int, char, char)>);
	STATIC_REQUIRE(remove_noexcept<void(&)(int, char, char), 			void(&)(int, char, char)>);
}