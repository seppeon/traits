#include "Hz/Traits/GetDimensionType.hpp"
#include <catch2/catch_all.hpp>
#include <array>

TEST_CASE("GetDimensionType", "[Hz::GetDimensionType]")
{
	using array_3d = std::array<std::array<std::array<char, 3>, 2>, 1>;

	using d0 = Hz::GetDimensionType<array_3d, 0>;
	using d1= Hz::GetDimensionType<array_3d, 1>;
	using d2 = Hz::GetDimensionType<array_3d, 2>;
	using d3 = Hz::GetDimensionType<array_3d, 3>;

	STATIC_REQUIRE(std::same_as<d0, array_3d>);
	STATIC_REQUIRE(std::same_as<d1, typename array_3d::value_type>);
	STATIC_REQUIRE(std::same_as<d2, typename array_3d::value_type::value_type>);
	STATIC_REQUIRE(std::same_as<d3, typename array_3d::value_type::value_type::value_type>);
}