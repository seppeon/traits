#include "Hz/Traits/GetElementCount.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("GetElementCount", "[Hz::get_element_count_v]")
{
	STATIC_REQUIRE(Hz::get_element_count_v<int[10][20][30]> == 10*20*30);
	STATIC_REQUIRE(Hz::get_element_count_v<int> == 1);
}