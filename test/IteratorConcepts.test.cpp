#include "Hz/Traits/ConceptsIterator.hpp"
#include <catch2/catch_all.hpp>
#include <deque>
#include <list>

namespace
{
	struct ForIter
	{
		struct iterator
		{
			using reference = int &;
			iterator&operator++() { return *this; }
			reference operator*() const  { static int a = 0; return a; }
			friend bool operator==(iterator const &, iterator const &) = default;
			friend bool operator!=(iterator const &, iterator const &) = default;
		};
		iterator begin() const noexcept { return {}; }
		iterator end() const noexcept { return {}; }
	};
	struct InputIter
	{
		struct iterator
		{
			using value_type = int;
			using reference = int &;
			using pointer = int *;
			using difference_type = std::make_signed_t<std::size_t>;

			iterator&operator++() { return *this; }
			iterator operator++(int) { auto out = *this; ++*this; return out; }
			reference operator*() const { static int a = 0; return a; }

			friend bool operator==(iterator const &, iterator const &) = default;
			friend bool operator!=(iterator const &, iterator const &) = default;
		};
		static_assert(std::input_iterator<iterator>);

		iterator begin() const noexcept { return {}; }
		iterator end() const noexcept { return {}; }
	};
}

TEST_CASE("IteratorConcepts", "[Hz::IteratorConcepts]")
{
	STATIC_REQUIRE(Hz::ForIterable<ForIter>);
	STATIC_REQUIRE_FALSE(Hz::InputIterable<ForIter>);
	STATIC_REQUIRE_FALSE(Hz::BidirectionalIterable<ForIter>);
	STATIC_REQUIRE_FALSE(Hz::RandomAccessIterable<ForIter>);
	STATIC_REQUIRE_FALSE(Hz::ContiguousIterable<ForIter>);

	STATIC_REQUIRE(Hz::ForIterable<InputIter>);
	STATIC_REQUIRE(Hz::InputIterable<InputIter>);
	STATIC_REQUIRE_FALSE(Hz::BidirectionalIterable<InputIter>);
	STATIC_REQUIRE_FALSE(Hz::RandomAccessIterable<InputIter>);
	STATIC_REQUIRE_FALSE(Hz::ContiguousIterable<InputIter>);

	STATIC_REQUIRE(Hz::ForIterable<std::list<int>>);
	STATIC_REQUIRE(Hz::InputIterable<std::list<int>>);
	STATIC_REQUIRE(Hz::BidirectionalIterable<std::list<int>>);
	STATIC_REQUIRE_FALSE(Hz::RandomAccessIterable<std::list<int>>);
	STATIC_REQUIRE_FALSE(Hz::ContiguousIterable<std::list<int>>);

	STATIC_REQUIRE(Hz::ForIterable<std::deque<int>>);
	STATIC_REQUIRE(Hz::InputIterable<std::deque<int>>);
	STATIC_REQUIRE(Hz::BidirectionalIterable<std::deque<int>>);
	STATIC_REQUIRE(Hz::RandomAccessIterable<std::deque<int>>);
	STATIC_REQUIRE_FALSE(Hz::ContiguousIterable<std::deque<int>>);

	STATIC_REQUIRE(Hz::ForIterable<std::vector<int>>);
	STATIC_REQUIRE(Hz::InputIterable<std::vector<int>>);
	STATIC_REQUIRE(Hz::BidirectionalIterable<std::vector<int>>);
	STATIC_REQUIRE(Hz::RandomAccessIterable<std::vector<int>>);
	STATIC_REQUIRE(Hz::ContiguousIterable<std::vector<int>>);
}