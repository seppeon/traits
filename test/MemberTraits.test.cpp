#include "Hz/Traits/MemberTraits.hpp"
#include <catch2/catch_all.hpp>

namespace
{
	struct Foo
	{
		int a;
		int const b;
		int volatile c;
		int const volatile d;
	};
}

TEST_CASE("MemberTraits", "[Hz::MemberTraits]")
{
	STATIC_REQUIRE(std::same_as<Hz::GetMemberType<decltype(&Foo::a)>, int>);
	STATIC_REQUIRE(std::same_as<Hz::GetMemberType<decltype(&Foo::b)>, int const>);
	STATIC_REQUIRE(std::same_as<Hz::GetMemberType<decltype(&Foo::c)>, int volatile>);
	STATIC_REQUIRE(std::same_as<Hz::GetMemberType<decltype(&Foo::d)>, int const volatile>);
	STATIC_REQUIRE(std::same_as<Hz::GetMemberObjectType<decltype(&Foo::a)>, Foo>);
	STATIC_REQUIRE(std::same_as<Hz::GetMemberObjectType<decltype(&Foo::b)>, Foo>);
	STATIC_REQUIRE(std::same_as<Hz::GetMemberObjectType<decltype(&Foo::c)>, Foo>);
	STATIC_REQUIRE(std::same_as<Hz::GetMemberObjectType<decltype(&Foo::d)>, Foo>);

	STATIC_REQUIRE_FALSE(Hz::get_member_is_const_v<decltype(&Foo::a)>);
	STATIC_REQUIRE(Hz::get_member_is_const_v<decltype(&Foo::b)>);
	STATIC_REQUIRE_FALSE(Hz::get_member_is_const_v<decltype(&Foo::c)>);
	STATIC_REQUIRE(Hz::get_member_is_const_v<decltype(&Foo::d)>);

	STATIC_REQUIRE_FALSE(Hz::get_member_is_volatile_v<decltype(&Foo::a)>);
	STATIC_REQUIRE_FALSE(Hz::get_member_is_volatile_v<decltype(&Foo::b)>);
	STATIC_REQUIRE(Hz::get_member_is_volatile_v<decltype(&Foo::c)>);
	STATIC_REQUIRE(Hz::get_member_is_volatile_v<decltype(&Foo::d)>);
}