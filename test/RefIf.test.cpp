#include "Hz/Traits/RefIf.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("RefIf", "[Hz::RefIf]")
{
	STATIC_REQUIRE(std::same_as<Hz::RefIf<false, int>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RefIf<true, int>, int &>);
	STATIC_REQUIRE(std::same_as<Hz::RefIf<false, volatile int>, volatile int>);
	STATIC_REQUIRE(std::same_as<Hz::RefIf<true, volatile int>, volatile int &>);
	STATIC_REQUIRE(std::same_as<Hz::RefIf<false, const int>, const int>);
	STATIC_REQUIRE(std::same_as<Hz::RefIf<true, const int>, const int &>);
	STATIC_REQUIRE(std::same_as<Hz::RefIf<false, const volatile int>, const volatile int>);
	STATIC_REQUIRE(std::same_as<Hz::RefIf<true, const volatile int>, const volatile int &>);
}