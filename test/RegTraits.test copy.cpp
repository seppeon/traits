#include "Hz/Traits/MemberTraits.hpp"
#include "Hz/Traits/ArraySizeTraits.hpp"
#include <catch2/catch_all.hpp>

namespace
{
	struct Temp { int a; };
}

TEST_CASE("MemberTraits", "[Hz::MemberTraits]")
{
	using temp_a = decltype(&Temp::a);
	STATIC_REQUIRE(std::same_as<Hz::GetMemberObjectType<temp_a>, Temp>);
	STATIC_REQUIRE(std::same_as<Hz::GetMemberType<temp_a>, int>);
	STATIC_REQUIRE(Hz::array_size_traits_v<int[4]> == 4);
	STATIC_REQUIRE(Hz::array_size_traits_v<std::array<int, 4>> == 4);
	STATIC_REQUIRE(Hz::array_size_traits_v<std::span<int, 4>> == 4);
}