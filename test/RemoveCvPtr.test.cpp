#include "Hz/Traits/RemoveCvPtr.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("RemoveCvPtr", "[Hz::RemoveCvPtr]")
{
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int *>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int const *>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int volatile *>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int volatile const *>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int const * const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int volatile * const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int volatile const * const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * volatile>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int const * volatile>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int volatile * volatile>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int volatile const * volatile>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * volatile const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int const * volatile const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int volatile * volatile const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int volatile const * volatile const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * *>, int *>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * const *>, int *>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * volatile *>, int *>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * volatile const *>, int *>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * * const>, int *>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * const * const>, int *>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * volatile * const>, int *>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * volatile const * const>, int *>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * * volatile>, int *>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * const * volatile>, int *>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * volatile * volatile>, int *>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * volatile const * volatile>, int *>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * * volatile const>, int *>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * const * volatile const>, int *>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * volatile * volatile const>, int *>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveCvPtr<int * volatile const * volatile const>, int *>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int *>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int const *>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int volatile *>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int volatile const *>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int const * const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int volatile * const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int volatile const * const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * volatile>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int const * volatile>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int volatile * volatile>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int volatile const * volatile>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * volatile const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int const * volatile const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int volatile * volatile const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int volatile const * volatile const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * *>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * const *>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * volatile *>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * volatile const *>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * * const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * const * const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * volatile * const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * volatile const * const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * * volatile>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * const * volatile>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * volatile * volatile>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * volatile const * volatile>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * * volatile const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * const * volatile const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * volatile * volatile const>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveAllCvPtr<int * volatile const * volatile const>, int>);
}