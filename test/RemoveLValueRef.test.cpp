#include "Hz/Traits/RemoveLValueRef.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("RemoveLValueRef", "[Hz::RemoveLValueRef]")
{
	STATIC_REQUIRE(std::same_as<Hz::RemoveLValueRef<int>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveLValueRef<int &>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveLValueRef<int const &>, int const>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveLValueRef<int volatile &>, int volatile>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveLValueRef<int const volatile &>, int const volatile>);
	STATIC_REQUIRE_FALSE(std::same_as<Hz::RemoveLValueRef<int &&>, int>);
	STATIC_REQUIRE_FALSE(std::same_as<Hz::RemoveLValueRef<int const &&>, int const>);
	STATIC_REQUIRE_FALSE(std::same_as<Hz::RemoveLValueRef<int volatile &&>, int volatile>);
	STATIC_REQUIRE_FALSE(std::same_as<Hz::RemoveLValueRef<int const volatile &&>, int const volatile>);
}