#include "Hz/Traits/RemoveRValueRef.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("RemoveRValueRef", "[Hz::RemoveRValueRef]")
{
	STATIC_REQUIRE(std::same_as<Hz::RemoveRValueRef<int>, int>);
	STATIC_REQUIRE_FALSE(std::same_as<Hz::RemoveRValueRef<int &>, int>);
	STATIC_REQUIRE_FALSE(std::same_as<Hz::RemoveRValueRef<int const &>, int const>);
	STATIC_REQUIRE_FALSE(std::same_as<Hz::RemoveRValueRef<int volatile &>, int volatile>);
	STATIC_REQUIRE_FALSE(std::same_as<Hz::RemoveRValueRef<int const volatile &>, int const volatile>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveRValueRef<int &&>, int>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveRValueRef<int const &&>, int const>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveRValueRef<int volatile &&>, int volatile>);
	STATIC_REQUIRE(std::same_as<Hz::RemoveRValueRef<int const volatile &&>, int const volatile>);
}