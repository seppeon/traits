#include "Hz/Traits/ReplaceOnMatch.hpp"
#include <catch2/catch_all.hpp>
#include <type_traits>

TEST_CASE("Correctly replaces type A with B, when A is type C", "[Hz::Traits]")
{
    STATIC_REQUIRE(std::is_same_v<Hz::ReplaceOnMatch<char, char, int>, int>);
    STATIC_REQUIRE(std::is_same_v<Hz::ReplaceOnMatch<char, bool, int>, char>);
    STATIC_REQUIRE(std::is_same_v<Hz::ReplaceOnMatch<int, bool, int>, int>);
}
TEST_CASE("Correctly replaces type A with B for nested and indirect cases", "[Hz::Traits]")
{
    // Trivial cases
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char, char, int>, int>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char, bool, int>, char>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int, bool, int>, int>);
    // Indirect pointer
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char *, char, int>, int *>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char *, bool, int>, char *>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int *, bool, int>, int *>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const *, char, int>, int const *>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const *, bool, int>, char const *>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const *, bool, int>, int const  *>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile *, char, int>, int volatile *>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile *, bool, int>, char volatile *>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int volatile *, bool, int>, int volatile  *>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile *, char, int>, int const volatile *>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile *, bool, int>, char const volatile *>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const volatile *, bool, int>, int const volatile  *>);
    // Indirect reference
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char &, char, int>, int &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char &, bool, int>, char &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int &, bool, int>, int &>);

    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char * &, char, int>, int * &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char * &, bool, int>, char * &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int * &, bool, int>, int * &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const * &, char, int>, int const * &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const * &, bool, int>, char const * &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const * &, bool, int>, int const  * &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile * &, char, int>, int volatile * &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile * &, bool, int>, char volatile * &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int volatile * &, bool, int>, int volatile  * &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile * &, char, int>, int const volatile * &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile * &, bool, int>, char const volatile * &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const volatile * &, bool, int>, int const volatile  * &>);

    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char * const &, char, int>, int * const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char * const &, bool, int>, char * const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int * const &, bool, int>, int * const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const * const &, char, int>, int const * const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const * const &, bool, int>, char const * const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const * const &, bool, int>, int const  * const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile * const &, char, int>, int volatile * const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile * const &, bool, int>, char volatile * const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int volatile * const &, bool, int>, int volatile  * const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile * const &, char, int>, int const volatile * const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile * const &, bool, int>, char const volatile * const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const volatile * const &, bool, int>, int const volatile  * const &>);

    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char * volatile &, char, int>, int * volatile &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char * volatile &, bool, int>, char * volatile &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int * volatile &, bool, int>, int * volatile &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const * volatile &, char, int>, int const * volatile &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const * volatile &, bool, int>, char const * volatile &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const * volatile &, bool, int>, int const  * volatile &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile * volatile &, char, int>, int volatile * volatile &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile * volatile &, bool, int>, char volatile * volatile &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int volatile * volatile &, bool, int>, int volatile  * volatile &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile * volatile &, char, int>, int const volatile * volatile &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile * volatile &, bool, int>, char const volatile * volatile &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const volatile * volatile &, bool, int>, int const volatile  * volatile &>);

    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char * volatile const &, char, int>, int * volatile const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char * volatile const &, bool, int>, char * volatile const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int * volatile const &, bool, int>, int * volatile const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const * volatile const &, char, int>, int const * volatile const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const * volatile const &, bool, int>, char const * volatile const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const * volatile const &, bool, int>, int const  * volatile const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile * volatile const &, char, int>, int volatile * volatile const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile * volatile const &, bool, int>, char volatile * volatile const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int volatile * volatile const &, bool, int>, int volatile  * volatile const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile * volatile const &, char, int>, int const volatile * volatile const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile * volatile const &, bool, int>, char const volatile * volatile const &>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const volatile * volatile const &, bool, int>, int const volatile  * volatile const &>);

    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char &&, char, int>, int &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char &&, bool, int>, char &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int &&, bool, int>, int &&>);

    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char * &&, char, int>, int * &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char * &&, bool, int>, char * &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int * &&, bool, int>, int * &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const * &&, char, int>, int const * &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const * &&, bool, int>, char const * &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const * &&, bool, int>, int const  * &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile * &&, char, int>, int volatile * &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile * &&, bool, int>, char volatile * &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int volatile * &&, bool, int>, int volatile  * &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile * &&, char, int>, int const volatile * &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile * &&, bool, int>, char const volatile * &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const volatile * &&, bool, int>, int const volatile  * &&>);

    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char * const &&, char, int>, int * const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char * const &&, bool, int>, char * const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int * const &&, bool, int>, int * const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const * const &&, char, int>, int const * const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const * const &&, bool, int>, char const * const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const * const &&, bool, int>, int const  * const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile * const &&, char, int>, int volatile * const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile * const &&, bool, int>, char volatile * const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int volatile * const &&, bool, int>, int volatile  * const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile * const &&, char, int>, int const volatile * const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile * const &&, bool, int>, char const volatile * const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const volatile * const &&, bool, int>, int const volatile  * const &&>);

    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char * volatile &&, char, int>, int * volatile &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char * volatile &&, bool, int>, char * volatile &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int * volatile &&, bool, int>, int * volatile &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const * volatile &&, char, int>, int const * volatile &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const * volatile &&, bool, int>, char const * volatile &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const * volatile &&, bool, int>, int const  * volatile &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile * volatile &&, char, int>, int volatile * volatile &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile * volatile &&, bool, int>, char volatile * volatile &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int volatile * volatile &&, bool, int>, int volatile  * volatile &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile * volatile &&, char, int>, int const volatile * volatile &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile * volatile &&, bool, int>, char const volatile * volatile &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const volatile * volatile &&, bool, int>, int const volatile  * volatile &&>);

    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char * volatile const &&, char, int>, int * volatile const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char * volatile const &&, bool, int>, char * volatile const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int * volatile const &&, bool, int>, int * volatile const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const * volatile const &&, char, int>, int const * volatile const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const * volatile const &&, bool, int>, char const * volatile const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const * volatile const &&, bool, int>, int const  * volatile const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile * volatile const &&, char, int>, int volatile * volatile const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char volatile * volatile const &&, bool, int>, char volatile * volatile const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int volatile * volatile const &&, bool, int>, int volatile  * volatile const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile * volatile const &&, char, int>, int const volatile * volatile const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<char const volatile * volatile const &&, bool, int>, char const volatile * volatile const &&>);
    STATIC_REQUIRE(std::is_same_v<Hz::DeepReplaceOnMatch<int const volatile * volatile const &&, bool, int>, int const volatile  * volatile const &&>);
}