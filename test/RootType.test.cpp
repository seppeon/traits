#include "Hz/Traits/RootType.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("RootType can get the value type of multiple dimension containers", "[Hz::RootType]")
{
	STATIC_REQUIRE(std::same_as<int,         Hz::RootType<int>>);
	STATIC_REQUIRE(std::same_as<int,         Hz::RootType<std::vector<std::array<int,3>>>>);
	STATIC_REQUIRE(std::same_as<int &,       Hz::RootType<int(&)[3][3]>>);
	STATIC_REQUIRE(std::same_as<int &,       Hz::RootType<std::array<int,3>(&)[3]>>);
	STATIC_REQUIRE(std::same_as<int const &, Hz::RootType<std::array<const std::array<int, 3>, 4> &>>);
}