#include "Hz/Traits/SimilarTo.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("SimilarTo", "[Hz::SimilarTo]")
{
	STATIC_REQUIRE(Hz::SimilarTo<int, int>);
	STATIC_REQUIRE(Hz::SimilarTo<int, int const>);
	STATIC_REQUIRE(Hz::SimilarTo<int, int volatile>);	
	STATIC_REQUIRE(Hz::SimilarTo<int, int const volatile>);

	STATIC_REQUIRE(Hz::SimilarTo<int &, int &>);
	STATIC_REQUIRE(Hz::SimilarTo<int &, int const &>);
	STATIC_REQUIRE(Hz::SimilarTo<int &, int volatile &>);	
	STATIC_REQUIRE(Hz::SimilarTo<int &, int const volatile &>);
	STATIC_REQUIRE_FALSE(Hz::SimilarTo<int const &, int &>);
	STATIC_REQUIRE(Hz::SimilarTo<int const &, int const &>);
	STATIC_REQUIRE_FALSE(Hz::SimilarTo<int const &, int volatile &>);
	STATIC_REQUIRE(Hz::SimilarTo<int const &, int const volatile &>);
	STATIC_REQUIRE_FALSE(Hz::SimilarTo<int volatile &, int &>);
	STATIC_REQUIRE_FALSE(Hz::SimilarTo<int volatile &, int const &>);
	STATIC_REQUIRE(Hz::SimilarTo<int volatile &, int volatile &>);
	STATIC_REQUIRE(Hz::SimilarTo<int volatile &, int const volatile &>);
	STATIC_REQUIRE_FALSE(Hz::SimilarTo<int volatile const &, int &>);
	STATIC_REQUIRE_FALSE(Hz::SimilarTo<int volatile const &, int const &>);
	STATIC_REQUIRE_FALSE(Hz::SimilarTo<int volatile const &, int volatile &>);
	STATIC_REQUIRE(Hz::SimilarTo<int volatile const &, int const volatile &>);

	STATIC_REQUIRE(Hz::SimilarTo<int *, int *>);
	STATIC_REQUIRE(Hz::SimilarTo<int *, int const *>);
	STATIC_REQUIRE(Hz::SimilarTo<int *, int volatile *>);	
	STATIC_REQUIRE(Hz::SimilarTo<int *, int const volatile *>);
	STATIC_REQUIRE_FALSE(Hz::SimilarTo<int const *, int *>);
	STATIC_REQUIRE(Hz::SimilarTo<int const *, int const *>);
	STATIC_REQUIRE_FALSE(Hz::SimilarTo<int const *, int volatile *>);
	STATIC_REQUIRE(Hz::SimilarTo<int const *, int const volatile *>);
	STATIC_REQUIRE_FALSE(Hz::SimilarTo<int volatile *, int *>);
	STATIC_REQUIRE_FALSE(Hz::SimilarTo<int volatile *, int const *>);
	STATIC_REQUIRE(Hz::SimilarTo<int volatile *, int volatile *>);
	STATIC_REQUIRE(Hz::SimilarTo<int volatile *, int const volatile *>);
	STATIC_REQUIRE_FALSE(Hz::SimilarTo<int volatile const *, int *>);
	STATIC_REQUIRE_FALSE(Hz::SimilarTo<int volatile const *, int const *>);
	STATIC_REQUIRE_FALSE(Hz::SimilarTo<int volatile const *, int volatile *>);
	STATIC_REQUIRE(Hz::SimilarTo<int volatile const *, int const volatile *>);
}