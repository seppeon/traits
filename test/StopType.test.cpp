#include "Hz/Traits/StopType.hpp"
#include <catch2/catch_all.hpp>
#include <string>

namespace
{
	struct NotStringStop
	{
		template <typename T>
		static constexpr auto Test = not std::same_as<T, std::string>;
	};
}

TEST_CASE("StopType", "[Hz::StopType]")
{
	STATIC_REQUIRE(std::same_as<Hz::StopType<NotStringStop, int>, int>);
	STATIC_REQUIRE(std::same_as<Hz::StopType<NotStringStop, std::vector<int>>, int>);
	STATIC_REQUIRE(std::same_as<Hz::StopType<NotStringStop, std::vector<std::string>>, std::string>);
	STATIC_REQUIRE(std::same_as<Hz::StopType<NotStringStop, std::vector<std::vector<int>>>, int>);
	STATIC_REQUIRE(std::same_as<Hz::StopType<NotStringStop, std::vector<std::vector<std::string>>>, std::string>);
}