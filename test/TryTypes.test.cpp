#include "Hz/Traits/TryTypes.hpp"
#include <catch2/catch_all.hpp>
#include <type_traits>

namespace
{
	struct IntegralType{};
	struct FloatType{};

	template <typename T>
		requires std::is_integral_v<T>
	struct Integral
	{
		using type = IntegralType;
	};

	template <typename T>
		requires std::is_floating_point_v<T>
	struct Float
	{
		using type = FloatType;
	};

	template <typename T>
	struct Any
	{
		using type = void;
	};

	template <typename T>
	using FloatOrIntegral = Hz::TryTypes<T, Integral, Float, Any>;
}

TEST_CASE("TryTypes", "[Hz::TryTypes]")
{
	STATIC_REQUIRE(std::same_as<FloatOrIntegral<int>, IntegralType>);
	STATIC_REQUIRE(std::same_as<FloatOrIntegral<float>, FloatType>);
	STATIC_REQUIRE(std::same_as<FloatOrIntegral<int*>, void>);
}