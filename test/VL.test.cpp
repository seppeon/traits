#include "Hz/Traits/VL.hpp"
#include <catch2/catch_all.hpp>

namespace
{
	struct OddFilter
	{
		template <std::size_t i>
		static constexpr bool Check(Hz::IL<i>)
		{
			return i & 1;
		}
	};
}

TEST_CASE("VL", "[Hz::VL]")
{
	STATIC_REQUIRE(std::same_as<Hz::VL<1, 2, 3>, Hz::ToVL<Hz::ToTL<Hz::VL<1, 2, 3>>>>);
	STATIC_REQUIRE(std::same_as<Hz::VL<std::size_t{1}>, Hz::IL<1>>);
	STATIC_REQUIRE(std::same_as<Hz::ILMake<4>, Hz::IL<0, 1, 2, 3>>);
	STATIC_REQUIRE(Hz::il_filter_result_length<Hz::IL<0, 1, 2, 3>, OddFilter> == 2);
	STATIC_REQUIRE(std::same_as<Hz::ILFilter<Hz::IL<0, 1, 2, 3>, OddFilter>, Hz::IL<1, 3>>);
}