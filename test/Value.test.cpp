#include "Hz/Traits/Value.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("Value", "[Hz::Value]")
{
	STATIC_REQUIRE(std::same_as<typename Hz::ValueTag<1>::value_type, int>);
	STATIC_REQUIRE(Hz::ValueTag<1>::value == 1);
	STATIC_REQUIRE(std::same_as<typename Hz::SizeTag<1>::value_type, std::size_t>);
	STATIC_REQUIRE(Hz::SizeTag<1>::value == 1);
}

