#include "Hz/Traits/ValueType.hpp"
#include <catch2/catch_all.hpp>
#include <memory>

namespace
{
	struct Foo
	{
		int x;
		int const y;
	};
	struct Bar
	{
		int a = 0;
		int & operator[](std::size_t) { return a; }
	};
	struct Laa
	{
		int a = 0;
		int const & operator[](std::size_t) const { return a; }
	};
	struct BarIter
	{
		int a = 0;
		int * begin() { return &a; }
		int * end() { return begin() + 1; }
	};
	struct LaaIter
	{
		int a = 0;
		int const * begin() const { return &a; }
		int const * end() const { return begin() + 1; }
	};
}
TEST_CASE("ValueType can get the types of values in various containers", "[Hz::ValueType]")
{
	SECTION("C-Arrays")
	{
		STATIC_REQUIRE(std::same_as<Hz::ValueType<int(*)[3]>, int&>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<int(&)[3]>, int&>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<int const (*)[3]>, int const &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<int const (&)[3]>, int const &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<int volatile (*)[3]>, int volatile &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<int volatile (&)[3]>, int volatile &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<int const volatile (*)[3]>, int const volatile &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<int const volatile (&)[3]>, int const volatile &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<int(* const)[3]>, int&>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<int const (* const)[3]>, const int &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<int volatile (* const)[3]>, int volatile &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<int const volatile (* const)[3]>, int const volatile &>);
	}
	SECTION("std::*container*")
	{
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int, 3>>, int>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int, 3> &>, int &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int const, 3>>, int const>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int, 3> const>, int const>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int const, 3> const>, int const>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int const, 3> &>, int const &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int, 3> const &>, int const &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int const, 3> const &>, int const &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int volatile, 3>>, int volatile>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int, 3> volatile>, int volatile>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int volatile, 3> volatile>, int volatile>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int volatile, 3> &>, int volatile &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int, 3> volatile &>, int volatile &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int volatile, 3> volatile &>, int volatile &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int const volatile, 3>>, int const volatile>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int, 3> const volatile>, int const volatile>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int const volatile, 3> const volatile>, int const volatile>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int const volatile, 3> &>, int const volatile &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int, 3> const volatile &>, int const volatile &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<std::array<int const volatile, 3> const volatile &>, int const volatile &>);
	}
	SECTION("Data member pointers")
	{
		auto x_ptr = &Foo::x;
		auto const x_c_ptr = &Foo::x;
		auto c_y_ptr = &Foo::y;
		auto const c_y_c_ptr = &Foo::y;
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(x_ptr)>, int &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(x_c_ptr)>, int &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(c_y_ptr)>, int const &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(c_y_c_ptr)>, int const &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(x_ptr) &>, int &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(x_c_ptr) &>, int &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(c_y_ptr) &>, int const &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(c_y_c_ptr) &>, int const &>);
	}
	SECTION("operator[]")
	{
		Bar bar;
		Laa laa;
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(bar)>, int &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(laa)>, int const &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(bar) &>, int &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(laa) &>, int const &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(laa) const &>, int const &>);
	}
	SECTION("begin(x)")
	{
		BarIter bar;
		LaaIter laa;
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(bar)>, int &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(laa)>, int const &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(bar) &>, int &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(laa) &>, int const &>);
		STATIC_REQUIRE(std::same_as<Hz::ValueType<decltype(laa) const &>, int const &>);
	}
	SECTION("smart_pointr::element_type")
	{
		SECTION("unique_ptr")
		{
			std::unique_ptr<int> ptr;
			using smart_ptr = decltype(ptr);
			STATIC_REQUIRE(std::same_as<Hz::ValueType<smart_ptr>, int &>);
			STATIC_REQUIRE(std::same_as<Hz::ValueType<smart_ptr const>, int &>);
			STATIC_REQUIRE(std::same_as<Hz::ValueType<smart_ptr &>, int &>);
			STATIC_REQUIRE(std::same_as<Hz::ValueType<smart_ptr const &>, int &>);
		}
		SECTION("shared_ptr")
		{
			std::shared_ptr<int> ptr;
			using smart_ptr = decltype(ptr);
			STATIC_REQUIRE(std::same_as<Hz::ValueType<smart_ptr>, int &>);
			STATIC_REQUIRE(std::same_as<Hz::ValueType<smart_ptr const>, int &>);
			STATIC_REQUIRE(std::same_as<Hz::ValueType<smart_ptr &>, int &>);
			STATIC_REQUIRE(std::same_as<Hz::ValueType<smart_ptr const &>, int &>);
		}
	}
}