#include "Hz/Traits/VolatileIf.hpp"
#include <catch2/catch_all.hpp>
TEST_CASE("VolatileIf can apply volatile conditionally", "[Hz::VolatileIf]")
{
	STATIC_REQUIRE(std::same_as<Hz::VolatileIf<false, int>, int>);
	STATIC_REQUIRE(std::same_as<Hz::VolatileIf<true, int>, volatile int>);
	STATIC_REQUIRE(std::same_as<Hz::VolatileIf<false, const int>, const int>);
	STATIC_REQUIRE(std::same_as<Hz::VolatileIf<true, const int>, const volatile int>);
	STATIC_REQUIRE(std::same_as<Hz::VolatileIf<false, volatile int>, volatile int>);
	STATIC_REQUIRE(std::same_as<Hz::VolatileIf<true, volatile int>, volatile int>);
}